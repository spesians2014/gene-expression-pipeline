/**
* GenEx Main (GUI)
* 
* Depends: StartPanel, WaitPanel, GraphPanel... and so on
* 
* (replace Import/*:. with classpath of JRI and JUNG .jars
* make sure you have the JRI .run script and JNI library)
*
* Compilation: javac -cp Import/*:. GenEx.java
* 	You may need to separately compile some classes... for some reason, not everything gets compiled
* Execution: ./run -cp Import/*:. GenEx
*
* TODO: ADD SUPPORT FOR .CSV MATRIX FILES (AnalysisPanel, GraphPanel, FileCheck...)
**/

/*import libraries*/
import org.rosuda.JRI.Rengine;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.util.Vector;
import java.awt.*;
import java.awt.event.*;
import java.io.File;


public class GenEx extends JFrame {
	/**
	* instance variables/constants
	**/
	//default panel size
	public static final Dimension DEFAULT_SIZE = new Dimension(600, 450);
	public static final int MAX_FILES = 2;
	//Rengine
	private Rengine re;
	//default location on screen
	private int LOCATION = 200;
	private JPanel bigPanel;
	private JPanel glassPane;
	private CardLayout cl;
	private StartPanel sp;
	private AnalysisPanel ap;
	private GraphPanel gp;
	private File[] files;
	private int[] arguments;


	/**
	* constructor
	**/
	public GenEx() {
		//start R
		re = Analysis.startR();

		//initialize files, arguments
		files = new File[0];
		arguments = new int[0];

		//set frame attributes
		this.setTitle("Gene Coexpression Analysis Pipeline");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(getXCoord(), 0);
		this.setMinimumSize(DEFAULT_SIZE);

		//set layout
		cl = new CardLayout(); 
		bigPanel = new JPanel(cl);
		this.add(bigPanel);

		//add glass pane
		this.addGlassPane();

		//create and add start panel
		this.createStartPanel();
		this.pack();
	}


	/**
	* building frame
	**/
	/*get location*/
	private int getXCoord() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int x = screenSize.width/2 - (int) DEFAULT_SIZE.getWidth()/2;
		return x;
	}


	/**
	* Panel changing methods
	**/
	/*create and add start panel*/
	public void createStartPanel() {
		sp = new StartPanel(this);
		bigPanel.add(sp);
	}

	/*create analysis panel*/
	public void createAnalysisPanel() {
		//remove old analysis panel
		if (ap != null) {
			bigPanel.remove(ap);
		}

		//create new AnalysisPanel
		ap = new AnalysisPanel(this);

		ap.nextStep();
		cl.last(bigPanel);
	}
	/*add analysis panel*/
	public void addAnalysisPanel() {
		if (ap != null) {
			bigPanel.add(ap);
		}		
	}

	/*create and add graph panel*/
	public void toGraphPanel() {
		this.setVisibleGlass(true);

		int n = files.length;

		//if greater than max no. of files, ask user to select up to two files to graph
		if (n > MAX_FILES) {
			//the dialog calls createGraphPanel
			FileSelectionDialog dialog = new FileSelectionDialog(files, this);
		}
		else {
			createGraphPanel();
		}
	}

	/*to first panel*/
	public void toFirst() {
		cl.first(bigPanel);
	}
	/*to next panel*/
	public void goForward() {
		cl.next(bigPanel);
	}


	/**
	* create graph panel
	**/
	public void createGraphPanel() {
		//load graph panel in separate thread
		class GraphLoader extends SwingWorker<Boolean, Void> {
			GenEx g;

			//constructor
			GraphLoader(GenEx g) {
				super();
				this.g = g;
			}

			@Override
			public Boolean doInBackground() {
				//remove old graph panel
				if (gp != null) {
					bigPanel.remove(gp);
				}

				//create new GraphPanel
				gp = new GraphPanel(g);
				//add
				bigPanel.add(gp);
				return true;
			}

			@Override
			public void done() {
				//move forward
				goForward();
				sp.g.setVisibleGlass(false);
			}
		}

		//execute load
		GraphLoader loader = new GraphLoader(this);
		loader.execute();
	}


	/**
	* glass pane
	**/
	private void addGlassPane() {
		glassPane = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				g.setColor(new Color(220, 220, 220, 200));
				g.fillRect(0, 0, this.getWidth(), this.getHeight());
			}
		};
		glassPane.setOpaque(false);
		glassPane.setLayout(new GridBagLayout());
		//wait label
		JLabel waitLabel = new JLabel("<html><div align = \"center\">Please wait.<br />Loading...</div></html>");
		waitLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 30));
		glassPane.add(waitLabel);

		//set glass pane
		this.setGlassPane(glassPane);
	}


	/**
	* access methods
	**/
	/*start panel*/
	public StartPanel getStartPanel() {
		return sp;
	}
	/*analysis panel*/
	public AnalysisPanel getAnalysisPanel() {
		return ap;
	}
	/*update files*/
	public void updateFiles(File[] files) {
		this.files = files;
	}
	/*get files*/
	public File[] getFiles() {
		return files;
	}
	/*update arguments*/
	public void updateArgs(int[] arguments) {
		this.arguments = arguments;
	}
	/*get arguments*/
	public int[] getArguments() {
		return arguments;
	}
	/*get Rengine*/
	public Rengine getRE() {
		return re;
	}
	/*glass pane*/
	public JPanel glassPane() {
		return glassPane;
	}
	/*set glass pane visibility*/
	public void setVisibleGlass(boolean vis) {
		glassPane.setVisible(vis);
		this.repaint();
		this.revalidate();
	}


	/**
	* show GUI
	**/
	public void showGUI() {
		this.setVisible(true);
	}


	/**
	* test main
	**/
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				GenEx g = new GenEx();
				g.showGUI();
			}
		});
	}
}
