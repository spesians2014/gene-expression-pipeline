/**
* GenEx GUI file checking library
* Methods for checking files 
*
* Depends: File, Java swing, In from stdlib
**/

/*import libraries*/
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.EmptyBorder;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.lang.InterruptedException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.CancellationException;
import edu.princeton.cs.introcs.*;


public final class FileCheck {
	/**
	* global constants
	**/
	public static final String CORRDATA = "_corrData.rds";
	public static final String CSV = ".csv";
	public static final String RDS = ".rds";
	public static final String SOFT = ".soft";
	public static final String TXT = ".txt";
	public static final int IS_LIST = 3;
	

	/**
	* StartPanel file check
	* Check number, extension
	**/
	public static final boolean checkSPFiles(File[] files) {
		boolean checked = true;

		//check number of files
		if (files.length < 1) {
			noFileErrorSP();
			checked = false;
		}
		//check file extension
		else {
			for (int i = 0; i < files.length; i++) {
				String ext = getExtension(files[i]);
				//check extension for each file
				if (!ext.equals(SOFT) && !ext.equals(CSV) && !ext.equals(RDS) && !ext.equals(TXT)) {
					fileTypeErrorSP();
					checked = false;
					break;
				}
			}
		}
		return checked;
	}

	/*StartPanel check: error messages*/
	/*no files uploaded error*/
	private static final void noFileErrorSP() {
		JOptionPane.showMessageDialog(null, "Please upload a file first.", "No File", JOptionPane.WARNING_MESSAGE);
	}

	/*file type error*/
	private static final void fileTypeErrorSP() {
		String errorMessage = String.format("<html><p>Please make sure your files are<br>%s, %s, %s, or %s files.", SOFT, RDS, CSV, TXT);
		JOptionPane.showMessageDialog(null, errorMessage, "Unrecognized File Type", JOptionPane.WARNING_MESSAGE);
	}



	/**
	* AnalysisPanel file check
	* returns integer for what to do
	**/
	public static final AnalysisPanel.Process checkAPFiles(File[] files) {
		//default process: normal
		AnalysisPanel.Process doProcess = AnalysisPanel.Process.NORMAL;
		int n = files.length;

		boolean[] corr = whichCorrelated(files);
		//if 1 file, if .SOFT file, ask about splitting
		if ((n == 1) && (getExtension(files[0]).equals(SOFT)) && !corr[n+1]) {
			//splitting question?
			int split = splitSet();
			//if yes, split and return
			if (split == JOptionPane.YES_OPTION) {
				doProcess = AnalysisPanel.Process.SPLIT;
				return doProcess;
			}
		}

		//if all files are .RDS and/or text adjacnecy lists, skip immediately to graphing
		if (corr[n + 1] && allListOrRDS(files)) {
			doProcess = AnalysisPanel.Process.GRAPH;
		}
		else if (corr[n]) {
			//use already correlated files question?
			int use = useCorrelated(corr, files);
			if (use == JOptionPane.YES_OPTION) {
				if (corr[n + 1]) {
					//if all already correlated, skip to graphing
					doProcess = AnalysisPanel.Process.GRAPH;
				}
				else {
						//else start analysis for all files but the already-correlated
					doProcess = AnalysisPanel.Process.SOME;
				}
			}
		}
		return doProcess;
	}

	/*check which files already correlated
	index n indicates whether at least one file is correlated
	index n + 1 indicates whether all files already correlated*/
	private static final boolean[] whichCorrelated(File[] files) {
		int n = files.length;
		boolean[] corr = new boolean[n + 2];
		corr[n] = false;
		corr[n + 1] = true;

		//check if a _corrData.rds file exists
		for (int i = 0; i < n; i++) {
			//check is correlated or not
			corr[i] = isCorrelated(files[i]);

			//change index for whether all correlated
			if (corr[n + 1] && !corr[i]) {
				corr[n + 1] = false;
			}
			//change index for if at least one correlated
			else if (!corr[n] && corr[i]) {
				corr[n] = true;
			}
		}

		return corr;
	}

	/*check if *A* file is already correlated*/
	public static final boolean isCorrelated(File file) {
		boolean isCorr = false;

		//check if name already includes _corrData.rds
		if (file.getName().indexOf(CORRDATA) > 0) {
			isCorr = true;
		}
		//else search directory for NAME_corrData.rds
		else {
			String corrFile = getPathNoExt(file) + CORRDATA;
			isCorr = new File(corrFile).isFile();

			//if no NAME_corrData.rds and if not a .soft or .rds,
			//check number of columns in first line
			//if 3 columns (2 delimiters), treat as adjacency list
			String ext = getExtension(file);
			if (!isCorr && !ext.equals(RDS) && !ext.equals(SOFT) && (getColumnNumber(file) == IS_LIST)) {
				isCorr = true;
			}
		}

		return isCorr;
	}

	/*check whether all files are .rds or adjacency lists*/
	private static final boolean allListOrRDS(File[] files) {
		boolean corr = true;
		for (int i = 0; i < files.length; i++) {
			String ext = getExtension(files[i]);
			if (ext.equals(RDS)) {
				continue;
			}
			else if (!ext.equals(SOFT) && (getColumnNumber(files[i]) != IS_LIST)) {
				//if not .soft and  columns in first line != 3,
				//is not adjacency list, so corr = false
				corr = false;
				break;
			}
		}
		return corr;
	}


	/* ask about splitting dataset*/
	private static final int splitSet() {
		int split = JOptionPane.showConfirmDialog(null, "Would you like to split this dataset\ninto subgroups by sample attributes (e.g., sample tissue type)?", "Split Dataset?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		return split;
	}


	/* ask about using already correlated values*/
	private static final int useCorrelated(boolean corr[], File files[]) {
		//get filenames
		StringBuilder filenameBuilder = new StringBuilder();
		for (int i = 0; i < files.length; i++) {
			if (corr[i]) {
				filenameBuilder.append(files[i].getName());
				filenameBuilder.append(", ");
			}
		}
		//remove unecessary commas
		filenameBuilder.delete(filenameBuilder.length() - 2, filenameBuilder.length() - 1);
		String filenames = filenameBuilder.toString();

		String alreadyCorrelated = String.format("Looks like you have correlation data for\nthe dataset(s) named %s\nfrom a previous session.\nWould you like to use the previous session's data?", filenames);
		int correlated = JOptionPane.showConfirmDialog(null, alreadyCorrelated, "Already Correlated", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		return correlated;
	}



	/*dataset splitting
	new panel to get how user would like to split their data*/
	public static final int splitDataFrame(File file) {
		Object[] features ={"Sorry! Error: unable to access dataset subgroup information."};
		String[] subsets = new String[0];

		try {
			if (SplitAnalysis.parseSOFT(file)) {
				subsets = SplitAnalysis.getSubsets(file);
			}

			if (subsets.length > 0) {
				features = new Object[subsets.length + 1];
				features[0] = "Select subgrouping";
				for (int i = 0; i < subsets.length; i++) {
					features[i+1] = subsets[i];
				}
			}
		} catch (NullPointerException ignore) {;}

		//display JOptionPane
		//get selection
		Object selection = new Object();
		selection = JOptionPane.showInputDialog(null, "Please select a feature to use to split the data:", "Split dataset into subgroups", JOptionPane.INFORMATION_MESSAGE, null, features, features[0]);

		//get option
		int val = -1;
		val = Arrays.asList(features).indexOf(selection);

		return val;
	}



	/**
	* GraphData 
	**/ 
	/*error if GraphData fails to read files*/
	public static final void graphDataError() {
		String errorMessage = String.format("<html><p>Please double check your file formats!</p></html>");
		JOptionPane.showMessageDialog(null, errorMessage, "Graph data error", JOptionPane.WARNING_MESSAGE);
	}



	/**
	* Utilities
	**/

	/*get file extension: after last '.' */
	public static final String getExtension(File file) {
		String ext = null;
		String filename = file.getName();
		int i = filename.lastIndexOf('.');
		if (i > 0 &&  i < filename.length() - 1) {
			ext = filename.substring(i).toLowerCase();
		}
		return ext;
	}
	/*get extension from filepath/filename string*/
	public static final String getExtension(String filename) {
		String ext = null;
		int i = filename.lastIndexOf('.');
		if (i > 0 &&  i < filename.length() - 1) {
			ext = filename.substring(i).toLowerCase();
		}
		return ext;
	}


	/*get file path with file name, no extension*/
	public static final String getPathNoExt(File file) {
		String path = null;
		String filepath = file.getAbsolutePath();
		int i = filepath.lastIndexOf('.');
		if (i > 0 && i < filepath.length() - 1) {
			path = filepath.substring(0, i);
		}
		else {
			path = filepath;
		}
		return path;
	}
	/*get file path with file name, no extension*/
	public static final String getPathNoExt(String filepath) {
		String path = null;
		int i = filepath.lastIndexOf('.');
		if (i > 0 && i < filepath.length() - 1) {
			path = filepath.substring(0, i);
		}
		else {
			path = filepath;
		}
		return path;
	}

	/*get file name without extension: name before last '.'*/
	public static final String getNameNoExt(File file) {
		String name = null;
		String filename = file.getName();
		int i = filename.lastIndexOf('.');
		if (i > 0 && i < filename.length() - 1) {
			name = filename.substring(0, i);
		}
		return name;
	}

	/**
	* check whether delimiter appears to be comma or tab:
	* assume the one with greatest frequency in first line is delimiter
	**/
	public static final char getDelimiter(String filepath) {
		File file = new File(filepath);
		char delimiter = getDelimiter(file);
		return delimiter;
	}
	public static final char getDelimiter(File file) {
		In test = new In(file);

		//get first line
		String firstLine = test.readLine();
		int tabCount = 0;
		int commaCount = 0;

		//close input stream
		test.close();

		//return comma by default
		char delimiter = ',';

		//iterate through string, add to tab or comma count
		for (int i = 0; i < firstLine.length(); i++) {
			char c = firstLine.charAt(i);
			if (c == ',') {
				commaCount++;
			}
			else if (c == '\t') {
				tabCount++;
			}
		}

		//if tabCount > commaCount, delimiter is tab; otherwise, remains as comma
		if (tabCount > commaCount) {
			delimiter = '\t';
		}
		else {
			delimiter = ',';
		}

		return delimiter;
	}


	/**
	* Get apparent number of columns in first row
	**/
	public static final int getColumnNumber(String filepath) {
		File file = new File(filepath);
		int cols = getColumnNumber(file);
		return cols;
	}
	public static final int getColumnNumber(File file) {
		int cols = 1;

		In test = new In(file);

		//get first line
		String firstLine = test.readLine();
		int tabCount = 0;
		int commaCount = 0;

		//close input stream
		test.close();

		//iterate through string, add to tab or comma count
		for (int i = 0; i < firstLine.length(); i++) {
			char c = firstLine.charAt(i);
			if (c == ',') {
				commaCount++;
			}
			else if (c == '\t') {
				tabCount++;
			}
		}

		//if tabCount > commaCount, delimiter is tab; otherwise, delimiter is comma
		//1 + delimiter count = number of columns
		if (tabCount > commaCount) {
			cols += tabCount;
		}
		else {
			cols += commaCount;
		}

		return cols;
	}


	/*get dataset name: file name before last '_' */
	public static final String getDataName(File file) {
		String name = getNameNoExt(file);
		String filename = file.getName();
		int i = filename.lastIndexOf('_');
		if (i > 0 && i < filename.length() - 1) {
			name = filename.substring(0, i);
		}
		return name;
	}

	/*get file 'type' (correlation, etc.): file name after last '_' before last '.'*/
	public static final String getFileType(File file) {
		String type = getNameNoExt(file);
		String filename = getNameNoExt(file);
		int i = filename.lastIndexOf('.');
		if (i > 0 && i < filename.length() - 1) {
			type = filename.substring(i, filename.length());
		}
		return type;
	}

}

