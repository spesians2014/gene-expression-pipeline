import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JOptionPane;
import org.rosuda.JRI.Rengine;
import javax.swing.filechooser.*;

public class Tab22 extends JPanel implements ActionListener, ItemListener {

    private final String PEARSON = "pearson";
    private final String SPEARMAN = "spearman";
    
    JPanel finalPanel;
    JPanel endPanel;
    JTabbedPane tp;
    Tab32 t32;
    
    File firstFile = null;
    JFileChooser fc = new JFileChooser();
    File secondFile = null;
    JFileChooser fc2 = new JFileChooser();
    
    JTextArea log;
    JButton browseButton;
    JCheckBox geneToProbe;
    JCheckBox aggData;
    JCheckBox downloadCorr;
    JCheckBox downloadPval;
    
    JTextArea log2;
    JButton browseButton2;
    JCheckBox geneToProbe2;
    JCheckBox aggData2;
    JCheckBox downloadCorr2;
    JCheckBox downloadPval2;
    
    JComboBox aggManip;
    JComboBox corrMethods;
    String method;
    String operation;
    
    JButton analyzeButton;
    JButton nextButton;
    JProgressBar progressBar;
    
    boolean g2p = true;
    boolean datadownload = true;
    boolean doDownloadCorr = false;
    boolean doDownloadPval = false;
    
    boolean g2p2 = true;
    boolean datadownload2 = true;
    boolean doDownloadCorr2 = false;
    boolean doDownloadPval2 = false;
    
    final static boolean shouldFill = true;
    final static boolean shouldWeightX = true;
    final static boolean RIGHT_TO_LEFT = false;
    
    
    public Tab22() {  

        // Create browsing log
        log = new JTextArea(3, 15);
        log.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(log);
        
        // Build browse button.
        browseButton = new JButton("Upload .soft or .csv data file");
        browseButton.addActionListener(this);
        
        // Add checkboxes for download options
        geneToProbe = new JCheckBox("Download Gene-Probe Map");
        geneToProbe.setSelected(true);
        geneToProbe.addItemListener(this);
        aggData = new JCheckBox("Download Aggregate Data");
        aggData.setSelected(true);
        aggData.addItemListener(this);
        downloadCorr = new JCheckBox("Download Correlation Matrix");
        downloadCorr.setSelected(false);
        downloadCorr.addItemListener(this);
        downloadPval = new JCheckBox("Download p-value Matrix");
        downloadPval.setSelected(false);
        downloadPval.addItemListener(this);
        
        // Create second browsing log
        log2 = new JTextArea(3, 15);
        log2.setEditable(false);
        JScrollPane logScrollPane2 = new JScrollPane(log2);
        
        // Build browse button.
        browseButton2 = new JButton("Upload .soft or .csv data file");
        browseButton2.addActionListener(this);
        
        // Add checkboxes for download options
        geneToProbe2 = new JCheckBox("Download Gene-Probe Map");
        geneToProbe2.setSelected(true);
        geneToProbe2.addItemListener(this);
        aggData2 = new JCheckBox("Download Aggregate Data");
        aggData2.setSelected(true);
        aggData2.addItemListener(this);
        downloadCorr2 = new JCheckBox("Download Correlation Matrix");
        downloadCorr2.setSelected(false);
        downloadCorr2.addItemListener(this);
        downloadPval2 = new JCheckBox("Download p-value Matrix");
        downloadPval2.setSelected(false);
        downloadPval2.addItemListener(this);
        
        
        // Build drop down menu for method of aggregation
        String[] aggOps = { "Select Method of Aggregation", "Mean", "Maximum Variance", "Maximum", "Minimum", "Maximum Average", "Minimum Average"};
        aggManip = new JComboBox(aggOps);
        aggManip.setSelectedIndex(0);
        aggManip.addActionListener(this);
        
        // Build drop down menu for method of correlation
        String[] corMeth = { "Select Correlation Method", "Pearson Correlation",
        "Spearman Correlation" };
        corrMethods = new JComboBox(corMeth);
        corrMethods.setSelectedIndex(0);
        corrMethods.addActionListener(this);
        
        // Build analyze button.
        analyzeButton = new JButton("Go!");
        analyzeButton.addActionListener(this);
        
        // Build progress bar
        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        
        //Build next button
        nextButton = new JButton("Next >>");
        nextButton.addActionListener(this);
        
        // Build panels and gridbaglayout
        finalPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        JPanel topPanel = new JPanel(new GridLayout(0, 2));
        topPanel.add(browseButton);
        topPanel.add(browseButton2);
        topPanel.add(logScrollPane);
        topPanel.add(logScrollPane2);
        topPanel.add(geneToProbe);
        topPanel.add(geneToProbe2);
        topPanel.add(aggData);
        topPanel.add(aggData2);
        topPanel.add(downloadCorr);
        topPanel.add(downloadCorr2);
        topPanel.add(downloadPval);
        topPanel.add(downloadPval2);
        
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        finalPanel.add(topPanel, c);
        
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        finalPanel.add(aggManip, c);
        
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 2;
        finalPanel.add(corrMethods, c);
        
        endPanel = new JPanel();
        endPanel.add(analyzeButton);
        endPanel.add(progressBar);
        
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 3;
        finalPanel.add(endPanel, c);
    }
    
    public void actionPerformed(ActionEvent e) {
        //Handle browse action
        if (e.getSource() == browseButton) {
            int returnVal = fc.showOpenDialog(Tab22.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                firstFile = fc.getSelectedFile();
                log.append(firstFile.getName() + ".\n");;
                t32.takeTabs(tp);
                t32.write1(firstFile);
                t32.updateList();
            } 
            log.setCaretPosition(log.getDocument().getLength());
        }
        
        //Handle browse action
        if (e.getSource() == browseButton2) {
            int returnVal = fc2.showOpenDialog(Tab22.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                secondFile = fc2.getSelectedFile();
                log2.append(secondFile.getName() + ".\n");
                t32.takeTabs(tp);
                t32.write2(secondFile);
            } 
            log2.setCaretPosition(log2.getDocument().getLength());
        }
        
        // handle aggManip dropdown action
        if (e.getSource() == aggManip) {
            operation = (String) aggManip.getSelectedItem();
        }
        
        // handle corrMethods dropdown action
        if (e.getSource() == corrMethods) {
            String string = (String)corrMethods.getSelectedItem();
            if (string.equals("Pearson Correlation"))
                method = DataAnalysis.PEARSON;
            else if (string.equals("Spearman Correlation"))
                method = DataAnalysis.SPEARMAN;
            else method = string;    
        }
        
        //if next button is pressed, go to tab 2
        if (e.getSource() == nextButton) {
            tp.setSelectedIndex(2);
        }
        
        //Handle analyze button action.
        if (e.getSource() == analyzeButton) {
            // if user has not uploaded file, send warning
            if (firstFile == null || secondFile == null) {
                JFrame frame0 = new JFrame();
                JOptionPane.showMessageDialog(frame0, "Please upload file.", 
                  "Warning", 
                  JOptionPane.WARNING_MESSAGE);
            }
            
            // if user has not selected method of aggregation, send warning
            else if (operation.equals("Select Method of Aggregation")) {
                JFrame frame1 = new JFrame();
                JOptionPane.showMessageDialog(frame1, "Please select method of aggregation.", "Warning",
                  JOptionPane.WARNING_MESSAGE);
            }
            
            // if user has not selected correlation method, send warning
            else if (method.equals("Select Correlation Method")) {
                JFrame frame2 = new JFrame();
                JOptionPane.showMessageDialog(frame2, "Please select method of " +
                  "correlation.", "Warning",
                  JOptionPane.WARNING_MESSAGE);   
            }
            //start analysis
            else {             
                analysis(firstFile);
                JFrame frame = new JFrame();
                JOptionPane.showMessageDialog(frame, "All computations for the first file are complete. Now proceeding to second file." ); 
                analysis(secondFile);
                JFrame frame0 = new JFrame();
                JOptionPane.showMessageDialog(frame0, "Done! All requested files are available in your directory." ); 
                endPanel.add(nextButton);
                analyzeButton.setEnabled(true);
            }
        }
    }
    
    
    // handle response to check boxes
    public void itemStateChanged(ItemEvent e) {
        Object source = e.getItemSelectable();
        if (source == geneToProbe && e.getStateChange() == ItemEvent.DESELECTED)
            g2p = false;
        if (source == aggData && e.getStateChange() == ItemEvent.DESELECTED) 
            datadownload = false;
        if (source == downloadCorr && e.getStateChange() == ItemEvent.DESELECTED)
            doDownloadCorr = false;
        if (source == downloadPval && e.getStateChange() == ItemEvent.DESELECTED)
            doDownloadPval = false;
        if (source == geneToProbe && e.getStateChange() == ItemEvent.SELECTED)
            g2p = true;
        if (source == aggData && e.getStateChange() == ItemEvent.SELECTED) 
            datadownload = true;
        if (source == downloadCorr && e.getStateChange() == ItemEvent.SELECTED) {
            doDownloadCorr = true;
            JFrame f = new JFrame();
            int reply = JOptionPane.showConfirmDialog(f, "Downloading the correlation matrix may be extremely time and memory intensive. \n Would you like to download anyway?", 
              "Warning", 
              JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
            if (reply == JOptionPane.CANCEL_OPTION) {
                downloadCorr.setSelected(false);
                doDownloadCorr = false;
                analyzeButton.setEnabled(true);
            }
        }
        if (source == downloadPval && e.getStateChange() == ItemEvent.SELECTED) {
            doDownloadPval = true;
            JFrame f = new JFrame();
            int reply = JOptionPane.showConfirmDialog(f, "Downloading the p-value matrix may be extremely time and memory intensive. \n Would you like to download anyway?", 
              "Warning", 
              JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
            if (reply == JOptionPane.CANCEL_OPTION) {
                downloadPval.setSelected(false);
                doDownloadPval = false;
                analyzeButton.setEnabled(true);
            }
        }
        
        if (source == geneToProbe2 && e.getStateChange() == ItemEvent.DESELECTED)
            g2p2 = false;
        if (source == aggData2 && e.getStateChange() == ItemEvent.DESELECTED) 
            datadownload2 = false;
        if (source == downloadCorr2 && e.getStateChange() == ItemEvent.DESELECTED)
            doDownloadCorr2 = false;
        if (source == downloadPval2 && e.getStateChange() == ItemEvent.DESELECTED)
            doDownloadPval2 = false;
        if (source == geneToProbe2 && e.getStateChange() == ItemEvent.SELECTED)
            g2p2 = true;
        if (source == aggData2 && e.getStateChange() == ItemEvent.SELECTED) 
            datadownload2 = true;
        if (source == downloadCorr2 && e.getStateChange() == ItemEvent.SELECTED) {
            doDownloadCorr2 = true;
            JFrame f = new JFrame();
            int reply = JOptionPane.showConfirmDialog(f, "Downloading the correlation matrix may be extremely time and memory intensive. \n Would you like to download anyway?", 
              "Warning", 
              JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
            if (reply == JOptionPane.CANCEL_OPTION) {
                downloadCorr2.setSelected(false);
                doDownloadCorr2 = false;
                analyzeButton.setEnabled(true);
            }
        }
        if (source == downloadPval2 && e.getStateChange() == ItemEvent.SELECTED) {
            doDownloadPval2 = true;
            JFrame f = new JFrame();
            int reply = JOptionPane.showConfirmDialog(f, "Downloading the p-value matrix may be extremely time and memory intensive. \n Would you like to download anyway?", 
              "Warning", 
              JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
            if (reply == JOptionPane.CANCEL_OPTION) {
                downloadPval2.setSelected(false);
                doDownloadPval2 = false;
                analyzeButton.setEnabled(true);
            }
        }
    }
    
    // analysis method
    public void analysis(File file) {
        setValue(5);
        
        //create DataAnalysis, process
        DataAnalysis da = new DataAnalysis(file, operation, method);
        setValue(15);
        da.processData();
        setValue(25);
        
        //GP-map and/or aggregated data downloads
        if (g2p) {
            da.outGPmap();
            setValue(35);
        }
        if (datadownload) {
            da.outAggData();
            setValue(45);
        }
        if (g2p || datadownload) {
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame, "The gene-probe map and/or aggregated data files are now available as .csv files in your directory.");
            setValue(50);
        }
        
        //warn user about time/memory usage
        JFrame f = new JFrame();
        int reply = JOptionPane.showConfirmDialog(f, "Calculating correlation and p-value matrices requires an estimated " + da.guessMem() + " of memory and may take several minutes. \n Would you like to continue?", "Warning", JOptionPane.YES_NO_OPTION);
        
        if (reply == JOptionPane.NO_OPTION) {
            analyzeButton.setEnabled(true);
            setValue(0);
        }
        
        //initiate analysis
        else if (reply == JOptionPane.YES_OPTION) {
            //correlate data
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            da.corrData();
            setValue(90);
            da.saveCorrData();
            setValue(95);
            
            // Update list on tab 3
            t32.takeTabs(tp);
            t32.updateList();
            t32.write1(firstFile);
            t32.write2(secondFile);
            
            setValue(100);
            JFrame frame2 = new JFrame();
            JOptionPane.showMessageDialog(frame2, "Correlation and p-value calculations complete!");
            
            //download correlation data
            if (doDownloadCorr || doDownloadPval) {
                JFrame f2 = new JFrame();
                int reply2 = JOptionPane.showConfirmDialog(f2, "Downloading the correlation and/or p-value matrix may be very time and memory intensive. \n Would you like to download anyway?", 
                 "Warning", 
                 JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                if (reply2 == JOptionPane.CANCEL_OPTION) {
                    downloadCorr.setSelected(false);
                    doDownloadCorr = false;
                    downloadPval.setSelected(false);
                    doDownloadPval = false;
                    analyzeButton.setEnabled(true);
                }
                else if (reply2 == JOptionPane.OK_OPTION) {
                    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    setValue(5);
                    
                    da.writeCorrData(doDownloadCorr, doDownloadPval);
                    
                    setValue(95);
                    
                    JFrame frame = new JFrame();
                    JOptionPane.showMessageDialog(frame, "The correlation and/or p-value matrices are available as .csv files in your directory.");
                    setValue(100);
                }
            } 

            setValue(0);
        }
        //clean up R space
        da.clearR();
    }
    
    // return panel
    public JPanel getPanel() {
        return finalPanel;
    }
    
    // transfer tabbed pane
    public void takeInfo(JTabbedPane tp, Tab32 t32) {
        this.tp = tp;
        this.t32 = t32;
    }
    
    
    public void setValue(int n) {
        progressBar.setValue(n);
        progressBar.paintImmediately(0, 0, progressBar.getSize().width, progressBar.getSize().height);
    }
}