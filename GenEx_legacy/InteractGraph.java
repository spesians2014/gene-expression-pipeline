/** InteractGraph
* creates graph, sends to GraphViewer, displays in GraphFramer
* 
* Implements GraphViewer colorEdges, colorVertices, shapeVertices,
* hideVertices, setLabels, hideLabels
*
* Depends: GraphViewer, GraphFramer 
**/

//graph libraries
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.algorithms.layout.*;
import edu.uci.ics.jung.visualization.*;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.screencap.Dump;
import edu.uci.ics.screencap.PNGDump;
import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.Transformer;

//GUI libraries
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.geom.*;
import java.awt.geom.Point2D;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.*;
import java.awt.event.*;

public class InteractGraph extends GraphViewer {
	/**
	* constants
	**/
	//colors
	protected static final Color EDGE_COLOR = Color.GRAY;
	protected static final double MAX_VERT = 24;
	protected static final double MIN_VERT = 8;
	protected static final double MAX_RED = 255;
	protected static final double MIN_RED = 0;

	/**
	* instance variables
	**/
	//graph construction
	protected int n; //largest possible number of vertices in graph
	protected int usedVertices;


	/**
	* constructor
	**/
	public InteractGraph() {
		;
	}
	public InteractGraph(int[] adj, String[] names, String framename) {
		//set default width, height, and frame name
		WIDTH = DEFAULT_WIDTH;
		HEIGHT = DEFAULT_HEIGHT;
		TITLE = framename;

		createGraph(adj, names); //create graph

		setAppearance(); //set graph appearance

		createAndShowGUI(false); //show GUI
	}


	/**
	* create graph (edges * vertices)
	**/	
	protected void createGraph(int[] adj, String[] names) {
		g = new UndirectedSparseGraph<String, Integer>();
		n = names.length;

		addVertices(names); //add all vertices

		addEdges(adj, names); //add all edges

		usedVertices = getUsedVertices(); //get number of used vertices
	}

	/*add vertices to graph*/
	protected void addVertices(String[] names) {
		for (int i = 0; i < n; i++) {
			g.addVertex(names[i]);
		}
	}

	/*add edges to graph*/
	protected void addEdges(int[] adj, String[] names) {
		int a = 0; //index in adjacency matrix
		for (int i = 0; i < n - 1; i++) {
			for (int j = i + 1; j < n; j++) {
				if (adj[a] > 0) {
					g.addEdge(a, names[i], names[j]);
				}
				a++;
			}
		}
	}

	/*count used vertices*/
	protected int getUsedVertices() {
		int u = 0;
		for (String v : g.getVertices()) {
			if (g.getIncidentEdges(v).size() > 0)
				u++;
		}
		return u;
	}


	/**
	* graph properties
	**/
	/*color edges*/
	protected void colorEdges() {
		edgePaint = 
		new Transformer<Integer, Paint>() {
			public Paint transform(Integer edge) {
				Color color = EDGE_COLOR;
				return color;
			}
		};
	}

	/*color vertices*/
	protected void colorVertices() {
		vertexPaint =
		new Transformer<String, Paint>() {
			public Paint transform(String vertex) {
				int number = g.getIncidentEdges(vertex).size() ;
				int red = (int) MIN_RED;
				int verts = usedVertices;
				if (usedVertices <= 0) {
					verts = 1;
				}
				double step = (double) ((MAX_RED - MIN_RED) / (double) verts);
				double scale = step * number + MIN_RED;
				red = (int) (MIN_RED + step * number);
				if (red > MAX_RED) red = (int) MAX_RED;
				Color color = new Color((int) red, 0, 0);
				return color;
			}
		};
	}

	/*shape vertices*/
	protected void shapeVertices() {
		vertexShape = 
		new Transformer<String, Shape>() {
			public Shape transform(String vertex) {
				int number = g.getIncidentEdges(vertex).size();
				int verts = usedVertices;
				if (usedVertices <= 0) {
					verts = 1;
				}
				double step = (double) ((MAX_VERT - MIN_VERT) / (double) verts);
				double size = step * number + MIN_VERT;
				if (size > MAX_VERT) size = MAX_VERT;
				Ellipse2D.Double ret = new Ellipse2D.Double(-size/2, -size/2, size,size);
				return ret;
			}
		};
	}

	/*hide vertices*/
	protected void hideVertices() {
		vertexShape =
		new Transformer<String, Shape>() {
			public Shape transform(String vertex) {
				int number = g.getIncidentEdges(vertex).size();
				int verts = usedVertices;
				if (usedVertices == 0) {
					verts = 1;
				}
				double step = (double) ((MAX_VERT - MIN_VERT) / (double) verts);
				double scale = step * number + MIN_VERT; 
				if (number == 0) {
					scale = 0;
				}
				Ellipse2D.Double ret = new Ellipse2D.Double(-scale/2, -scale/2, scale,scale);
				return ret;
			}
		};
	}

	/*add labels*/
	protected void setLabels() {
		vertexLabel = 
		new ToStringLabeller<String>();
	}

	/*hide labels*/
	protected void hideLabels() {
		vertexLabel = 
		new Transformer<String, String>() {
			public String transform(String vertex) {
				if (g.getIncidentEdges(vertex).size() == 0)
					vertex = "";
				return vertex;
			}
		};
	}


	/**
	* test main
	**/
	public static void main(String[] args) {
		int[] arr = new int[]{1 ,0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0};
		String[] names = new String[]{"A", "B", "C", "D", "E", "F"};
		String title = "Hello";
		InteractGraph ig = new InteractGraph(arr, names, title);
	}
}
