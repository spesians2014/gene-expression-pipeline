/**GraphAnalysis
  *Input: user input name of GEO GDS SOFT file (NAME.soft), p-value, tau value (network threshold), list of genes
  *Output: 
  *
  *Dependencies: java.io, javax.swing, JRI.Rengine, GraphFunctions.R, stdlib, InteractGraph, Analysis
  *In order to run test main, must have correlated GDS4457.soft through DataAnalysis
  *
  *Compilation: javac -cp LIBRARIES:. GraphAnalysis.java
  *Execution: ./run -cp LIBRARIES:. GraphAnalysis
  **/

/*import libraries*/

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.filechooser.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import org.rosuda.JRI.Rengine;
import org.rosuda.JRI.REXP;

import edu.princeton.cs.introcs.*;


public class GraphAnalysis extends Analysis {
    //default values
    private final double NONEMPTY = 1.0;
    public static final double STDP = 0.05;
    public static final double STDTAU = 0.9;
    //for two-graph comparison
    public static final int BOTH = 3;
    public static final int G1 = 1;
    public static final int G2 = 2;
    public static final int NONE = 0;

    /*instance variables*/
    //rengine inherited from Analysis
    //R variable names
    private String fileName;
    static String corrEnv;
    private String adjEnv;
    private String adjVec;
    //user arguments
    private String[] geneList;
    private double pval;
    private double tau;
    //boolean flags
    private boolean didAdjacency;
    
    /*constructors*/
    /*call to preload autocomplete list of genes from tab2*/
    public GraphAnalysis(File softfile) {
        //start R
        super();

        pval = STDP;
        tau = STDTAU;
        geneList = null;
        didAdjacency = false;
        fileName = null;
        corrEnv = null;
        adjEnv = null;
        adjVec = null;

        try {
            setRVarNames(softfile);
            re.eval(corrEnv + " <- new.env()");
            //actually load data
            loadRDSFromTemplate(softfile.getAbsolutePath());
        }
        catch (NullPointerException e) {
            throw new NullPointerException();
        }
    }
    
    /*call to initiate GraphAnalysis that will result in a graph*/
    public GraphAnalysis(File softfile, File corrfile, File pfile, File indexfile) {
        //start R
        super();

        //set some instance variables
        pval = STDP;
        tau = STDTAU;
        geneList = null;
        didAdjacency = false;
        fileName = null;
        corrEnv = null;
        adjEnv = null;
        adjVec = null;

        //load data
        //from inherited file
        try {
            if (softfile != null && (corrfile == null && pfile == null)) {
            //R variable names
                setRVarNames(softfile);
                re.eval(corrEnv + " <- new.env()");
            //actually load data
                loadRDSFromTemplate(softfile.getAbsolutePath());
            }
        //from .csv
            else if (corrfile != null && pfile != null && getExtension(corrfile).equals(".csv") && getExtension(pfile).equals(".csv")) {
            //R variable names
                setRVarNames(corrfile);
                re.eval(corrEnv + " <- new.env()");
            //actually load data
                loadCSVData(corrfile.getAbsolutePath(), pfile.getAbsolutePath());
            }
        //from .rds
            else if (corrfile != null && pfile != null && indexfile != null && getExtension(corrfile).equals(".rds") && getExtension(pfile).equals(".rds") && getExtension(indexfile).equals(".rds")) {
            //R variable names
                setRVarNames(corrfile);
                re.eval(corrEnv + " <- new.env()");
            //actually load data
                loadRDSData(corrfile.getAbsolutePath(), pfile.getAbsolutePath(), indexfile.getAbsolutePath());
            }
            else {
                throw new NullPointerException();
            }
        }
        catch (NullPointerException e) {
            noCorrelatedDataWarning();
        }
    }


    /*constructor helper: set R variable names*/
    private void setRVarNames(File f) {
        fileName = noExtension(f);
        corrEnv = "A" + fileName + "corrEnv";
        adjEnv = "A" + fileName + "adjEnv";
        adjVec = adjEnv + "$adjVec";
    }

    /*constructor helper: load correlation data SOFT file*/
    public static void loadRDSFromTemplate(String softname) {
        if (checkTemplateFiles(softname)) {
            String load = String.format("%s <- loadRDSFromTemplate(\"%s\")", corrEnv, softname);
            re.eval(load);
        }
    }

    /*constructor helper: load correlation data from CSV files*/
    public static void loadCSVData(String corrName, String pName) {
        String newEnv = String.format("%s <- loadCSVData(\'%s\', \'%s\')", corrEnv, corrName, pName);
        re.eval(newEnv);
    }

    /*constructor helper: load correlation data from RDS files*/
    public static void loadRDSData(String corrName, String pName, String indexName) {
        String newEnv = String.format("%s <- loadRDSData(\'%s\', \'%s\', \'%s\')", corrEnv, corrName, pName, indexName);
        re.eval(newEnv);
    }

    /*constructor helper: check if previously correlated RDS data is in directory*/
    public static boolean checkTemplateFiles(String softname) {
        String corrPath = softname + "_correlations.rds";
        String pPath = softname + "_p_vals.rds";
        String indexPath = softname + "_gene_index.rds";

        boolean corrFile = new File(corrPath).isFile();
        boolean pFile = new File(pPath).isFile();
        boolean indexFile = new File(indexPath).isFile();

        boolean didCorrelate = false;

        if (corrFile & pFile & indexFile) {
            didCorrelate = true;
        }
        else {
            throw new NullPointerException();
        }
        return didCorrelate;
    }

    /*no correlated data warning*/
    private static void noCorrelatedDataWarning() {
        JFrame frame = new JFrame();
        JOptionPane.showMessageDialog(frame, "Please make sure you have correlated this dataset with GenEx, \n or upload both correlation and p-value matrices.", "Warning", JOptionPane.WARNING_MESSAGE);
        throw new NullPointerException();
    }

    /*check if correlated data is loaded*/
    public boolean isCorrLoaded() {
        boolean isLoaded = false;
        String check = String.format("if (!identical(%s$corrVec, NULL)) flag <- 1 else flag <- 0", corrEnv);
        re.eval(check);
        Double flag = re.eval("flag").asDouble();
        if (flag == NONEMPTY)
            isLoaded = true;
        else {
            noCorrelatedDataWarning();
        }
        return isLoaded;
    }


    /*change geneList*/
    public void changeGeneList(String[] list) {
        this.geneList = list;
    }

    /*change p value*/
    public void changePval(double p) {
        this.pval = p;
    }

    /*change tau*/
    public void changeTau(double t) {
        this.tau = t;
    }

    /*return filename*/
    public String fileName() {
        return fileName;
    }


    /*get gene names as ArrayList*/
    public ArrayList<String> getGenes() {
        ArrayList<String> list = null;
        if (isCorrLoaded()) {
            String getCorrEnv = String.format("%s$index", corrEnv);
            String[] geneNames = re.eval(getCorrEnv).asStringArray();
            list = new ArrayList<String>();
            for (int i = 0; i < geneNames.length; i++) {
                list.add(geneNames[i]);
            }
        }
        return list;
    }


    /*preload N most correlated genes*/
    public String[] preloadGenes(int n) {
        //get most correlated genes
        String[] preload = null;
        if (isCorrLoaded()) {
            String get = String.format("preload <- getBigGenes(%s$corrVec, %s$index, %d)", corrEnv, corrEnv, n);
            re.eval(get);
            preload = re.eval("preload").asStringArray();
            this.changeGeneList(preload);
        }
        return preload;
    }


    /*get 100 random genes*/
    public String[] randomGenes(int n) {
        String[] random = null;
        if (isCorrLoaded()) {
            String get = String.format("randgenes <- sample(%s$index, %d, replace = TRUE)", corrEnv, n);
            re.eval(get);
            random = re.eval("randgenes").asStringArray();
            this.changeGeneList(random);
        }
        return random;
    }


    /*generate adjacency matrix (as triangular matrix vector) from gene list, p-value, and network threshold*/
    public void adjacency() {
        if (isCorrLoaded() && (geneList != null)) {
            re.assign("geneList", geneList);
            String adj = String.format("%s <- adjacency(geneList, %f, %f, corrEnv = %s)", adjEnv, pval, tau, corrEnv);
            re.eval(adj);
            didAdjacency = true;
            cullGeneList();
        }
        else if (geneList == null)
            genesNotFoundWarning();
    }


    /*update gene list to get rid of genes not in dataset*/
    public void cullGeneList() {
        if (didAdjacency) {
            String list = adjEnv+ "$geneList";
            String[] getNewList = re.eval(list).asStringArray();
            changeGeneList(getNewList);
        }
    }


    /*genes not found error*/
    private static void genesNotFoundWarning() {
        JFrame frame = new JFrame();
        JOptionPane.showMessageDialog(frame, "The genes you specified were not found in this dataset. \n Please try a different set of genes.", 
          "Warning", 
          JOptionPane.WARNING_MESSAGE);
        throw new NullPointerException();
    }


    /*check if adjacency matrix is empty*/
    public boolean isAdjEmpty() {
        boolean isEmpty = true;
        try {
            double[] hi = re.eval(adjVec).asDoubleArray();
            for (int i = 0; i < hi.length; i++)
                hi[i] = 1;
            isEmpty = false;
        }
        catch (NullPointerException e) {
            isEmpty = true;
            genesNotFoundWarning();
        }
        return isEmpty;
    }


    /*get adjacency matrix (as triangular matrix vector) as double array*/
    public int[] getAdjVec() {
        if (didAdjacency && !isAdjEmpty()) {
            double[] getAdj = re.eval(adjVec).asDoubleArray();
            int[] getAdjInt = new int[getAdj.length];
            for (int i = 0; i < getAdj.length; ++i)
                getAdjInt[i] = (int) getAdj[i];
            return getAdjInt;
        }
        else {
            return null;
        }
    }


    /*generate single network graph*/
    public void graph() {
        if (!isAdjEmpty()) {
            int[] adjVec = this.getAdjVec();
            if (adjVec.length < 1) {
                genesNotFoundWarning();
                return;
            }
            else {
                InteractGraph ig = new InteractGraph(this.getAdjVec(), geneList, fileName);
            }
        }
    }


    /*compare adjacency matrix/vectors of two GraphAnalysis*/
    public int[] compareGraphs(int[] adjVec2) {
        int[] adjVec1 = this.getAdjVec();
        int N = adjVec1.length;

        //check gene length
        if (adjVec2.length != N) {
            genesNotFoundWarning();
            return null;
        }

        int[] compVec = new int[N];

        for (int i = 0; i < N; i++) {
            if ((adjVec2[i] == 1) && (adjVec1[i] == 1))
                compVec[i] = BOTH;
            else if (adjVec1[i] == 1)
                compVec[i] = G1;
            else if (adjVec2[i] == 1)
                compVec[i] = G2;
            else
                compVec[i] = NONE;
        }
        return compVec;  
    }


    /*generate graphs of unions and unique portions of this graph and another*/
    public void multiGraph(int[] adjVec2, String fileName2) {
        if (!isAdjEmpty()) {
            int[] compVec = this.compareGraphs(adjVec2);

            if (compVec != null) {
            //generate double graph
                DoubleGraph dg;
                dg = new DoubleGraph(compVec, geneList, fileName, fileName2);

            //generate unique graph1
                SingleGraph sg1;
                sg1 = new SingleGraph(compVec, geneList, fileName, G1);

            //generate unique graph2
                SingleGraph sg2;
                sg2 = new SingleGraph(compVec, geneList, fileName2, G2);
            }

            else {
                genesNotFoundWarning();
                return;
            }
        }
    }


    /*remove ALL objects from R*/
    /*careful! this should ONLY be called after saving/writing all data*/
    public void clearR() {
        super.clearR();
        didAdjacency = false;
    }


    /*MAIN*/
    public static void main(String[] args) { 
        File file = new File("GDS4457.soft");
        File cor = new File("gds2771_cancer.csv_correlations.rds");
        File p = new File("gds2771_cancer.csv_p_vals.rds");
        File genes = new File("gds2771_cancer.csv_gene_index.rds");


        Stopwatch timer = new Stopwatch();
        GraphAnalysis g = new GraphAnalysis(file, cor, p, genes);
        System.out.println("GraphAnalysis created after " + timer.elapsedTime() + " seconds.");

        int m = 50;
        String[] genes2 = g.preloadGenes(m);
        System.out.println("Random genes generated after " + timer.elapsedTime() + " seconds.");

        g.adjacency();
        System.out.println("Adjacency matrix generated after " + timer.elapsedTime() + " seconds.");

        int[] lol = g.getAdjVec();
        int[] joker = {8, 12, 355, 1212, 1203, 883, 810, 452, 454, 18, 294, 854, 354, 894, 835, 234, 543, 654, 453, 653, 584, 999, 249, 236, 982};
        for (int i = 0; i < joker.length; i++)
            lol[joker[i]] = 0;
        int[] joker2 = {94, 134, 452, 532, 995, 903, 289, 990, 923, 653, 29, 524, 545, 77, 888, 666, 230, 660, 340, 200, 993, 555, 356};
        for (int i = 0; i < joker2.length; i++)
            lol[joker2[i]] = 1;

        g.graph();

        g.multiGraph(lol, "GDS4457, randomly changed");
        //System.out.println("Function graphed.");

        g.endR();  
    }
}