interface PreGraph {
	void outGPmap();

	void outAggData();

	String guessMem();

	void corrData();

	void saveCorrData();

	void writeCorrData(boolean a, boolean b);	

	void clearR();
	

	default void processData() {
		;
	}

	default void aggregate() {
		;
	}

	default void makeDAs(int s, String a, String b) {
		;
	}

	default String[] getVariables() {
		String[] a = new String[1];
		return a;
	}


}