import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JOptionPane;
import org.rosuda.JRI.Rengine;
import javax.swing.filechooser.*;

public class Tab2 extends JPanel implements ActionListener, ItemListener {

    JButton browseButton;
    JTextArea log;
    JFileChooser fc = new JFileChooser();
    
    JButton analyzeButton;
    JButton nextButton;
    JComboBox aggManip;
    String operation;
    JProgressBar progressBar;
    
    boolean g2p = true;
    boolean datadownload = true;
    boolean doDownloadCorr = false;
    boolean doDownloadPval = false;
    JCheckBox geneToProbe;
    JCheckBox aggData;
    JCheckBox downloadCorr;
    JCheckBox downloadPval;
    
    JPanel choices;
    File file;
    String method;
    JComboBox corrMethods;
    JTabbedPane tp;
    JPanel finalPanel;
    JPanel browsePanel;
    JPanel firstPanel;
    JPanel secondPanel;
    
    JComboBox varChoices;
    int doSeparate;
    PreGraph a;

    public Tab2() {

        // initialize instance variables
        doSeparate = -1;
        operation = "Select Method of Aggregation";
        file = null;
        method = "Select Correlation Method";
        
        // Create browsing log
        log = new JTextArea(2,20);
        log.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(log);
        
        // Build browse button.
        browseButton = new JButton("Upload .soft or .csv data file");
        browseButton.addActionListener(this);
        
        // Add browse button and log to panel
        browsePanel = new JPanel();
        browsePanel.add(browseButton);
        browsePanel.add(logScrollPane);
        
        // Build analyze button.
        analyzeButton = new JButton("Go!");
        analyzeButton.addActionListener(this);
        
        //Build next button
        nextButton = new JButton("Next >>");
        nextButton.addActionListener(this);

        // Build drop down menu for analyzing data in subgroups
        String vars = "Please select a variable to split the data:";
        varChoices = new JComboBox();
        varChoices.addItem(vars);
        varChoices.setSelectedIndex(0);
        varChoices.addActionListener(this);
        varChoices.setVisible(false);
        
        // Build drop down menu for method of aggregation
        String[] aggOps = { "Select Method of Aggregation", "Mean", "Maximum Variance", "Maximum", "Minimum", "Maximum Average", "Minimum Average"};
        aggManip = new JComboBox(aggOps);
        aggManip.setSelectedIndex(0);
        aggManip.addActionListener(this);
        
        // Build drop down menu for method of correlation
        String[] corMeth = { "Select Correlation Method", "Pearson Correlation",
        "Spearman Correlation" };
        corrMethods = new JComboBox(corMeth);
        corrMethods.setSelectedIndex(0);
        corrMethods.addActionListener(this);
        
        // Build progress bar
        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        
        // Add checkboxes for download options
        geneToProbe = new JCheckBox("Download Gene-Probe Map");
        geneToProbe.setSelected(true);
        geneToProbe.addItemListener(this);
        aggData = new JCheckBox("Download Aggregate Data");
        aggData.setSelected(true);
        aggData.addItemListener(this);
        downloadCorr = new JCheckBox("Download Correlation Matrix");
        downloadCorr.setSelected(false);
        downloadCorr.addItemListener(this);
        downloadPval = new JCheckBox("Download p-value Matrix");
        downloadPval.setSelected(false);
        downloadPval.addItemListener(this);
        
        // Add content
        firstPanel = new JPanel();
        firstPanel.add(geneToProbe);
        firstPanel.add(aggData);
        secondPanel = new JPanel();
        secondPanel.add(downloadCorr);
        secondPanel.add(downloadPval);
        finalPanel = new JPanel();
        finalPanel.add(analyzeButton);
        finalPanel.add(progressBar);
        
        choices = new JPanel(new GridLayout(0, 1));
        choices.add(browsePanel);
        choices.add(firstPanel);
        choices.add(secondPanel);
        choices.add(varChoices);
        choices.add(aggManip);
        choices.add(corrMethods);
        choices.add(finalPanel);       
    }
    
    public void actionPerformed(ActionEvent e) {

        //handle varChoices dropdown action
        if (e.getSource() == varChoices) {
            doSeparate = varChoices.getSelectedIndex();
        }

        // handle aggManip dropdown action
        if (e.getSource() == aggManip) {
            operation = (String) aggManip.getSelectedItem();
        }
        
        // handle corrMethods dropdown action
        if (e.getSource() == corrMethods) {
            String string = (String)corrMethods.getSelectedItem();
            if (string.equals("Pearson Correlation"))
                method = DataAnalysis.PEARSON;
            else if (string.equals("Spearman Correlation"))
                method = DataAnalysis.SPEARMAN;
            else method = string;    
        }        
        
        //if next button is pressed, go to tab 3
        if (e.getSource() == nextButton) {
            tp.setSelectedIndex(2);
        }
        
        //Handle browse action
        if (e.getSource() == browseButton) {
            int returnVal = fc.showOpenDialog(Tab2.this);

            //Try to send data to tab3
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                file = fc.getSelectedFile();
                log.append("Opened: " + file.getName() + ".\n");
                Tab3 t3 = new Tab3();
                t3.takeTabs(tp);
                t3.getFile(file);
                t3.updateList();
                
                if (t3.preloadGraphAnalysis) {
                    JFrame t3f = new JFrame();
                    int skip = JOptionPane.showConfirmDialog(t3f, "Looks like you have correlated this dataset before! \n Would you like to continue immediately to filtering & graphing?", "Already Correlated", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (skip == JOptionPane.YES_OPTION) {
                        //skip to tab 3!
                        tp.setSelectedIndex(2);
                    }
                }
            } 
            log.setCaretPosition(log.getDocument().getLength());

            //try to split data by phenotype
            try {
                if (VarAnalysis.getExtension(file).equals(".soft")) {
                    JFrame varLoad = new JFrame();
                    int doLoad = JOptionPane.showConfirmDialog(varLoad, "Would you like to split this dataset by sample attributes (e.g., patient phenotype)?", "Split Dataset?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (doLoad == JOptionPane.YES_OPTION) {
                        varChoices.setVisible(true);
                        a = new VarAnalysis(file);
                        String[] manyVars = a.getVariables();
                        for (int i = 0; i < manyVars.length; i++)
                            varChoices.addItem(manyVars[i]);
                        varChoices.setSelectedIndex(0);
                    }
                    else {
                        varChoices.setVisible(false);
                        doSeparate = -1;
                    }    
                }
            }
            catch (NullPointerException expt) {
                ;
            }
        }

        //Handle analyze button action.
        if (e.getSource() == analyzeButton) {
            // if user has not uploaded file, send warning
            if (file == null) {
                JFrame frame0 = new JFrame();
                JOptionPane.showMessageDialog(frame0, "Please upload file.", 
                  "Warning", 
                  JOptionPane.WARNING_MESSAGE);
            }

            // if user has not selected method of aggregation, send warning
            else if (operation.equals("Select Method of Aggregation")) {
                JFrame frame1 = new JFrame();
                JOptionPane.showMessageDialog(frame1, "Please select method of aggregation.", "Warning",
                  JOptionPane.WARNING_MESSAGE);
            }

            // if user has not selected correlation method, send warning
            else if (method.equals("Select Correlation Method")) {
                JFrame frame2 = new JFrame();
                JOptionPane.showMessageDialog(frame2, "Please select method of " +
                  "correlation.", "Warning",
                  JOptionPane.WARNING_MESSAGE);   
            }
            //start analysis
            else {
                setValue(5);


            //check if user wants to split data
                if (doSeparate > 0) {
                    a.makeDAs(doSeparate, operation, method);
                    a.aggregate();
                }
                else {
                //create DataAnalysis, process
                    a = new DataAnalysis(file, operation, method);
                    setValue(15);
                    a.processData();
                }

                setValue(25);

                //GP-map and/or aggregated data downloads
                if (g2p) {
                    a.outGPmap();
                    setValue(35);
                }
                if (datadownload) {
                    a.outAggData();
                    setValue(45);
                }
                if (g2p || datadownload) {
                    JFrame frame = new JFrame();
                    JOptionPane.showMessageDialog(frame, "The gene-probe map and/or aggregated data files are now available as .csv files in your directory.");
                    setValue(50);
                }

                //warn user about time/memory usage
                JFrame f = new JFrame();
                int reply = JOptionPane.showConfirmDialog(f, "Calculating correlation and p-value matrices requires an estimated " + a.guessMem() + " of memory and may take several minutes. \n Would you like to continue?", "Warning", JOptionPane.YES_NO_OPTION);

                if (reply == JOptionPane.NO_OPTION) {
                    analyzeButton.setEnabled(true);
                    setValue(0);
                }

                //initiate analysis
                else if (reply == JOptionPane.YES_OPTION) {
                    //correlate data
                    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    a.corrData();
                    setValue(85);

                    a.saveCorrData();
                    setValue(90);

                    // Update list on tab 3
                    Tab3 t3 = new Tab3();
                    t3.takeTabs(tp);
                    t3.getFile(file);
                    t3.updateList();
                    setValue(95);

                    setValue(100);
                    JFrame frame2 = new JFrame();
                    JOptionPane.showMessageDialog(frame2, "Correlation and p-value calculations complete!");

                    //download correlation data
                    if (doDownloadCorr || doDownloadPval) {
                        JFrame f2 = new JFrame();
                        int reply2 = JOptionPane.showConfirmDialog(f2, "Downloading the correlation and/or p-value matrix may be very time and memory intensive. \n Would you like to download anyway?", 
                         "Warning", 
                         JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                        if (reply2 == JOptionPane.CANCEL_OPTION) {
                            downloadCorr.setSelected(false);
                            doDownloadCorr = false;
                            downloadPval.setSelected(false);
                            doDownloadPval = false;
                            analyzeButton.setEnabled(true);
                        }
                        else if (reply2 == JOptionPane.OK_OPTION) {
                            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                            setValue(5);
                            a.writeCorrData(doDownloadCorr, doDownloadPval);
                            setValue(95);
                            JFrame frame = new JFrame();
                            JOptionPane.showMessageDialog(frame, "The correlation and/or p-value matrices are available as .csv files in your directory.");
                            setValue(100);
                        }
                    } 
                    setValue(0);
                    finalPanel.add(nextButton);
                    analyzeButton.setEnabled(true);
                }
                //clean up R space
                a.clearR();
            }
        }
    }

    // handle response to check boxes
    public void itemStateChanged(ItemEvent e) {
        Object source = e.getItemSelectable();
        if (source == geneToProbe && e.getStateChange() == ItemEvent.DESELECTED)
            g2p = false;
        if (source == aggData && e.getStateChange() == ItemEvent.DESELECTED) 
            datadownload = false;
        if (source == downloadCorr && e.getStateChange() == ItemEvent.DESELECTED)
            doDownloadCorr = false;
        if (source == downloadPval && e.getStateChange() == ItemEvent.DESELECTED)
            doDownloadPval = false;
        if (source == geneToProbe && e.getStateChange() == ItemEvent.SELECTED)
            g2p = true;
        if (source == aggData && e.getStateChange() == ItemEvent.SELECTED) 
            datadownload = true;
        if (source == downloadCorr && e.getStateChange() == ItemEvent.SELECTED) {
            doDownloadCorr = true;
            JFrame f = new JFrame();
            int reply = JOptionPane.showConfirmDialog(f, "Downloading the correlation matrix may be extremely time and memory intensive. \n Would you like to download anyway?", 
              "Warning", 
              JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
            if (reply == JOptionPane.CANCEL_OPTION) {
                downloadCorr.setSelected(false);
                doDownloadCorr = false;
                analyzeButton.setEnabled(true);
            }
        }
        if (source == downloadPval && e.getStateChange() == ItemEvent.SELECTED) {
            doDownloadPval = true;
            JFrame f = new JFrame();
            int reply = JOptionPane.showConfirmDialog(f, "Downloading the p-value matrix may be extremely time and memory intensive. \n Would you like to download anyway?", 
              "Warning", 
              JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
            if (reply == JOptionPane.CANCEL_OPTION) {
                downloadPval.setSelected(false);
                doDownloadPval = false;
                analyzeButton.setEnabled(true);
            }
        }
    }


    // return panel
    public JPanel getPanel() {
        return choices;
    }

    public void setValue(int n) {
        progressBar.setValue(n);
        progressBar.paintImmediately(0, 0, progressBar.getSize().width, progressBar.getSize().height);
    }

    // transfer tabbed pane
    public void takeTabs(JTabbedPane tp) {
        this.tp = tp;
    }
}