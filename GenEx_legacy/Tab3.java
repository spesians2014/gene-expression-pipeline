// ./run -cp Import/*:. GUI

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.SwingUtilities;
import javax.swing.JOptionPane;
import javax.swing.event.*;
import java.beans.*;
import org.rosuda.JRI.Rengine;
import java.util.ArrayList;
import javax.swing.filechooser.*;
import edu.princeton.cs.introcs.*;


public class Tab3 extends JPanel implements ActionListener, ListSelectionListener {

    //accessible from tab 2: can we skip straight to tab3?
    public boolean preloadGraphAnalysis; 

    private static final String addString = "Add";
    private static final String removeString = "Remove";
    private static final String clearString = "Clear";
    
    // P-Value and threshold value
    protected JTextField textfield;
    protected JTextField threshfield;
    private double pValue;
    private double threshold;
    
    // Buttons
    private JButton addButton;
    private JButton removeButton;
    private JButton clearButton;
    private JButton browseGeneButton;
    private JButton browseCorrButton;
    private JButton browsePButton;
    private JButton browseIndexButton;
    private JButton analyzeButton;
    private JButton loadButton;
    
    private ArrayList<String> alist;
    
    private JPanel choices;
    private static File softFile = null;
    private File corrFile = null;
    private File pValFile = null;
    private File indexFile = null;
    private JFileChooser corrFC = new JFileChooser();
    private JFileChooser pValFC = new JFileChooser();
    private JFileChooser indexFC = new JFileChooser();
    
    private File geneFile;
    private JFileChooser genesFC = new JFileChooser();
    
    private Java2sAutoTextField genes;
    private JTextField corrN;
    private JProgressBar progressBar;
    private JList list;
    private DefaultListModel listModel;
    
    private JPanel listGUI;
    private JPanel panel3;
    private JScrollPane listScrollPane;
    private JPanel panel6;
    private JPanel panel7;
    private JPanel panel;

    private JPanel browsePanel;
    private JPanel browsePanel2;
    private JTextArea log;
    private JTabbedPane tp;
    
    public Tab3() {
        preloadGraphAnalysis = false;

        // Labels for text fields
        String pval = "P-Value Filter";
        String thresh = "Network Theshold";
        
        // Create browsing log on top
        log = new JTextArea(2,18);
        log.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(log);
        
        // Build correlation file browser button.
        browseCorrButton = new JButton("Upload correlations");
        browseCorrButton.addActionListener(this);
        
        // Build pval file browser button.
        browsePButton = new JButton("Upload p-values");
        browsePButton.addActionListener(this);

        // Build index file browser button.
        browseIndexButton = new JButton("Upload gene index");
        browseIndexButton.addActionListener(this);

        // Build browse panel label
        JLabel browseLabel = new JLabel("<html>Use .csv files: Upload correlation and p-value files <br>Use .rds files: Upload correlation, p-value, and gene index files <br> Inherit data from previous step: Upload nothing.</html>");
        
        // Add browse buttons and log to panel
        browsePanel = new JPanel();
        browsePanel2 = new JPanel();
        browsePanel.add(browseCorrButton);
        browsePanel.add(browsePButton);
        browsePanel2.add(browseIndexButton);
        browsePanel.add(logScrollPane);
        browsePanel2.add(browseLabel);
        
        // create text field for p-values
        textfield = new JTextField("0.05", 5);
        textfield.setActionCommand(pval);
        textfield.addActionListener(this);
        
        // input area for theshold value
        threshfield = new JTextField("0.9", 5);
        threshfield.setActionCommand(thresh);
        threshfield.addActionListener(this);
        
        // input area for p-value label
        JLabel textfieldLabel = new JLabel(pval + ": ");
        textfieldLabel.setLabelFor(textfield);
        JLabel threshfieldLabel = new JLabel(thresh + ": ");
        threshfieldLabel.setLabelFor(threshfield);
        
        // Build analyze button.
        analyzeButton = new JButton("Go!");
        analyzeButton.addActionListener(this);
        
        // Build progress bar
        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        
        // Build gene list
        listModel = new DefaultListModel();
        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.addListSelectionListener(this);
        list.setVisibleRowCount(6);
        listScrollPane = new JScrollPane(list);
        
        // Build add button
        addButton = new JButton(addString);
        AddListener addListener = new AddListener(addButton);
        addButton.setActionCommand(addString);
        addButton.addActionListener(addListener);
        addButton.setEnabled(false);
        
        // Build remove button
        removeButton = new JButton(removeString);
        removeButton.setActionCommand(removeString);
        removeButton.addActionListener(new RemoveListener());
        removeButton.setEnabled(false);
        
        //build clear button
        clearButton = new JButton(clearString);
        removeButton.setActionCommand(removeString);
        clearButton.addActionListener(this);
        clearButton.setEnabled(false);
        
        // Build text field for entering genes
        alist = new ArrayList<String>();
        alist.add("Please upload file to begin");    
        genes = new Java2sAutoTextField(alist);
        
        genes.addActionListener(addListener);
        genes.getDocument().addDocumentListener(addListener);
        JLabel geneLabel = new JLabel("                                " +
          "Enter genes to include in graph network");
        
        // Build label + text field for pre-loading genes
        JLabel fileLabela = new JLabel("and/or load ");
        corrN = new JTextField("50", 3);
        JLabel fileLabelb = new JLabel(" most correlated genes");
        
        // Build Load Button.
        loadButton = new JButton("Load...");
        loadButton.addActionListener(this);
        
        // Build file chooser for uploading genes
        JLabel fileLabel2 = new JLabel("and/or upload file of gene names, one gene per line");
        
        // Build gene browse button.
        browseGeneButton = new JButton("Browse Files...");
        browseGeneButton.addActionListener(this);
        
        // Add content
        choices = new JPanel();
        choices.setLayout(new BoxLayout(choices, BoxLayout.Y_AXIS));  
        panel = new JPanel();
        panel.add(analyzeButton);
        panel.add(progressBar);
        JPanel textfieldPanel= new JPanel();
        textfieldPanel.add(textfieldLabel);
        textfieldPanel.add(textfield);
        JPanel threshfieldPanel = new JPanel();
        threshfieldPanel.add(threshfieldLabel);
        threshfieldPanel.add(threshfield);
        
        listGUI = new JPanel(new GridLayout(0, 2));
        listGUI.add(textfieldPanel);
        listGUI.add(threshfieldPanel);
        
        panel3 = new JPanel(new GridLayout(0, 1));
        panel3.add(geneLabel);
        JPanel panel4 = new JPanel(new GridLayout(0, 1));
        panel4.add(addButton);
        panel4.add(removeButton);
        panel4.add(clearButton);
        panel4.add(genes);
        JPanel panel5 = new JPanel(new GridLayout(0, 2));
        panel5.add(panel4);
        panel5.add(listScrollPane);
        panel6 = new JPanel();
        panel6.add(fileLabela);
        panel6.add(corrN);
        panel6.add(fileLabelb);
        panel6.add(loadButton);
        panel7 = new JPanel();
        panel7.add(fileLabel2);
        panel7.add(browseGeneButton);
        choices.add(browsePanel);
        choices.add(browsePanel2);
        choices.add(listGUI);
        choices.add(panel3);
        choices.add(panel5);
        choices.add(panel6);
        choices.add(panel7);
        choices.add(panel); 
    }
    
    public void actionPerformed(ActionEvent e) {

        //Handle analyze button action.
        if (e.getSource() == analyzeButton) {

            if (textfield.getText().equals("") || threshfield.getText().equals("")) {
                JFrame frame0 = new JFrame();
                JOptionPane.showMessageDialog(frame0, "Please fill in " +
                  "the missing inputs.", "Warning",
                  JOptionPane.WARNING_MESSAGE);
            }
            
            else if (softFile == null && (corrFile == null || pValFile == null)) {
                JFrame frame1 = new JFrame();
                JOptionPane.showMessageDialog(frame1, "Please upload a file.", 
                  "Warning", JOptionPane.WARNING_MESSAGE);
            }
            
            else if (listModel.getSize() == 0) {
                JFrame frame1 = new JFrame();
                JOptionPane.showMessageDialog(frame1, "Please enter genes.", "Warning", 
                  JOptionPane.WARNING_MESSAGE);
            }
            
            // else, begin to perform analysis
            else {

                // check to make sure p-value and threshold are within boundaries
                pValue = Double.parseDouble(textfield.getText());
                threshold = Double.parseDouble(threshfield.getText());
                if (pValue <= 0 || pValue >= 1 || threshold <= 0 || threshold >= 1) {
                    System.out.println(pValue + " " + threshold);
                    JFrame frame2 = new JFrame();
                    JOptionPane.showMessageDialog(frame2, "P-Value and threshold must be " +
                      "between 0 and 1.", "Warning", 
                      JOptionPane.WARNING_MESSAGE);
                }
                
                else {
                    setValue(5);
                    analyzeButton.setEnabled(false);
                    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    String[] arr = new String[listModel.size()];
                    for (int i = 0; i < listModel.size(); i++) {
                        arr[i] = (String)listModel.getElementAt(i);
                    }
                    setValue(15);
                    
                    //update GraphAnalysis values
                    try {
                        GraphAnalysis ga = new GraphAnalysis(softFile, corrFile, pValFile, indexFile);
                        ga.changeGeneList(arr);
                        ga.changePval(pValue);
                        ga.changeTau(threshold);
                        removeButton.setEnabled(true); 
                        clearButton.setEnabled(true);
                        setValue(35);

                        ga.adjacency();
                        setValue(70);

                        ga.graph();
                        setValue(100);

                        ga.clearR();
                    }
                    catch (NullPointerException exp) {
                        System.out.println("Failed to initiate GraphAnalysis. Did not correlate?");
                    }
                    finally {
                        analyzeButton.setEnabled(true);
                        setValue(0);
                    }

                }
            }
        }
        
        if (e.getSource() == loadButton) {
            if (textfield.getText().equals("")) {
                JFrame frame0 = new JFrame();
                JOptionPane.showMessageDialog(frame0, "Please enter an integer number of genes.", "Warning",
                  JOptionPane.WARNING_MESSAGE);
            }
            else {
                int n = Integer.parseInt(corrN.getText());
                loadDefault(n);
                clearButton.setEnabled(true);
            }
        }
        
        if (e.getSource() == clearButton) {
            if (listModel.getSize() > 0) {
                //clear listModel & disable
                listModel.clear();
                clearButton.setEnabled(false);
            }
            else 
                clearButton.setEnabled(false);
        }
        
        //Handle upload corrFile action
        if (e.getSource() == browseCorrButton) {
            int returnVal = corrFC.showOpenDialog(Tab3.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                corrFile = corrFC.getSelectedFile();
                log.append("Opened: " + corrFile.getName() + "\n");
                log.setCaretPosition(log.getDocument().getLength());
            } 
        }
        
        //Handle upload pValue action
        if (e.getSource() == browsePButton) {
            int returnVal = pValFC.showOpenDialog(Tab3.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                pValFile = pValFC.getSelectedFile();
                log.append("Opened: " + pValFile.getName() + "\n");
                log.setCaretPosition(log.getDocument().getLength());
            } 
        }

        //Handle upload geneIndex action
        if (e.getSource() == browseIndexButton) {
            int returnVal = indexFC.showOpenDialog(Tab3.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                indexFile = indexFC.getSelectedFile();
                log.append("Opened: " + indexFile.getName() +"\n");
                log.setCaretPosition(log.getDocument().getLength());
            }
        }
        
        // Handle gene browse file action
        if (e.getSource() == browseGeneButton) {
            int returnVal = genesFC.showOpenDialog(Tab3.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                geneFile = genesFC.getSelectedFile();
                In in = new In(geneFile);
                while (!in.isEmpty()) {
                    int index = listModel.getSize();
                    listModel.insertElementAt(in.readLine(), index);
                    index++;
                }
                clearButton.setEnabled(true);
                removeButton.setEnabled(true);
            }
        }

    }
    
    // when a new file is loaded, create new analysis object
    public void updateList() {
        GraphAnalysis ga = null;
        try {
            ga = new GraphAnalysis(softFile);
            ArrayList<String> newList = ga.getGenes();
            if (newList != null) {
                genes = new Java2sAutoTextField(newList);
            }
        }
        catch (NullPointerException e) {
            return;
        }
        JPanel panel4 = new JPanel(new GridLayout(0, 1));
        panel4.add(addButton);
        panel4.add(removeButton);
        panel4.add(clearButton);
        panel4.add(genes);
        JPanel panel5 = new JPanel(new GridLayout(0, 2));
        panel5.add(panel4);
        panel5.add(listScrollPane);
        JPanel choices1 = new JPanel();
        choices1.setLayout(new BoxLayout(choices1, BoxLayout.Y_AXIS));
        choices1.add(browsePanel);
        choices1.add(listGUI);
        choices1.add(panel3);
        choices1.add(panel5);
        choices1.add(panel6);
        choices1.add(panel7);
        choices1.add(panel);
        choices = choices1;
        addButton.setEnabled(true);
        tp.remove(2);
        tp.addTab("Filtering & Graphing", choices);
        tp.setMnemonicAt(2, KeyEvent.VK_3);

        preloadGraphAnalysis = true;
    }

    /*load default genes to list*/
    public void loadDefault(int n) {
        try {
            GraphAnalysis ga = new GraphAnalysis(softFile, corrFile, pValFile, indexFile);
            if (listModel.getSize() > 0) {
            //clear listModel & disable
                listModel.clear();
                clearButton.setEnabled(false);
            }
            String[] defaultGenes = ga.preloadGenes(n);
            if (defaultGenes == null) return;
            for (int i = 0; i < defaultGenes.length; i++) {
                int index = listModel.getSize();
                listModel.insertElementAt(defaultGenes[i], index);
                index++;
            }
        }
        catch (NullPointerException e) {
            System.out.println("Error in loading default genes -- double check correlation.");
        }
    }

    class RemoveListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            int index = list.getSelectedIndex();
            listModel.remove(index);

            int size = listModel.getSize();

            if (size == 0) { //nothing is left to remove
                removeButton.setEnabled(false);
                clearButton.setEnabled(false);
                
            } else { //Select an index.
                if (index == listModel.getSize()) {
                    //removed item in last position
                    index--;
                }
                
                list.setSelectedIndex(index);
                list.ensureIndexIsVisible(index);
            }
        }
    }
    
    //This listener is shared by the text field and the add button
    class AddListener implements ActionListener, DocumentListener {
        private boolean alreadyEnabled = false;
        private JButton button;
        
        public AddListener(JButton button) {
            this.button = button;
        }
        
        //Required by ActionListener.
        public void actionPerformed(ActionEvent e) {
            String name = genes.getText();
            
            //User didn't type in a unique name...
            if (name.equals("") || alreadyInList(name)) {
                Toolkit.getDefaultToolkit().beep();
                genes.requestFocusInWindow();
                genes.selectAll();
                return;
            }
            
            int index = list.getSelectedIndex(); //get selected index
            if (index == -1) { //no selection, so insert at beginning
                index = 0;
            } else {           //add after the selected item
                index++;
            }
            
            listModel.insertElementAt(genes.getText(), index);
            
            //Reset the text field.
            genes.requestFocusInWindow();
            genes.setText("");
            
            //Select the new item and make it visible.
            list.setSelectedIndex(index);
            list.ensureIndexIsVisible(index);
            removeButton.setEnabled(true);
        }
        
        //This method tests for string equality. Required by Document Listener.
        protected boolean alreadyInList(String name) {
            return listModel.contains(name);
        }
        public void insertUpdate(DocumentEvent e) {
            enableButton();
        }
        public void removeUpdate(DocumentEvent e) {
            handleEmptyTextField(e);
        }
        public void changedUpdate(DocumentEvent e) {
            if (!handleEmptyTextField(e)) {
                enableButton();
            }
        }
        private void enableButton() {
            if (!alreadyEnabled) {
                button.setEnabled(true);
            }
        }
        private boolean handleEmptyTextField(DocumentEvent e) {
            if (e.getDocument().getLength() <= 0) {
                button.setEnabled(false);
                alreadyEnabled = false;
                return true;
            }
            return false;
        }
    }
    
    //This method is required by ListSelectionListener.
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {
            if (list.getSelectedIndex() == -1) {
                // No selection, disable remove button.
                removeButton.setEnabled(false); 
            } else {
                // Selection, enable the remove & clear buttons.
                removeButton.setEnabled(true);
                clearButton.setEnabled(true);
            }
        }
    }
    
    // return panel
    public JPanel getPanel() {
        return choices;
    }
    
    public void setValue(int n) {
        progressBar.setValue(n);
        progressBar.paintImmediately(0, 0, progressBar.getSize().width, progressBar.getSize().height);
    }
    
    // transfer tabbed pane
    public void takeTabs(JTabbedPane tp) {
        this.tp = tp;
    }
    
    public void getFile(File softFile) {
        this.softFile = softFile;
    }
}