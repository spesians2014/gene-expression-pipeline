import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.lang.reflect.Field;

public class GUI extends JPanel implements ActionListener {

    JTabbedPane tabs;
    JButton one;
    JButton two;
    JPanel panel;
    JPanel buttonPanel;
    
    public GUI() {   
        // Generate opening text
        JLabel intro = new JLabel("Welcome to GenEx!");
        intro.setFont(new Font("Verdana",1,20));
        JLabel oneOrTwo = new JLabel("How many files would you like to upload?");
        
        // Generate choice buttons
        one = new JButton("Upload one file");
        one.addActionListener(this);
        two = new JButton("Upload two files");
        two.addActionListener(this);
        
        // create button Panel
        buttonPanel = new JPanel();
        buttonPanel.add(one);
        buttonPanel.add(two);
        
        // create child panel
        panel = new JPanel(new GridLayout(0, 1));
        panel.add(intro);
        panel.add(oneOrTwo);
        panel.add(buttonPanel);
        
        // create main panel to contain child panel (centered)
        JPanel mainPanel = new JPanel();
        mainPanel.add(panel);
        
        // set size of panel
        mainPanel.setPreferredSize(new Dimension(600, 450));
        
        // center page layout
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        Box horizontalBox = Box.createHorizontalBox(); 
        horizontalBox.add(Box.createHorizontalGlue()); 
        horizontalBox.add(panel); 
        horizontalBox.add(Box.createHorizontalGlue()); 
        Box verticalBox = Box.createVerticalBox(); 
        verticalBox.add(Box.createVerticalGlue()); 
        verticalBox.add(horizontalBox); // one inside the other
        verticalBox.add(Box.createVerticalGlue());
        mainPanel.add(verticalBox);
        
        // Create tabbed pane   
        tabs = new JTabbedPane();
        tabs.addTab("Choose Number of Files", mainPanel);
        tabs.setMnemonicAt(0, KeyEvent.VK_1);
        tabs.setSelectedIndex(0);
        add(tabs, BorderLayout.PAGE_START);
    }
    
    public void actionPerformed(ActionEvent e) {
        // if user chooses to upload one file
        if (e.getSource() == one) {
            if (!two.isEnabled()) {
                tabs.remove(2);
                tabs.remove(1);
            }
            // build next two tabs
            Tab2 t2 = new Tab2();
            tabs.addTab("Aggregation & Correlation", t2.getPanel());
            tabs.setMnemonicAt(1, KeyEvent.VK_2);
            Tab3 t3 = new Tab3();
            tabs.addTab("Filtering & Graphing", t3.getPanel());
            tabs.setMnemonicAt(2, KeyEvent.VK_3);
            add(tabs, BorderLayout.PAGE_START);
            one.setEnabled(false);
            two.setEnabled(true);
            tabs.setSelectedIndex(1);
            t2.takeTabs(tabs);
            t3.takeTabs(tabs);
        }
        
        // if user chooses to upload two files
        if (e.getSource() == two) {
            if (!one.isEnabled()) {
                tabs.remove(2);
                tabs.remove(1);
            }
            // build next two tabs
            Tab22 t22 = new Tab22();
            tabs.addTab("Aggregation & Correlation", t22.getPanel());
            tabs.setMnemonicAt(1, KeyEvent.VK_2);
            Tab32 t32 = new Tab32();
            tabs.addTab("Filtering & Graphing", t32.getPanel());
            tabs.setMnemonicAt(2, KeyEvent.VK_3);
            add(tabs, BorderLayout.PAGE_START);
            two.setEnabled(false);
            one.setEnabled(true);
            tabs.setSelectedIndex(1);
            t22.takeInfo(tabs, t32);
            t32.takeTabs(tabs);
        }
    }
    
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Gene Expression Pipeline");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Add content to the window.
        frame.add(new GUI());
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
    
    // display GUI
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                createAndShowGUI();
            }
        });
    }
}