/**
* GenEx ReadMe (sort of)
**/

Setup requirements:
We developed the program on Mac OS X 10.9. Compatibility with older Mac OS platforms and other operating systems is untested.

GenEx requires the statistical programming language R, and several third-party R packages.

	1) If you don't have R, or your version of R is older than R 3.0.3, download R at http://www.r-project.org/

	2) Open your R console and install the following packages (copy and paste the code):

		install.packages("data.table")
		install.packages("WCGNA")
		source("http://bioconductor.org/biocLite.R")
		biocLite("Biobase")
		biocLite("GEOquery")

	You will be asked to select a download mirror. Pick a mirror that is geographically close to you. Then click OK. R will download and install packages for data.table, WCGNA, Biobase, and GEOquery.

	3) Install the GenEx R function library, GenExR_1.0.tar.gz
		 Go back to the R console. Copy and paste this line:

		install.packages( "FILEDIRECTORY/GenExR_1.0.tar.gz" , repos = NULL, type="source")

Replace FILEDIRECTORY with the directory (folder) in which you downloaded GenEx_1.0.tar.gz.


The main class is GUI.java.
To compile and execute the program (in Mac-speak):
	Set the current directory to the parent folder (GenEx_legacy)""

	Compilation:
		javac -cp Import/*:. GUI.java

		(make sure to set the classpath! The program depends on .jar files in Import subfolder)

	Execution:
		./run -cp Import/*:. GUI

		(Not sure if this run script works for Linux... see http://rforge.net/JRI/)

