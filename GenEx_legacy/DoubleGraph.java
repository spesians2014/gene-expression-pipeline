/** DoubleGraph
* creates graph, sends to GraphViewer, displays in GraphFramer
* colors are different if edges are unique to graph or shared by graphs
*
* Depends: SingleGraph, InteractGraph, GraphViewer, GraphFramer 
**/

//graph libraries
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.algorithms.layout.*;
import edu.uci.ics.jung.visualization.*;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.screencap.Dump;
import edu.uci.ics.screencap.PNGDump;
import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.Transformer;

//GUI libraries
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.geom.*;
import java.awt.geom.Point2D;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.*;
import java.awt.event.*;


public class DoubleGraph extends InteractGraph {
    /**
    * constants and instance variables
    **/
    private Color BOTHCOLOR = Color.GREEN;
    private Color G1COLOR = Color.BLUE;
    private Color G2COLOR = Color.RED;
    private String data1;
    private String data2;

    /**
    * constructors
    **/
    public DoubleGraph(int[] adj, String[] names, String graph1, String graph2) {
        //set width and height
        WIDTH = DEFAULT_WIDTH;
        HEIGHT = DEFAULT_HEIGHT;
        TITLE = graph1 + " and " + graph2;
        data1 = graph1;
        data2 = graph2;

        createGraph(adj, names); //create graph

        setAppearance(); //set graph appearance

        createAndShowGUI(true); //show GUI
    }


    /**
    * Overrides: 
    **/
    @Override
    /*add edges to graph*/
    protected void addEdges(int[] adj, String[] names) {
        int a = 0; //index in adjacency matrix
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (adj[a] > 0) {
                    int k = a * 10 + adj[a];
                    g.addEdge(k, names[i], names[j]);
                }
                a++;
            }
        }
    }

    @Override
    /*color edges*/
    protected void colorEdges() {
        edgePaint = 
        new Transformer<Integer, Paint>() {
            public Paint transform(Integer edge) {
                int number = (edge % 10);
                Color color = Color.GRAY;
                if (number == GraphAnalysis.BOTH) color = BOTHCOLOR;
                else if (number == GraphAnalysis.G1) color = G1COLOR;
                else if (number == GraphAnalysis.G2) color = G2COLOR;
                return color;
            }
        };
    }

    @Override
    protected JPanel aKeyPanel() {
        //key for graphs
        JPanel keyPanel = new JPanel();
        JLabel keyLabel = new JLabel("Key: ");
        JLabel bothLabel = new JLabel("Common to both graphs; ");
        bothLabel.setForeground(BOTHCOLOR);
        JLabel g1Label = new JLabel("Unique to " + data1 + "; ");
        g1Label.setForeground(G1COLOR);
        JLabel g2Label = new JLabel("Unique to " + data2);
        g2Label.setForeground(G2COLOR);
        keyPanel.add(keyLabel);
        keyPanel.add(bothLabel);
        keyPanel.add(g1Label);
        keyPanel.add(g2Label);

        return keyPanel;
    }
}