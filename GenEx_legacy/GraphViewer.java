/** GraphViewer
* adds graph viewer to JFrame
* 
* Provides implementations of GraphFramer methods:
* buildGraphPanel, buildMenuBar, addResizeListener, ResizeListener,
* hideIsolates, showIsolates, saveGraph, stopJittering
*
* Subclasses should implement colorEdges, colorVertices, shapeVertices,
* hideVertices, setLabels, hideLabels
* 
*
* Depends: GraphFramer
**/ 


//graph libraries
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.algorithms.layout.*;
import edu.uci.ics.jung.algorithms.layout.*;
import edu.uci.ics.jung.visualization.*;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.control.*;
import edu.uci.ics.screencap.Dump;
import edu.uci.ics.screencap.PNGDump;
import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.Transformer;

//GUI libraries
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.geom.*;
import java.awt.geom.Point2D;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.*;
import java.awt.event.*;


public abstract class GraphViewer extends GraphFramer {
	/**
	* constants
	**/
    //default dimensions
	protected static final int DEFAULT_WIDTH = 700;
	protected static final int DEFAULT_HEIGHT = 600;
	protected static final int MENUHEIGHT = 20;

    /**
    * instance variables
    **/
    protected Layout<String, Integer> l;
    protected EditingModalGraphMouse gm;
    protected ResizeListener rl;

    protected VisualizationViewer<String, Integer> vv;
    protected Transformer<Integer, Paint> edgePaint;
    protected Transformer<String, Paint> vertexPaint;
    protected Transformer<String, Shape> vertexShape;
    protected Transformer<String, String> vertexLabel;

    protected Graph<String, Integer> g; //abstract
    protected int WIDTH;
    protected int HEIGHT;


    /**
    * constructor
    **/
    public GraphViewer() {
    	;
    }


    /**
	* set graph appearance
	**/
	protected void setAppearance() {
		//set layout
		l = new KKLayout<String, Integer>(g);
		l.setSize(new Dimension(WIDTH, HEIGHT));

		//set edge/vertex/label properties
		colorEdges();
		colorVertices();
		shapeVertices();
		setLabels();

		//create & set visualization viewer
		vv = new VisualizationViewer<String, Integer>(l);
		vv.setPreferredSize(new Dimension(WIDTH,HEIGHT)); //set size
		setVVProperties();

		//add mouse
		addMouse();
	}


	/*set vv propertes*/
	protected void setVVProperties() {
		vv.getRenderContext().setVertexLabelTransformer(vertexLabel);
		vv.getRenderContext().setVertexShapeTransformer(vertexShape);
		vv.getRenderContext().setVertexFillPaintTransformer(vertexPaint);
		vv.getRenderContext().setEdgeDrawPaintTransformer(edgePaint);
		vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<String,Integer>());
	}

	/*add mouse for picking/choosing/translating graph*/
	protected void addMouse() {
    	//mouse vertex factory
		Factory<String> vertexFactory = new Factory<String>() {
			public String create() {
				String newString = "";
				return newString;
			}
		};

    	//mouse edge factory
		Factory<Integer> edgeFactory = new Factory<Integer>() {
			public Integer create() {
				int x = 0;
				return x;
			}
		};

        //mouse creation
		gm = new EditingModalGraphMouse(vv.getRenderContext(), vertexFactory, edgeFactory);
		vv.setGraphMouse(gm);
		gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
	}


    /**
    * edge, vertex, and label property changers
    **/
    abstract void colorEdges();
    abstract void colorVertices();
    abstract void shapeVertices();
    abstract void hideVertices();
    abstract void setLabels();
    abstract void hideLabels();
    


    /**
    * show/hide isolates
    **/
    //show isolates
    protected void showIsolates() {
    	//reset vertices & labels
    	shapeVertices();
    	setLabels();
    	setVVProperties();

    	//alter isolates panel
    	showIsolates.setVisible(false);
    	hideIsolates.setVisible(true);

    	graphPanel.repaint();
    }

    //hide isolates
    protected void hideIsolates() {
    	//set vertices & labels
    	hideVertices();
    	hideLabels();
    	setVVProperties();

    	//alter isolates panel
    	showIsolates.setVisible(true);
    	hideIsolates.setVisible(false);

    	graphPanel.repaint();
    }


    /**
    * other GraphFramer implementations
    **/
    //build graph panel
    protected void buildGraphPanel() {
    	graphPanel = new JPanel();
    	graphPanel.add(vv);
    	frame.getContentPane().add(graphPanel);
    }

    //build menu bar
    protected void buildMenuBar() {
    	menuBar = new JMenuBar();
    	JMenu modeMenu = gm.getModeMenu();
    	modeMenu.setText("Select Mode of Graph Interaction");
    	modeMenu.setIcon(null);
    	modeMenu.setPreferredSize(new Dimension(WIDTH,MENUHEIGHT)); 
    	menuBar.add(modeMenu);
    	frame.setJMenuBar(menuBar);
    }

    //build key panel... or not
    protected JPanel aKeyPanel() {
        JPanel keyPanel = new JPanel();
        return keyPanel;
    }

    //add resize listener
    protected void addResizeListener() {
    	rl = new ResizeListener(this);
    	frame.addComponentListener(rl);
    }

    //stop jittering
    protected void stopJittering() {
        //stop vertices from jittering
        for (String v : g.getVertices()) {
            l.lock(v, true);
        }
    }

    //save graph
    protected void saveGraph() {
    	// Create the VisualizationImageServer
    	VisualizationImageServer<String, Integer> vis =
    	new VisualizationImageServer<String, Integer>(vv.getGraphLayout(), vv.getGraphLayout().getSize());
    	vis.setBackground(Color.WHITE);
    	vis.getRenderContext().setVertexShapeTransformer(vertexShape);
    	vis.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<String>());
    	vis.getRenderContext().setVertexFillPaintTransformer(vertexPaint);
    	vis.getRenderContext().setEdgeDrawPaintTransformer(edgePaint);
    	vis.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<String,Integer>());

        // Create the buffered image
    	BufferedImage image = (BufferedImage) vis.getImage(new Point2D.Double(vv.getGraphLayout().getSize().getWidth() / 2, vv.getGraphLayout().getSize().getHeight() / 2), new Dimension(vv.getGraphLayout().getSize()));

        // Write image to a png file
    	File outputfile = new File(saveName.getText() + ".png");
    	try {
    		ImageIO.write(image, "png", outputfile);
    	} catch (IOException e) { }
    }

}




/**
* resize listener
**/
class ResizeListener extends ComponentAdapter {
	final GraphViewer gv;

	public ResizeListener(GraphViewer gv) {
		this.gv = gv;
	}

	public void componentResized(ComponentEvent e) {
		if ((gv.graphPanel != null) && (gv.l != null) && (gv.vv != null)) {
			Dimension d = new Dimension(gv.graphPanel.getWidth(), gv.graphPanel.getHeight());
			gv.l.setSize(d);
			gv.vv.setSize(d);
			gv.vv.setLocation(0, 0);
			gv.graphPanel.setLocation(0, 50);
			gv.repaint();
		}
	}
}