/**Analysis
  *Parent class for DataAnalysis and GraphAnalysis
  *
  *Dependencies: java.io, JRI.Rengine, stdlib, javax.swing, GenExR
  *
  *Compilation: javac -cp LIBRARIES:. Analysis.java
  *Execution: ./run -cp LIBRARIES:. Analysis
  **/

/*import libraries*/
import java.io.*;
import org.rosuda.JRI.Rengine;
import org.rosuda.JRI.REXP;
import javax.swing.*;
import javax.swing.JOptionPane;


public abstract class Analysis {
	public static Rengine re;

	//constructor
	public Analysis() {
		startR();
	}

	/*start Rengine*/
	private static void startR() {
        //check if R Engine exists
		re = Rengine.getMainEngine();
		if (re == null) {
            // Create R Engine
			String[] Rargs = {"--vanilla"};
			re = new Rengine(Rargs, false, null);

			// import R function library
			re.eval("library(GenExR)");
			re.eval("gc()");
		}

        // Check that R is ready
		if(!re.waitForR()) {
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog(frame, "Cannot load R.", 
				"Warning", 
				JOptionPane.WARNING_MESSAGE);
		}
	}

	/*get file extension from file*/
	public static String getExtension(File file) {
		String ext = null;
		String filepath = file.getName();
		int i = filepath.lastIndexOf('.');
		if (i > 0 &&  i < filepath.length() - 1) {
			ext = filepath.substring(i).toLowerCase();
		}
		return ext;
	}
	/*get file extension from String*/
	public static String getExtension(String filepath) {
		String ext = null;
		int i = filepath.lastIndexOf('.');
		if (i > 0 &&  i < filepath.length() - 1) {
			ext = filepath.substring(i).toLowerCase();
		}
		return ext;
	}

	/*get file name without extension from file*/
	public static String noExtension(File file) {
		String name = null;
		String filename = file.getName();
		int i = filename.lastIndexOf('.');
		if (i > 0 && i < filename.length() - 1) {
			name = filename.substring(0, i);
		}
		return name;
	}
	/*get file name without extension from String*/
	public static String noExtension(String filename) {
		String name = null;
		int i = filename.lastIndexOf('.');
		if (i > 0 && i < filename.length() - 1) {
			name = filename.substring(0, i);
		}
		return name;
	}

	/*clears R session -- only call if you're done with the data*/
	public void clearR() {
		String clear = "rm(list=ls())";
		re.eval(clear);
		re.eval("gc()");
	}

	/*ends R session -- only include if you're done with *everything*/
	public void endR() {
		re.end();
	}

}