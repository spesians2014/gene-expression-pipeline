/** SingleGraph
* creates graph, sends to GraphViewer, displays in GraphFramer
* colors are different if edges are unique to graph
*
* Depends: InteractGraph, GraphViewer, GraphFramer 
**/

//graph libraries
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.algorithms.layout.*;
import edu.uci.ics.jung.visualization.*;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.screencap.Dump;
import edu.uci.ics.screencap.PNGDump;
import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.Transformer;

//GUI libraries
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.geom.*;
import java.awt.geom.Point2D;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.*;
import java.awt.event.*;


public class SingleGraph extends InteractGraph {
    /**
    * constants and instance variables
    **/
	private final Color NEWCOLOR; //color of unique edges
    private int which;

    /**
    * constructors
    **/
    public SingleGraph(int[] adj, String[] names, String graph, int which) {
        //set width and height
        WIDTH = DEFAULT_WIDTH;
        HEIGHT = DEFAULT_HEIGHT;
        TITLE = graph;
        this.which = which;

		//make sure we're asking for either graph 1 or graph 2
		//set edge color correspondingly
        if (which == GraphAnalysis.G1)
           NEWCOLOR = Color.BLUE;
        else if (which == GraphAnalysis.G2)
           NEWCOLOR = Color.RED;
        else
           throw new IllegalArgumentException();

        createGraph(adj, names); //create graph

        setAppearance(); //set graph appearance

        createAndShowGUI(true); //show GUI
    }


    /**
    * Overrides: 
    **/
    @Override
    /*add edges to graph*/
    protected void addEdges(int[] adj, String[] names) {
        int a = 0; //index in adjacency matrix
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (adj[a] == GraphAnalysis.BOTH || adj[a] == which) {
                    int k = a * 10 + adj[a];
                    g.addEdge(k, names[i], names[j]);
                }
                a++;
            }
        }
    }

    @Override
    /*color edges*/
    protected void colorEdges() {
        edgePaint = 
        new Transformer<Integer, Paint>() {
            public Paint transform(Integer edge) {
                int number = (edge % 10);
                Color color = EDGE_COLOR;
                if (number == which) color = NEWCOLOR;
                return color;
            }
        };
    }

    @Override
    protected JPanel aKeyPanel() {
        //key for graphs
    	JPanel keyPanel = new JPanel();
    	JLabel keyLabel = new JLabel("Key: ");
    	JLabel bothLabel = new JLabel("Common to both graphs; ");
    	bothLabel.setForeground(EDGE_COLOR);
    	JLabel uniqueLabel = new JLabel("Unique to " + TITLE);
    	uniqueLabel.setForeground(NEWCOLOR);
    	keyPanel.add(keyLabel);
    	keyPanel.add(bothLabel);
    	keyPanel.add(uniqueLabel);

        return keyPanel;
    }
}