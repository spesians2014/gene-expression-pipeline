// ./run -cp Import/*:. GUI

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.SwingUtilities;
import javax.swing.JOptionPane;
import javax.swing.event.*;
import java.beans.*;
import org.rosuda.JRI.Rengine;
import java.util.ArrayList;
import javax.swing.filechooser.*;
import edu.princeton.cs.introcs.*;


public class Tab32 extends JPanel implements ActionListener, ListSelectionListener {

    // Browse record logs
    private JTextArea log;
    private JTextArea log2;
    
    // P-Value and threshold value
    protected JTextField textfield;
    protected JTextField threshfield;
    private double pValue;
    private double threshold;
    
    // Buttons
    private JButton corrButton;
    private JButton pValButton;
    private JButton indexButton;
    private JButton corrButton2;
    private JButton pValButton2;
    private JButton indexButton2;
    private JButton addButton;
    private JButton removeButton;
    private JButton clearButton;
    private JButton analyzeButton;
    private JButton browseButton;
    private JButton loadButton;
    private JButton loadButton2;
    
    // Files
    private File softFile;
    private File softFile2;
    private File corrFile;
    private File pValFile;
    private File indexFile;
    private File corrFile2;
    private File pValFile2;
    private File indexFile2;
    private File genefile;
    
    // File Choosers
    private JFileChooser corrChooser = new JFileChooser();
    private JFileChooser corrChooser2 = new JFileChooser();
    private JFileChooser pValChooser = new JFileChooser();
    private JFileChooser pValChooser2 = new JFileChooser();
    private JFileChooser geneChooser = new JFileChooser();
    private JFileChooser indexChooser = new JFileChooser();
    private JFileChooser indexChooser2 = new JFileChooser();
    
    private ArrayList<String> alist;
    
    private JPanel choices;
    
    private Java2sAutoTextField genes;
    private JTextField corrN;
    private JProgressBar progressBar;
    private JList list;
    private DefaultListModel listModel;
    
    private JPanel listGUI;
    private JPanel panel3;
    private JScrollPane listScrollPane;
    private JPanel panel6;
    private JPanel panel7;
    private JPanel panel;
    private JPanel browsePanel;
    private JPanel browsePanel2;
    
    private JTabbedPane tp;
    
    public Tab32() {

        // Create browsing log on top
        log = new JTextArea(2,20);
        log.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(log);
        
        // Create second browsing log on top
        log2 = new JTextArea(2,20);
        log2.setEditable(false);
        JScrollPane logScrollPane2 = new JScrollPane(log2);
        
        // Build upload buttons
        corrButton = new JButton("Load correlations");
        corrButton.addActionListener(this);
        pValButton = new JButton("Load p-values");
        pValButton.addActionListener(this);
        corrButton2 = new JButton("Load correlations");
        corrButton2.addActionListener(this); 
        pValButton2 = new JButton("Load p-values");
        pValButton2.addActionListener(this);
        indexButton = new JButton("Load gene index");
        indexButton.addActionListener(this);
        indexButton2 = new JButton("Load gene index");
        indexButton2.addActionListener(this);
        
        // create text field for p-values
        textfield = new JTextField("0.05", 5);
        textfield.setActionCommand("P-Value Filter");
        textfield.addActionListener(this);
        
        // input area for theshold value
        threshfield = new JTextField("0.9", 5);
        threshfield.setActionCommand("Network Threshold");
        threshfield.addActionListener(this);
        
        // p-value label
        JLabel textfieldLabel = new JLabel("P-Value Filter: ");
        textfieldLabel.setLabelFor(textfield);
        // threshold label
        JLabel threshfieldLabel = new JLabel("Network Threshold: ");
        threshfieldLabel.setLabelFor(threshfield);
        
        // Build analyze button.
        analyzeButton = new JButton("Go!");
        analyzeButton.addActionListener(this);
        
        // Build progress bar
        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        
        // Build list
        listModel = new DefaultListModel();
        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.addListSelectionListener(this);
        list.setVisibleRowCount(6);
        listScrollPane = new JScrollPane(list);
        
        // Build add button
        addButton = new JButton("Add");
        AddListener addListener = new AddListener(addButton);
        addButton.setActionCommand("Add");
        addButton.addActionListener(addListener);
        addButton.setEnabled(false);
        
        // Build remove button
        removeButton = new JButton("Remove");
        removeButton.setActionCommand("Remove");
        removeButton.addActionListener(new RemoveListener());
        removeButton.setEnabled(false);
        
        //build clear button
        clearButton = new JButton("Clear");
        removeButton.setActionCommand("Clear");
        clearButton.addActionListener(this);
        clearButton.setEnabled(false);
        
        // Build text field for entering genes
        alist = new ArrayList<String>();
        alist.add("Please upload file from previous tab to begin");    
        genes = new Java2sAutoTextField(alist);
        
        genes.addActionListener(addListener);
        genes.getDocument().addDocumentListener(addListener);
        JLabel geneLabel = new JLabel("                                " +
          "Enter genes to include in graph network");
        
        // Build label + text field for pre-loading genes
        JLabel fileLabela = new JLabel("and/or load ");
        corrN = new JTextField("50", 3);
        JLabel fileLabelb = new JLabel(" most correlated genes");
        
        // Build Load Button.
        loadButton = new JButton("Load from data 1");
        loadButton.addActionListener(this);
        
        loadButton2 = new JButton("Load from data 2");
        loadButton2.addActionListener(this);
        
        // Build file chooser for uploading genes
        JLabel fileLabel2 = new JLabel("and/or upload file of gene names, one gene per line");
        
        // Build browse button.
        browseButton = new JButton("Browse Files...");
        browseButton.addActionListener(this);
        
        // Add content
        choices = new JPanel();
        choices.setLayout(new BoxLayout(choices, BoxLayout.Y_AXIS));  
        panel = new JPanel();
        panel.add(analyzeButton);
        panel.add(progressBar);
        JPanel textfieldPanel= new JPanel();
        textfieldPanel.add(textfieldLabel);
        textfieldPanel.add(textfield);
        JPanel threshfieldPanel = new JPanel();
        threshfieldPanel.add(threshfieldLabel);
        threshfieldPanel.add(threshfield);
        
        listGUI = new JPanel(new GridLayout(0, 2));
        listGUI.add(textfieldPanel);
        listGUI.add(threshfieldPanel);
        
        //browse correlation-related files
        browsePanel = new JPanel();
        browsePanel.setLayout(new BoxLayout(browsePanel, BoxLayout.X_AXIS));
        GridLayout browseLayout = new GridLayout(0, 2);
        Box browse1 = Box.createVerticalBox();
        JLabel browse1Label = new JLabel("    Data 1:");
        JPanel browse1Layout = new JPanel();
        browse1Layout.add(browse1Label, BorderLayout.EAST);
        browse1Layout.setLayout(browseLayout);
        browse1Layout.add(corrButton);
        browse1Layout.add(indexButton);
        browse1Layout.add(pValButton);
        browse1.add(browse1Layout);
        browse1.add(logScrollPane);
        Box browse2 = Box.createVerticalBox();
        JLabel browse2Label = new JLabel("    Data 2:");
        JPanel browse2Layout = new JPanel();
        browse2Layout.add(browse2Label, BorderLayout.EAST);
        browse2Layout.setLayout(browseLayout);
        browse2Layout.add(corrButton2);
        browse2Layout.add(indexButton2);
        browse2Layout.add(pValButton2);
        browse2.add(browse2Layout);
        browse2.add(logScrollPane2);
        browsePanel.add(browse1);
        browsePanel.add(Box.createRigidArea(new Dimension(8,0)));
        browsePanel.add(browse2);

        browsePanel2 = new JPanel();
        JLabel browseLabel = new JLabel("<html>Inherit data from previous step: upload nothing. Use .csv files: upload correlation <br> and p-value files. Use .rds files: upload correlation, p-value, and gene index files. <br> <hr> </html>");
        browsePanel2.add(browseLabel, BorderLayout.NORTH);
        
        panel3 = new JPanel(new GridLayout(0, 1));
        panel3.add(geneLabel);
        JPanel panel4 = new JPanel(new GridLayout(0, 1));
        panel4.add(addButton);
        panel4.add(removeButton);
        panel4.add(clearButton);
        panel4.add(genes);
        JPanel panel5 = new JPanel(new GridLayout(0, 2));
        panel5.add(panel4);
        panel5.add(listScrollPane);
        panel6 = new JPanel();
        panel6.add(fileLabela);
        panel6.add(corrN);
        panel6.add(fileLabelb);
        panel6.add(loadButton);
        panel6.add(loadButton2);
        panel7 = new JPanel();
        panel7.add(fileLabel2);
        panel7.add(browseButton);
        choices.add(browsePanel);
        choices.add(browsePanel2);
        //choices.add(browseLabel);
        choices.add(listGUI);
        choices.add(panel3);
        choices.add(panel5);
        choices.add(panel6);
        choices.add(panel7);
        choices.add(panel);
        
    }
    
    public void actionPerformed(ActionEvent e) {

        //Handle analyze button action.
        if (e.getSource() == analyzeButton) {
            // check for missing inputs 
            if (textfield.getText().equals("") || threshfield.getText().equals("")) {
                JFrame frame0 = new JFrame();
                JOptionPane.showMessageDialog(frame0, "Please fill in " +
                  "the missing inputs.", "Warning",
                  JOptionPane.WARNING_MESSAGE);
            }
            // check for genes entered
            else if (listModel.getSize() == 0) {
                JFrame frame1 = new JFrame();
                JOptionPane.showMessageDialog(frame1, "Please enter genes.", "Warning", 
                  JOptionPane.WARNING_MESSAGE);
            }
            
            // else, begin to perform analysis
            else {
                // check to make sure p-value and threshold are within boundaries
                pValue = Double.parseDouble(textfield.getText());
                threshold = Double.parseDouble(threshfield.getText());
                if (pValue <= 0 || pValue >= 1 || threshold <= 0 || threshold >= 1) {
                    JFrame frame2 = new JFrame();
                    JOptionPane.showMessageDialog(frame2, "P-Value and threshold must be " +
                      "between 0 and 1.", "Warning", 
                      JOptionPane.WARNING_MESSAGE);
                }
                
                else {

                    analyzeButton.setEnabled(false);
                    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    String[] arr = new String[listModel.size()];
                    for (int i = 0; i < listModel.size(); i++) {
                        arr[i] = (String)listModel.getElementAt(i);
                    }

                    // Create graph analysis objects
                    try {
                        GraphAnalysis ga = new GraphAnalysis(softFile, corrFile, pValFile, indexFile);
                        ga.changeGeneList(arr);
                        ga.changePval(pValue);
                        ga.changeTau(threshold);
                        ga.adjacency();

                        GraphAnalysis ga2 = new GraphAnalysis(softFile2, corrFile2, pValFile2, indexFile2);
                        ga2.changeGeneList(arr);
                        ga2.changePval(pValue);
                        ga2.changeTau(threshold);
                        ga2.adjacency();

                        ga.multiGraph(ga2.getAdjVec(), ga2.fileName());

                    //clear R
                        ga.clearR();
                        ga2.clearR();
                    }
                    catch (NullPointerException exp) {
                        System.out.println("Data was not correlated");
                    }
                    finally {
                        analyzeButton.setEnabled(true);
                        removeButton.setEnabled(true); 
                        clearButton.setEnabled(true);
                    }
                }
            }
        }
        
        // Handle load genes action
        if (e.getSource() == loadButton) {
            if (textfield.getText().equals("")) {
                JFrame frame0 = new JFrame();
                JOptionPane.showMessageDialog(frame0, "Please enter an integer number of genes.", "Warning",
                  JOptionPane.WARNING_MESSAGE);
            }
            else {
                int n = Integer.parseInt(corrN.getText());
                loadDefault(n, 1);
                clearButton.setEnabled(true);
            }
        }
        
        // Handle load genes action
        if (e.getSource() == loadButton2) {
            if (textfield.getText().equals("")) {
                JFrame frame0 = new JFrame();
                JOptionPane.showMessageDialog(frame0, "Please enter an integer number of genes.", "Warning",
                  JOptionPane.WARNING_MESSAGE);
            }
            else {
                int n = Integer.parseInt(corrN.getText());
                loadDefault(n, 2);
                clearButton.setEnabled(true);
            }
        }
        
        // Handle clear action
        if (e.getSource() == clearButton) {
            if (listModel.getSize() > 0) {
                //clear listModel & disable
                listModel.clear();
                clearButton.setEnabled(false);
            }
            else 
                clearButton.setEnabled(false);
        }
        
        //Handle upload action
        if (e.getSource() == corrButton) {
            int returnVal = corrChooser.showOpenDialog(Tab32.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                corrFile = corrChooser.getSelectedFile();
                log.append("Opened: " + corrFile.getName() + ".\n");
                log.setCaretPosition(log.getDocument().getLength());
            } 
        }
        
        //Handle upload action
        if (e.getSource() == corrButton2) {
            int returnVal = corrChooser2.showOpenDialog(Tab32.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                corrFile2 = corrChooser2.getSelectedFile();
                log2.append("Opened: " + corrFile2.getName() + ".\n");
                log2.setCaretPosition(log2.getDocument().getLength());
            } 
        }
        
        //Handle upload action
        if (e.getSource() == pValButton) {
            int returnVal = pValChooser.showOpenDialog(Tab32.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                pValFile = pValChooser.getSelectedFile();
                log.append("Opened: " + pValFile.getName() + ".\n");
                log.setCaretPosition(log.getDocument().getLength());
            } 
        }
        
        //Handle upload action
        if (e.getSource() == pValButton2) {
            int returnVal = pValChooser2.showOpenDialog(Tab32.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                pValFile2 = pValChooser2.getSelectedFile();
                log2.append("Opened: " + pValFile2.getName() + ".\n");
                log2.setCaretPosition(log2.getDocument().getLength());
            } 
        } 

        //Handle upload action
        if (e.getSource() == indexButton) {
            int returnVal = indexChooser.showOpenDialog(Tab32.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                indexFile = indexChooser.getSelectedFile();
                log.append("Opened: " + indexFile.getName() + ".\n");
                log.setCaretPosition(log.getDocument().getLength());
            } 
        }
        
        //Handle upload action
        if (e.getSource() == indexButton2) {
            int returnVal = indexChooser2.showOpenDialog(Tab32.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                indexFile2 = indexChooser2.getSelectedFile();
                log2.append("Opened: " + indexFile2.getName() + ".\n");
                log2.setCaretPosition(log2.getDocument().getLength());
            } 
        } 
        
        // Handle gene browse file action
        if (e.getSource() == browseButton) {
            int returnVal = geneChooser.showOpenDialog(Tab32.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                genefile = geneChooser.getSelectedFile();
                In in = new In(genefile);
                while (!in.isEmpty()) {
                    int index = listModel.getSize();
                    listModel.insertElementAt(in.readLine(), index);
                    index++;
                }
                clearButton.setEnabled(true);
                removeButton.setEnabled(true);
            }
        }
    }
    
    // update autocomplete list
    public void updateList() {
        GraphAnalysis ga = null;
        try {
            ga = new GraphAnalysis(softFile);
            ArrayList<String> newList = ga.getGenes();
            if (newList != null)
                genes = new Java2sAutoTextField(newList);
            JPanel panel4 = new JPanel(new GridLayout(0, 1));
            panel4.add(addButton);
            panel4.add(removeButton);
            panel4.add(clearButton);
            panel4.add(genes);
            JPanel panel5 = new JPanel(new GridLayout(0, 2));
            panel5.add(panel4);
            panel5.add(listScrollPane);
            JPanel choices1 = new JPanel();
            choices1.setLayout(new BoxLayout(choices1, BoxLayout.Y_AXIS));
            choices1.add(browsePanel);
            choices1.add(listGUI);
            choices1.add(panel3);
            choices1.add(panel5);
            choices1.add(panel6);
            choices1.add(panel7);
            choices1.add(panel);
            choices = choices1;
            addButton.setEnabled(true);
            tp.remove(2);
            tp.addTab("Filtering & Graphing", choices);
            tp.setMnemonicAt(2, KeyEvent.VK_3);
        }
        catch (NullPointerException e) {
            return;
        }
    }
    
    // load default genes to list
    public void loadDefault(int n, int which) {
        GraphAnalysis ga;
        if (which == 1) {
            ga = new GraphAnalysis(softFile, corrFile, pValFile, indexFile);
        }
        else ga = new GraphAnalysis(softFile2, corrFile2, pValFile2, indexFile2);
        if (listModel.getSize() > 0) {
            //clear listModel & disable
            listModel.clear();
            clearButton.setEnabled(false);
        }
        String[] defaultGenes = ga.preloadGenes(n);
        if (defaultGenes == null) return;
        for (int i = 0; i < defaultGenes.length; i++) {
            int index = listModel.getSize();
            listModel.insertElementAt(defaultGenes[i], index);
            index++;
        }
    }
    
    class RemoveListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            int index = list.getSelectedIndex();
            listModel.remove(index);
            int size = listModel.getSize();
            
            
            if (size == 0) { //nothing is left to remove
                removeButton.setEnabled(false);
                clearButton.setEnabled(false);
                
            } else { //Select an index.
                if (index == listModel.getSize()) {
                    //removed item in last position
                    index--;
                }
                list.setSelectedIndex(index);
                list.ensureIndexIsVisible(index);
            }
        }
    }
    
    //This listener is shared by the text field and the add button
    class AddListener implements ActionListener, DocumentListener {
        private boolean alreadyEnabled = false;
        private JButton button;
        
        public AddListener(JButton button) {
            this.button = button;
        }
        
        //Required by ActionListener.
        public void actionPerformed(ActionEvent e) {
            String name = genes.getText();
            
            //User didn't type in a unique name...
            if (name.equals("") || alreadyInList(name)) {
                Toolkit.getDefaultToolkit().beep();
                genes.requestFocusInWindow();
                genes.selectAll();
                return;
            }
            
            int index = list.getSelectedIndex(); //get selected index
            if (index == -1) { //no selection, so insert at beginning
                index = 0;
            } else {           //add after the selected item
                index++;
            }
            
            listModel.insertElementAt(genes.getText(), index);
            
            //Reset the text field.
            genes.requestFocusInWindow();
            genes.setText("");
            
            //Select the new item and make it visible.
            list.setSelectedIndex(index);
            list.ensureIndexIsVisible(index);
            removeButton.setEnabled(true);
        }
        
        //This method tests for string equality. Required by Document Listener.
        protected boolean alreadyInList(String name) {
            return listModel.contains(name);
        }
        public void insertUpdate(DocumentEvent e) {
            enableButton();
        }
        public void removeUpdate(DocumentEvent e) {
            handleEmptyTextField(e);
        }
        public void changedUpdate(DocumentEvent e) {
            if (!handleEmptyTextField(e)) {
                enableButton();
            }
        }
        private void enableButton() {
            if (!alreadyEnabled) {
                button.setEnabled(true);
            }
        }
        private boolean handleEmptyTextField(DocumentEvent e) {
            if (e.getDocument().getLength() <= 0) {
                button.setEnabled(false);
                alreadyEnabled = false;
                return true;
            }
            return false;
        }
    }
    
    //This method is required by ListSelectionListener.
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {
            if (list.getSelectedIndex() == -1) {
                // No selection, disable remove button.
                removeButton.setEnabled(false); 
            } else {
                // Selection, enable the remove & clear buttons.
                removeButton.setEnabled(true);
                clearButton.setEnabled(true);
            }
        }
    }
    
    // return panel
    public JPanel getPanel() {
        return choices;
    }
    
    // transfer tabbed pane
    public void takeTabs(JTabbedPane tp) {
        this.tp = tp;
    }
    
    // transfer file
    public void write1(File softFile) {
        this.softFile = softFile;
    }
    
    // transfer file
    public void write2(File softFile) {
        this.softFile2 = softFile;
    }
}