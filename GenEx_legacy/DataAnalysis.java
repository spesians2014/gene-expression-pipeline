/**DataAnalysis
  *
  *Input: user input name of GEO GDS SOFT file (NAME.soft), aggregation, correlation methods
  *Output: CSV files of gene-probe map and aggregated data (optional);
  *correlation and p-value matrices.
  *
  *Dependencies: java.io, javax.swing, JRI.Rengine, DataFunctions.R, stdlib, Analysis
  *
  *Compilation: javac -cp LIBRARIES:. DataAnalysis.java
  *Execution: ./run -cp LIBRARIES:. DataAnalysis
  **/

/*import libraries*/
import java.io.*;
import javax.swing.*;
import javax.swing.JOptionPane;
import org.rosuda.JRI.Rengine;
import org.rosuda.JRI.REXP;
import edu.princeton.cs.introcs.*;


public class DataAnalysis extends Analysis implements PreGraph {
    /*constants*/
    public final static String PEARSON = "pearson";
    public final static String SPEARMAN = "spearman";
    private final int BYTE = 8;
    private final int GB = 10;

    /*instance variables*/
    //rengine inherited from Analysis
    //user arguments
    private final String filePath;
    private final String fileName;
    private final String aggregateBy;
    private final String correlateBy;
    //R variable names
    private String gdsDT;
    private String aggDT;
    private String gpMap;
    private String corrEnv;
    //progress flags
    private boolean isAggregated;
    private boolean isCorrelated;

    
    /*constructors*/
    public DataAnalysis(File file, String aggregate, String correlate) {
        this(file.getAbsolutePath(), noExtension(file), aggregate, correlate);
    }
    //from a variable analysis
    public DataAnalysis(String filePath, String fileName, String aggregate, String correlate) {
        //start R
        super();

        this.filePath = filePath;
        this.fileName = "A" + fileName;

        gdsDT = this.fileName + "gdsDT";
        aggDT = this.fileName + "aggDT";
        gpMap = this.fileName + "gpMap";
        corrEnv = this.fileName + "corrEnv";
        aggregateBy = aggregate;
        correlateBy = correlate;
        isAggregated = false;
        isCorrelated = false;
    }
    
    
    /*process data: read in & aggregate*/
    public void processData() {
        //get file extension
        String ext = getExtension(filePath);
        
        //read file into R
        if (ext.equals(".soft")) {
            String read = String.format("%s <- readDataSOFT(\'%s\')", gdsDT, filePath);
            re.eval(read);
        }
        else if (ext.equals(".csv")) {
            String read = String.format("%s <- readDataCSV(\'%s\')", gdsDT, filePath);
            re.eval(read);
        }
        
        //aggregate data
        aggregateData();
    }

    public void aggregateData() {
        String aggregate = String.format("%s <- aggData(%s, method = \'%s\')", aggDT, gdsDT, aggregateBy);
        re.eval(aggregate);
        isAggregated = true;
    }

    /*estimate time and memory for correlation*/
    public String guessMem() {
        //get number of genes in dataset
        if (isAggregated) {
            String guess = String.format("n <- nrow(%s)", aggDT);
            re.eval(guess);
            long n = (long) re.eval("n").asInt();

        //compute memory needs
            double mem;
            String memNeeds;
            long need = n * n * 8;
            long bytetoGB = 1;
            for (int i = 0; i < GB; i++) {
                bytetoGB = bytetoGB * BYTE;
            }
            if (need > bytetoGB) {
                mem = (double) need / bytetoGB;
                memNeeds = String.format("%1$,.1f GB", mem);
            }
            else {
                long bytetoMB = 1;
                for (int i = 0; i < (GB - 3); i++) {
                    bytetoMB = bytetoMB * 8;
                }
                bytetoMB = bytetoMB / 2;
                mem = (double) need / bytetoMB;
                memNeeds = String.format("%1$,.1f MB", mem);
            }
            return memNeeds;
        }
        else
            return null;
    }
    
    /*write gene probe map as .csv to user's current directory*/
    public void outGPmap() {
        String write = String.format("%s <- geneProbe(%s)", gpMap, gdsDT);
        re.eval(write);
        String fname = filePath + "_gene_probe.csv";
        String out = String.format("outFile(%s, \'%s\')", gpMap, fname);
        re.eval(out);
    }
    
    /*write aggregated data as .csv to user's current directory*/
    public void outAggData() {
        if(isAggregated) {
            String fname = filePath + "_aggregated_data.csv";
            String out = String.format("outFile(%s, \'%s\')", aggDT, fname);
            re.eval(out);
        }
        else
            noAggregatedData();
    }
    
    /*generate correlation and p-value matrices*/
    public void corrData() {
        if(isAggregated) {
            String corr = String.format("%s <- corrData(%s, %s)", corrEnv, aggDT, correlateBy);
            re.eval(corr);
            re.eval("gc()");
            isCorrelated = true; //garbage collect
        }
        else
            noCorrelatedDataWarning();
    }
    
    /*save correlation data*/
    public void saveCorrData() {
        if(isCorrelated) {
            String save = String.format("saveCorrData(%s, \'%s\')", corrEnv,filePath);
            re.eval(save);
        }
        else
            noCorrelatedDataWarning();
    }
    
    /*write correlation data to csv file*/
    public void writeCorrData(boolean writeCor, boolean writeP) {
        if(isCorrelated){
            //write correlation data
            if (writeCor) {
                String writefile = filePath + "_correlation_data.csv";
                String write = String.format("writeCorrData(%s$corrVec, %s$index, \'%s\')", corrEnv, corrEnv, writefile);
                re.eval(write);
            }

            //write p-value data
            if (writeP) {
                String writefile = filePath + "_pval_data.csv";
                String write = String.format("writeCorrData(%s$pVec, %s$index, \'%s\')", corrEnv, corrEnv, writefile);
                re.eval(write);  
            }
        }
        else
            noCorrelatedDataWarning();
    }

    /*update flags*/
    public void changeAggregated(boolean change) {
        isAggregated = change;
    }

    /*no aggregated data warning*/
    private static void noAggregatedData() {
        JFrame frame = new JFrame();
        JOptionPane.showMessageDialog(frame, "Unable to access aggregated data. Please make sure you have uploaded \n correctly formatted .csv or .soft files.", 
          "Warning", 
          JOptionPane.WARNING_MESSAGE);
    }

    /*no correlated data warning*/
    private static void noCorrelatedDataWarning() {
        JFrame frame = new JFrame();
        JOptionPane.showMessageDialog(frame, "Unable to access correlated data. Please make sure you have uploaded \n correctly formatted .csv or .soft files.", 
          "Warning", 
          JOptionPane.WARNING_MESSAGE);
    }

    /*remove ALL objects from R*/
    /*careful! this should ONLY be called after saving/writing all data*/
    public void clearR() {
        super.clearR();
        isCorrelated = false;
        isAggregated = false;
    }

    
    /*TEST MAIN*/
    public static void main(String[] args) { 
        Stopwatch timer = new Stopwatch();

        File file = new File("/Users/rachelxu/SPE/GDS4457.soft");

        DataAnalysis a = new DataAnalysis(file, "Mean", "pearson");
        System.out.println("Analysis a created after " + timer.elapsedTime() + " seconds.");
        
        a.processData();
        System.out.println("Data processed & aggregated after " + timer.elapsedTime() + " seconds.");

        System.out.println("Correlation will require " + a.guessMem() + " of memory");
        
        a.corrData();
        System.out.println("Data correlated after " + timer.elapsedTime() + " seconds.");
        
        a.saveCorrData();
        System.out.println("Correlated matrices saved after " + timer.elapsedTime() + " seconds.");

        //boolean yescor = true;
        //boolean yesp = false;
        //a.writeCorrData(yescor, yesp);
        //System.out.println("Matrix written out after " + timer.elapsedTime() + " seconds.");
        
        a.endR();
        System.out.println("Done!");
        
    }
}