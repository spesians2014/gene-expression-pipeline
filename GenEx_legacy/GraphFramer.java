/** GraphFramer
* provides JFrame structure for graph viewer
* 
* Subclasses must implement:
* buildGraphPanel, buildMenuBar, addResizeListener, ResizeListener, aKeyPanel,
* hideIsolates, showIsolates, saveGraph, stopJittering
**/ 

//Screenshot libraries
import edu.uci.ics.screencap.Dump;
import edu.uci.ics.screencap.PNGDump;

//GUI libraries
import java.awt.*;
import java.awt.geom.*;
import java.awt.geom.Point2D;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.*;
import java.awt.event.*;

public abstract class GraphFramer extends JPanel implements ActionListener {
	/**
	* instance variables
	**/
	protected JFrame frame;
	protected JPanel graphPanel;
	protected JMenuBar menuBar;
	protected JPanel isolatePanel;
	protected JPanel savePanel;
	protected JTextField saveName;
	protected JPanel topPanel;
	protected JPanel bottomPanel;

	protected JButton saveGraph;
	protected JButton saveScreen;
	protected JButton hideIsolates;
	protected JButton showIsolates;

	protected boolean addKey;
	protected String TITLE;


	/**
	* empty constructor
	**/
	public GraphFramer() {
		;
	}


	/**
	* create and show GUI
	**/
	protected void createAndShowGUI(boolean addKey) {
		this.addKey = addKey;

		frame = new JFrame(TITLE);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		//building viewing components 
		buildGraphPanel();
		buildMenuBar();
		buildIsolatePanel();
		buildSavePanel();
		
		//add components to JFrame
		addTopPanel();
		addBottomPanel();
		frame.pack();

		//display JFrame
		frame.setVisible(true);

		//add resize listener
		this.addResizeListener();

		//stop jittering!
		stopJittering();
	}


	/**
	* invoked in create and show GUI: building framer
	**/
	//build graph viewer to panel
	abstract void buildGraphPanel();

	//build menu bar
	abstract void buildMenuBar();

	//build isolates panel
	protected void buildIsolatePanel() {
		isolatePanel = new JPanel();

		//remove isolates button
		hideIsolates = new JButton("Hide isolates");
		hideIsolates.addActionListener(this);
		isolatePanel.add(hideIsolates);
		hideIsolates.setVisible(true);

        //show isolates button
		showIsolates = new JButton("Show isolates");
		showIsolates.addActionListener(this);
		isolatePanel.add(showIsolates);
		showIsolates.setVisible(false);

        //if a key needs to be included, add it here
		if (addKey)
			isolatePanel.add(aKeyPanel());
	};

	//build panel for saving image
	protected void buildSavePanel() {
		savePanel = new JPanel();

		// input area for file name
		saveName = new JTextField("NetworkGraph", 15);
		saveName.setActionCommand("Save image as: ");
		saveName.addActionListener(this);

        // input area for file name label
		JLabel nameLabel = new JLabel("Save image as: ");
		nameLabel.setLabelFor(saveName);

        // save Buttons
		saveScreen = new JButton("Save Screenshot");
		saveScreen.addActionListener(this);
		saveGraph = new JButton("Save Full Graph");
		saveGraph.addActionListener(this);

        // add content to panel
		savePanel.add(nameLabel);
		savePanel.add(saveName);
		savePanel.add(saveGraph);
		savePanel.add(saveScreen);
		savePanel.setBackground(Color.WHITE);
	}

	//build & add top container panel
	protected void addTopPanel() {
		//build
		topPanel =  new JPanel(new GridLayout(0, 1));
		JLabel label1 = new JLabel("ALL MODES: right click - delete edge/vertex");
		JLabel label2 = new JLabel("TRANSFORMING: drag - translation; scroll - zoom");
		JLabel label3 = new JLabel("PICKING: click + drag - move vertex");
		topPanel.add(label1);
		topPanel.add(label2);
		topPanel.add(label3);
		topPanel.setBackground(Color.WHITE);

		//add to frame
		frame.add(topPanel, BorderLayout.PAGE_START);
	}

	//build & add bottom container panel
	protected void addBottomPanel() {
		//build
		bottomPanel = new JPanel(new GridLayout(0, 1));
		bottomPanel.add(isolatePanel);
		bottomPanel.add(savePanel);

		//add to frame
		frame.add(bottomPanel, BorderLayout.PAGE_END);
	}

	//add resize listener
	abstract void addResizeListener();

	//add key panel
	abstract JPanel aKeyPanel();

	//stop jittering
	abstract void stopJittering();


	/**
	* action listener
	**/
	public void actionPerformed(ActionEvent e) {
		//save graph
		if (e.getSource() == saveGraph) {
			saveGraph();
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog(frame, "The graph has been saved " +
				"into your directory.");
		}

		//save screenshot
		if (e.getSource() == saveScreen) {
			saveScreenShot();
			JFrame frame1 = new JFrame();
			JOptionPane.showMessageDialog(frame1, "The image has been saved into " +
				"your directory.");
		}

		//hide isolates
		if (e.getSource() == hideIsolates) {
			hideIsolates();
		}

		//show isolates
		if (e.getSource() == showIsolates) {
			showIsolates();
		}
	}


	/**
	* show/hide isolates
	**/
	abstract void hideIsolates();
	abstract void showIsolates();


	/**
	* saving graph image
	**/
	abstract void saveGraph();
	protected void saveScreenShot() {
		Dump dumper = new PNGDump();
		try {
			dumper.dumpComponent(new File(saveName.getText() + ".png"),graphPanel);
		}
		catch (IOException e1) {
			;
		}
	}

}
