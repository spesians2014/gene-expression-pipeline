/**VarAnalysis
  *Allows user to select variables from read-in SOFT file
  *
  *Dependencies: java.io, javax.swing, JRI.Rengine, GraphFunctions.R, stdlib, InteractGraph, Analysis
  *
  *Compilation: javac -cp LIBRARIES:. VarAnalysis.java
  *Execution: ./run -cp LIBRARIES:. VarAnalysis
  **/

/*import libraries*/

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.filechooser.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import org.rosuda.JRI.Rengine;
import org.rosuda.JRI.REXP;

import edu.princeton.cs.introcs.*;


public class VarAnalysis extends Analysis implements PreGraph {
	/**
	* instance variables
	**/
	//names
	private final String filePath;
	private final String fileName;
	private final String rootPath;
	//calculation methods
	private String aggregateBy;
	private String correlateBy;
	//set of DataAnalysis
	private String[] names;
	private String[] paths;
	private DataAnalysis[] data;
	//make sure we've chosen something to go by...
	private int n;


	/**
	* constructor
	**/
	public VarAnalysis(File file) {
		//start R
		super();

		//initialize things
		filePath = file.getAbsolutePath();
		fileName = noExtension(file);
		rootPath = getRootPath(file);
		aggregateBy = null;
		correlateBy = null;
		names = null;
		paths = null;
		data = null;
		n = -1;

		//read in GDS file
		parseGEO(); 
	}

	/*get file path + file name before the last '.'*/
	public static String getRootPath(File file) {
		String dir = null;
		String path = file.getAbsolutePath();
		int i = path.lastIndexOf('.');
		if (i > 0 && i < path.length() - 1)
			dir = path.substring(0, i);
		return dir;
	}


	/*read in GEO SOFT file*/
	private void parseGEO() {
		String read = String.format("gds <- parseGEO(\'%s\')", filePath);
		re.eval(read);
	}


	/*get sample information from gds
	you'll want to use a try-catch with this to catch a null pointer
	in case there is no varEnv*/
	public String[] getVariables() {
		//collect variable information from R
		String getVars = String.format("varEnv <- getVariables(gds)");
		re.eval(getVars);

		//return different variable information as String array
		String[] vars = re.eval("varEnv$names").asStringArray();
		return vars;
	}


	/*user selects which variable they want to sort data by;
	returns index
	now we want to create several data tables based on that variable criteria
	and DataAnalysis for those data tables*/
	public void makeDAs(int v, String aggregate, String correlate) {
		if (v < 1 || aggregate == null || correlate == null)
			throw new NullPointerException();
		//i'll figure out what to do here later...

		aggregateBy = aggregate;
		correlateBy = correlate;

		//create R environment containing mini-gdsDTs
		String make = String.format("DTEnv <- makeDTs(%d, varEnv, gds)", v);
		re.eval(make);

		//fill in names
		getNames(v);
		n = names.length;

		//extract gdsDT & create new DataAnalysis for each subgroup
		data = new DataAnalysis[n];
		for (int i = 0; i < n; i++) {
			extractDT(i);
			data[i] = toDataAnalysis(paths[i], names[i]);
		}
	}

	/*fill in names and paths of sub-groups*/
	private void getNames(int v) {
		//extract DTs from DTEnvs
		//get names to append
		String getNames = String.format("names <- getNames(%d, varEnv)", v);
		re.eval(getNames);
		String[] appendNames = re.eval("names").asStringArray();
		int n = appendNames.length;

		//create names for DataAnalysis
		names = new String[n];
		paths = new String[n];
		for (int i = 0; i < n; i++) {
			names[i] = fileName + "_" + appendNames[i];
			paths[i] = rootPath + "_" + appendNames[i];
		}
	}

	/*to DataAnalysis: creates new data analysis*/
	private DataAnalysis toDataAnalysis(String path, String name) {
		DataAnalysis da = new DataAnalysis(path, name, aggregateBy, correlateBy);
		return da;
	}

	/*extract DT from DTEnv*/
	private void extractDT(int i) {
		String dt = "A" + names[i] + "gdsDT";
		String dtEnv = String.format("DTEnv$DT%d", i+1);
		String extract = String.format("dt <- assign(%s, %s, env = parent.env(DTEnv))", dt, dtEnv);
		re.eval(dt + " <- " + dtEnv);
	}

	/**
	* wrappers for DataAnalysis functions
	**/
	public void aggregate() {
		for (int i = 0; i < n; i++)
			data[i].aggregateData();
	}

	public void outGPmap() {
		//only need one!
		data[0].outGPmap();
	}

	public void outAggData() {
		for (int i = 0; i < n; i++)
			data[i].outAggData();
	}

	public String guessMem() {
		/*estimate time and memory for correlation*/
		String old = data[0].guessMem();
		String[] split = old.split(" ");
		double guess = n * Double.parseDouble(split[0]);
		String newGuess = String.format("%1$,.1f %2$s", guess, split[1]);
		return newGuess;
	}

	public void corrData() {
		for (int i = 0; i < n; i++)
			data[i].corrData();
	}

	public void saveCorrData() {
		for (int i = 0; i < n; i++)
			data[i].saveCorrData();
	}

	public void writeCorrData(boolean writeCor, boolean writeP) {
		for (int i = 0; i < n; i++)
			data[i].writeCorrData(writeCor, writeP);
	}


	/**
	* test main
	**/
	public static void main(String[] args) {
		String fileName = args[0];
		File file = new File(fileName);

		VarAnalysis v = new VarAnalysis(file);
		String[] vars = v.getVariables();
		for (int i = 0; i < vars.length; i++)
			StdOut.println((i + 1) + ": " + vars[i]);

		int s = StdIn.readInt();
		v.makeDAs(s, "Mean", "pearson");
		System.out.println("Made DAs");
		v.aggregate();
		System.out.println("agg\'d DAs");
		System.out.println(v.guessMem());
	}

}
