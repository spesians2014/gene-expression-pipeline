/**
* GenEx GUI DoubleGraph
* Creates a panel with the interactive graph, with the JUNG package
* colors are different if edges are unique to graph or shared by graphs
*
* Depends: SingleGraph
**/

/*graph libraries*/
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.algorithms.layout.*;
import edu.uci.ics.jung.visualization.*;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.control.*;
import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.Transformer;
/*GUI libraries*/
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.geom.*;
import java.awt.geom.Point2D;
import javax.swing.*;
import javax.swing.border.*;
import javax.imageio.ImageIO;
import java.io.*;
import java.awt.event.*;
/*other*/
import java.util.Collection;
import java.util.Vector;
import java.util.HashSet;


public class DoubleGraph extends SingleGraph {
    /**
    * constants and instance variables
    **/
    //inherits many from SingleGraph
    private final Color BOTH_COLOR = Color.GREEN;
    private final Color G1_COLOR = Color.BLUE;
    private final Color G2_COLOR = Color.RED;
    private Vector<Color> edgeColors;
    private String graph1;
    private String graph2;
    private boolean toggleOn;


    /**
    * constructors
    **/
    public DoubleGraph(GraphData.EdgeList data1, GraphData.EdgeList data2, String graph1, String graph2) {
        //check
        if (!data1.isValid() || !data2.isValid()) {
            throw new NullPointerException();
        }

        //set default width, height, name
        WIDTH = DEFAULT_WIDTH;
        HEIGHT = DEFAULT_HEIGHT;
        this.graph1 = graph1;
        this.graph2 = graph2;
        TITLE = graph1 + " and " + graph2;
        needsKey = true;
        canToggle = true;
        toggleOn = false;

        highlight = new HashSet<String>();
        this.setLayout(new BorderLayout());

        createGraph(data1, data2); //create graph
        createLayout(); //create graph layout
        setAppearance(); //set graph appearance
        addMenuBar(); //add menu bar

        this.add(vv, BorderLayout.CENTER); //add viewer

        addResizeListener(); //add resizing listener
    }


    /**
    * create graph
    **/ 
    protected void createGraph(GraphData.EdgeList data1, GraphData.EdgeList data2) {
        g = new UndirectedSparseGraph<String, Integer>();
        V = data1.names().length;

        addEdges(data1, data2); //add all edges & connected vertices
        fillIsolates(data1, data2); //get isolates
    }

    /*add edges to graph*/
    protected void addEdges(GraphData.EdgeList data1, GraphData.EdgeList data2) {
        edgeWeights = new Vector<Float>(V);
        edgeColors = new Vector<Color>(V);
        int edgeCount = 0;

        //which graph has more edges?
        GraphData.EdgeList big = null;
        GraphData.EdgeList little = null;
        if (data1.E() > data2.E()) {
            big = data1;
            little = data2;
            this.graph1 = graph1;
            this.graph2 = graph2;
        }
        else {
            big = data2;
            little = data1;
            this.graph1 = graph2;
            this.graph2 = graph1;
        }
        

        //add bigger EdgeList first
        for (int i = 0; i < big.E(); i++) {
            //get vertex names
            String first = big.getFirstName(i);
            String second = big.getSecondName(i);

            //add vertices -- does nothing if graph already contains vertices
            g.addVertex(first);
            g.addVertex(second);

            //add edge
            g.addEdge(edgeCount++, first, second);
            edgeWeights.add(big.values()[i]);
            edgeColors.add(G1_COLOR);
        }

        //then add smaller
        for (int i = 0; i < little.E(); i++) {
            //get vertex names
            String first = little.getFirstName(i);
            String second = little.getSecondName(i);

            //does the graph already contain an edge between these vertices?
            Integer e;
            if ((e = g.findEdge(first, second)) != null) {
                //if it already contains the edge, change the color to both
                edgeColors.set(e, BOTH_COLOR);
            }
            else {
                //if the graph does not already contain an edge
                //add vertices -- does nothing if graph already contains vertices
                g.addVertex(first);
                g.addVertex(second);

                //add edge
                g.addEdge(edgeCount++, first, second);
                edgeWeights.add(little.values()[i]);
                edgeColors.add(G2_COLOR);
            } 
        }
    }

    /*fill in isolates vector -
    case sensitive, but shouldn't be an issue*/
    protected void fillIsolates(GraphData.EdgeList data1, GraphData.EdgeList data2) {
        String[] names1 = data1.names();
        String[] names2 = data2.names();

        //whoo fast lookup
        HashSet<String> verts = new HashSet<String>(g.getVertices());
        isolates = new Vector<String>();

        //add to isolates vector
        for (int i = 0; i < names1.length; i++) {
            if (!verts.contains(names1[i])) {
                isolates.add(names1[i]);
            }
        }
        for (int i = 0; i < names2.length; i++) {
            if (!verts.contains(names2[i])) {
                isolates.add(names2[i]);
            }
        }
    }


    @Override
    /*color edges; copied from SingleGraph*/
    protected void colorEdges() {
        edgePaint = 
        new Transformer<Integer, Paint>() {
            public Paint transform(Integer e) {
                Color color = edgeColors.elementAt(e);

                //check whether to paint the unique portions or the overlap potion
                if (toggleOn) {
                    if ((color == G1_COLOR) || (color == G2_COLOR)) {
                        color = EDGE_COLOR;
                    }
                }
                else {
                    if (color == BOTH_COLOR) {
                        color = EDGE_COLOR;
                    }
                }

                return color;
            }
        };
    }


    @Override
    /*add key panel*/
    public JPanel keyPanel() {
        //panel
        JPanel keyPanel = new JPanel();
        keyPanel.setLayout(new GridLayout(0, 1));

        //text
        JLabel commonLabel = new JLabel("Common to both graphs");
        JLabel oneLabel = new JLabel("Unique to " + graph1);
        JLabel twoLabel = new JLabel("Unique to " + graph2);

        //set colors based on toggle state
        if (toggleOn) {
            commonLabel.setForeground(BOTH_COLOR);
            oneLabel.setForeground(EDGE_COLOR);
            twoLabel.setForeground(EDGE_COLOR);
        }
        else {
            commonLabel.setForeground(EDGE_COLOR);
            oneLabel.setForeground(G1_COLOR);
            twoLabel.setForeground(G2_COLOR);
        }

        //add text to panel
        keyPanel.add(commonLabel);
        keyPanel.add(oneLabel);
        keyPanel.add(twoLabel);

        return keyPanel;
    }


    @Override
    /*toggle edge colors*/
    public void toggleColors() {
        toggleOn = !toggleOn;
        this.repaint();
    }

    /*get toggle status*/
    public boolean isToggleOn() {
        return toggleOn;
    }

}
