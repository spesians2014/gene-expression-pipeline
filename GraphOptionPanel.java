/**
* GenEx GUI GraphOption
* Panel with data/basic graph manipulation options for a SingleGraph
*
* Depends: SingleGraph, Java Swing
**/

/*import libraries*/
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.*;
import java.util.Vector;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import edu.uci.ics.screencap.Dump;
import edu.uci.ics.screencap.PNGDump;
import edu.princeton.cs.introcs.*;


/**
* GraphOptionPanel
* Builds the option panel
**/
class GraphOptionPanel extends JPanel implements ActionListener {
	/**
	* instance variables/constants
	**/
	public static final int WIDTH = 250;
	private final EmptyBorder PADDING = new EmptyBorder(5, 1, 5, 1);
	private final String TOGGLE_OFF = "Highlight Intersection";
	private final String TOGGLE_ON = "Highlight Differences";
	private final String LABEL_ON = "Hide labels";
	private final String LABEL_OFF = "Show labels";
	private final String LOCK_ON = "Unfreeze graph";
	private final String LOCK_OFF = "Freeze graph";
	private final Dimension MAX_COMPONENT_SIZE = new Dimension(WIDTH, 175);
	private final int ITERATIONS = 1500;
	//graph
	private SingleGraph sg;
	private GraphData gd;
	//GUI components
	//graph information
	private JPanel infoPanel;
	private JLabel vertexInfo;
	private JLabel edgeInfo;
	//key
	private JPanel keyPanel;
	//intersection highlight
	private JButton toggleButton;
	//labels on/off
	private JButton labelButton;
	private boolean labelOn;
	//lock on/off
	private JButton lockButton;
	private boolean lockOn;
	//highlight vertices
	private JButton highlightButton;
	private JButton clearButton;
	private JButton stopHighlightButton;
	private DefaultListModel<String> highlightList;
	private ListViewerPanel highlightViewer;
	private Java2sAutoComboBox highlightField; //autocomplete box
	private JButton addHighlightButton;
	//reset layout
	private JButton layoutButton;
	//isolates
	private JScrollPane isolatesPanel;
	//save
	private JButton saveButton;
	private JButton logButton;
	private JButton adjacencyButton;
	//other
	private double pval;
	private double tau;
	private GraphFramer gf;


	/**
	* constructor
	**/
	public GraphOptionPanel(SingleGraph sg, double pval, double tau, GraphData gd, GraphFramer gf) {
		this.sg = sg;
		this.gd = gd;
		this.pval = pval;
		this.tau = tau;
		this.gf = gf;

		//set panel attributes
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setBorder(PADDING);
		this.setMaximumSize(new Dimension(WIDTH, 1500)); //arbitrary height
		this.setPreferredSize(new Dimension(WIDTH, SingleGraph.DEFAULT_HEIGHT));

		//add components
		this.addInfoPanel();
		this.addKeyPanel();
		this.addManyButtons();
		this.addHighlightPanel();
		//this.addIsolatesPanel();
		this.addSaveButtons();
		this.addFiller();
	}


	/**
	* add components
	**/
	/*add information panel (title, parameters, etc.)*/
	private void addInfoPanel() {
		//parent panel
		infoPanel = new JPanel();
		infoPanel.setBorder(
			BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Network graph information"),
				PADDING
				)
			);
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
		infoPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		infoPanel.setMaximumSize(MAX_COMPONENT_SIZE);

		//title
		JLabel title = new JLabel("<html><div WIDTH=245>" + sg.getTitle() + "<br></div></html>");
		title.setMaximumSize(new Dimension(WIDTH-10, 50));
		title.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 16));
		title.setHorizontalAlignment(JLabel.CENTER);
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		//padding
		JPanel padding = new JPanel();
		padding.setMaximumSize(new Dimension(WIDTH, 5));

		//other information
		JPanel otherInfo = new JPanel();
		otherInfo.setLayout(new GridLayout(2, 2));
		otherInfo.setAlignmentX(Component.CENTER_ALIGNMENT);
		//text in other information
		String pString = String.format("P-value: %1$,.2f", pval);
		JLabel pInfo = new JLabel(pString);
		String tauString = String.format("Threshold: %1$,.2f", tau);
		JLabel tauInfo = new JLabel(tauString);
		String vertexString = String.format("Vertices: %d", sg.vertexCount());
		vertexInfo = new JLabel(vertexString);
		String edgeString = String.format("Edges: %d", sg.edgeCount());
		edgeInfo = new JLabel(edgeString);

		//add components
		otherInfo.add(pInfo);
		otherInfo.add(vertexInfo);
		otherInfo.add(tauInfo);
		otherInfo.add(edgeInfo);

		infoPanel.add(title);
		infoPanel.add(padding);
		infoPanel.add(otherInfo);

		this.addPadding();
		this.add(infoPanel);
		this.addPadding();
	}


	/*add key panel*/
	private void addKeyPanel() {
		if (sg.needsKey()) {
			JPanel parentPanel = new JPanel();
			parentPanel.setLayout(new BorderLayout());
			parentPanel.setMaximumSize(new Dimension(WIDTH, 85));
			parentPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
			parentPanel.setBorder(
				BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("Graph key"),
					PADDING
					)
				);

			//add key panel
			keyPanel = sg.keyPanel();
			parentPanel.add(keyPanel, BorderLayout.PAGE_START);

			//add toggle button
			toggleButton = new JButton();
			toggleButton.setText(TOGGLE_OFF);
			toggleButton.setFocusPainted(false);
			toggleButton.addActionListener(this);
			parentPanel.add(toggleButton, BorderLayout.PAGE_END);

			//add to panel
			this.add(parentPanel);
			this.addPadding();
		}
	}


	/*add label show/hide button, graph lock button, and layout re-run button*/
	private void addManyButtons() {
		//container panel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonPanel.setMaximumSize(new Dimension(WIDTH, 40));

		//label button
		labelButton = new JButton();
		labelButton.setFocusPainted(false);
		labelButton.addActionListener(this);
		if (sg.vertexCount() <= SingleGraph.TOO_MANY) {
			labelButton.setText(LABEL_ON);
			labelOn = true;
		}
		else {
			labelButton.setText(LABEL_OFF);
			labelOn = false;
		}

		//freeze button
		lockButton = new JButton(LOCK_OFF);
		lockButton.setFocusPainted(false);
		lockButton.addActionListener(this);
		lockOn = false;

		//layout button
		layoutButton = new JButton("Redo layout");
		layoutButton.setFocusPainted(false);
		layoutButton.addActionListener(this);

		//add
		buttonPanel.add(labelButton, BorderLayout.PAGE_START);
		buttonPanel.add(lockButton, BorderLayout.LINE_START);
		buttonPanel.add(layoutButton, BorderLayout.LINE_END);
		this.add(buttonPanel);
		this.addPadding();
	}


	/*add highlight panel*/
	private void addHighlightPanel() {
		JPanel highlightPanel = new JPanel();
		highlightPanel.setMaximumSize(new Dimension(WIDTH, 235));
		highlightPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		highlightPanel.setLayout(new BoxLayout(highlightPanel, BoxLayout.Y_AXIS));
		highlightPanel.setOpaque(false);
		highlightPanel.setBorder(
			BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Highlight vertices"),
				PADDING
				)
			);

		//add genes
		JPanel addPanel = new JPanel();
		addPanel.setMaximumSize(new Dimension(WIDTH, 20));
		addPanel.setLayout(new BorderLayout());
		addPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		//list of vertices in graph
		Vector tempVertices = new Vector(sg.getVertices());
		highlightField = new Java2sAutoComboBox(tempVertices);
		highlightField.setSelectedItem("");
		highlightField.addActionListener(this);
		highlightField.getTextField().setFocusable(true);
		highlightField.getTextField().addMouseListener
		(new MouseInputAdapter() {
			public void mouseClicked(MouseEvent e) {
				highlightField.getTextField().requestFocusInWindow();
			}

		});
		highlightField.getTextField().addKeyListener
		(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				if (key == KeyEvent.VK_ENTER) {
					addToHighlightList();
				}
			}
		});
		highlightField.setPreferredSize(new Dimension(150, 20));
		//add button
		addHighlightButton = new JButton("Add");
		addHighlightButton.setFocusPainted(false);
		addHighlightButton.addActionListener(this);
		//add to subpanel
		addPanel.add(highlightField, BorderLayout.LINE_START);
		addPanel.add(addHighlightButton, BorderLayout.LINE_END);

		//list and list viewer
		highlightList = new DefaultListModel<String>();
		highlightViewer = new ListViewerPanel(highlightList);
		highlightViewer.setAlignmentX(Component.CENTER_ALIGNMENT);
		highlightViewer.setMaximumSize(new Dimension(WIDTH, 120));

		//highlight/clear buttons
		JPanel buttonPanel = new JPanel();
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonPanel.setMaximumSize(new Dimension(WIDTH, 20));
		buttonPanel.setLayout(new BorderLayout());
		//buttons
		highlightButton = new JButton("Highlight");
		highlightButton.setFocusPainted(false);
		highlightButton.addActionListener(this);
		clearButton = new JButton("Clear list");
		clearButton.setFocusPainted(false);
		clearButton.addActionListener(this);
		stopHighlightButton = new JButton("Clear highlights");
		stopHighlightButton.setFocusPainted(false);
		stopHighlightButton.addActionListener(this);
		//add to subpanel
		buttonPanel.add(highlightButton, BorderLayout.PAGE_START);
		buttonPanel.add(clearButton, BorderLayout.LINE_START);
		buttonPanel.add(stopHighlightButton, BorderLayout.LINE_END);

		//add
		highlightPanel.add(addPanel);
		highlightPanel.add(Box.createVerticalGlue());
		highlightPanel.add(highlightViewer);
		highlightPanel.add(Box.createVerticalGlue());
		highlightPanel.add(buttonPanel);
		this.add(highlightPanel);
		this.addPadding();
	}


	/*add isolates panel
	private void addIsolatesPanel() {
		//get isolates list
		JList<String> isolatesList = new JList<String>(sg.getIsolates());
		int size = sg.getIsolates().size();
		
		//add to scroll panel
		isolatesPanel = new JScrollPane(isolatesList);
		String isolatesTitle = String.format("Unconnected vertices: %d", size);

		//set panel attributes
		isolatesPanel.setMaximumSize(new Dimension(WIDTH, 125));
		isolatesPanel.setPreferredSize(new Dimension(WIDTH, 125));
		isolatesPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		isolatesPanel.setOpaque(false);
		isolatesPanel.setBorder(
			BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(isolatesTitle),
				PADDING
				)
			);

		//add
		this.add(isolatesPanel);
		this.addPadding();
	}*/

	/*add save graph button, save adjacency matrix button, save log button*/
	private void addSaveButtons() {
		//container panel
		JPanel savePanel = new JPanel();
		savePanel.setLayout(new GridLayout(0, 1));
		savePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		savePanel.setMaximumSize(new Dimension(WIDTH, 60));

		//buttons
		saveButton = new JButton("Save image");
		saveButton.setFocusPainted(false);
		saveButton.addActionListener(this);
		logButton = new JButton("Save graph information");
		logButton.setFocusPainted(false);
		logButton.addActionListener(this);
		adjacencyButton = new JButton("Save adjacency data");
		adjacencyButton.setFocusPainted(false);
		adjacencyButton.addActionListener(this);

		//add
		savePanel.add(saveButton);
		savePanel.add(logButton);
		savePanel.add(adjacencyButton);
		this.add(savePanel);
		this.addPadding();
	}


	/*add padding*/
	private void addPadding() {
		JPanel padding = new JPanel();
		padding.setMaximumSize(new Dimension(WIDTH, 5));
		padding.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(padding);
	}
	/*add filler*/
	private void addFiller() {
		JPanel filler = new JPanel();
		filler.setBorder(PADDING);
		filler.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(filler);
	}


	/**
	* update vertex and edge info
	**/
	public void updateGraphInfo() {
		//update vertex label
		String vertexString = String.format("Graph Vertices: %d", sg.vertexCount());
		vertexInfo.setText(vertexString);
		//update edge label
		String edgeString = String.format("Edges: %d", sg.edgeCount());
		edgeInfo.setText(edgeString);
		//repaint GUI
		infoPanel.repaint();
	}


	/**
	* save screenshot of graph
	**/
	private void saveGraph() {
		//create fileChooser
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setAcceptAllFileFilterUsed(false);

        //open fileChooser
		int returnVal = fileChooser.showSaveDialog(this);

		//capture and save image if file selected
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			String fileName = fileChooser.getSelectedFile().getAbsolutePath() + ".png";
			Dump dumper = new PNGDump();
			try {
				dumper.dumpComponent(new File(fileName), sg.getViewer());
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	/**
	* save adjacency matrix
	**/
	private void saveMatrix() {
		//create fileChooser
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setAcceptAllFileFilterUsed(false);

        //open fileChooser
		int returnVal = fileChooser.showSaveDialog(this);
		//get filename
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			String fileName = fileChooser.getSelectedFile().getAbsolutePath() + ".txt";
			gd.outAdjData(fileName);
		}
	}


	/**
	* save text log of graph:
	* contains: data file name(s), p-value, network threshold, genes graphed,
	* (connected and unconnected), genes not found
	**/
	private void saveLog() {
		//create fileChooser
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setAcceptAllFileFilterUsed(false);

        //open fileChooser
		int returnVal = fileChooser.showSaveDialog(this);
		//get filename
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			//create file output
			String fileName = fileChooser.getSelectedFile().getAbsolutePath() + ".txt";
			Out fileWriter = new Out(fileName);

			try { 
				//get genes and isolates
				Collection<String> genes = sg.getVertices();
				Vector<String> isolates = sg.getIsolates();
				Vector<String> notFound = gd.genesNotFound();

				//write data file names
				String nameString = String.format("Files used: %s", sg.getTitle());
				fileWriter.println(nameString);
				fileWriter.println();

				//write p-value, network threshold
				String pString = String.format("p-value: %1$,.2f", pval);
				String tauString = String.format("network threshold: %1$,.2f", tau);
				if (!gd.isList()) {
					fileWriter.println(pString);
				}
				fileWriter.println(tauString);
				fileWriter.println();

				//write vertices, edges
				String vertString = String.format("Connected graph vertices: %d", sg.vertexCount());
				String isolateString = String.format("Unconnected graph vertices: %d", isolates.size());
				String edgeString = String.format("Graph edges: %d", sg.edgeCount());
				fileWriter.println(vertString);
				fileWriter.println(isolateString);
				fileWriter.println(edgeString);

				fileWriter.println();

				//write genes graphed
				//connected genes
				fileWriter.println("Graphed, connected genes:");
				for (String s : genes) {
					fileWriter.println(s);
				}
				fileWriter.println();
				//unconnected genes
				fileWriter.println("Unconnected genes:");
				for (int i = 0; i < isolates.size(); i++) {
					fileWriter.println(isolates.elementAt(i));
				}
				fileWriter.println();

				//write genes not found
				fileWriter.println("Query genes not found:");
				for (int i = 0; i < notFound.size(); i++) {
					fileWriter.println(notFound.elementAt(i));
				}
				fileWriter.println();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			finally {
				fileWriter.close();
			}
		}
	}


	/**
	* action listener
	**/
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();

		//saving graph
		if (source == saveButton) {
			saveGraph();
		}
		//or adjusting labels
		else if (source == labelButton) {
			toggleLabels();
		}
		//or freezing graph
		else if (source == lockButton) {
			toggleLock();
		}
		//or reapplying layout
		else if (source == layoutButton) {
			reapplyLayout();
		}
		//or adjusting graph colors
		else if (source == toggleButton) {
			toggleColors();
		}
		//or adding to highlight list
		else if (source == addHighlightButton) {
			addToHighlightList();
		}
		//or highlighting vertices
		else if (source == highlightButton) {
			sg.highlightVertices(highlightList);
		}
		//or stop highlighting vertices
		else if (source == stopHighlightButton) {
			sg.stopHighlighting();
		}
		//or clear highlighted vertices list
		else if (source == clearButton) {
			highlightViewer.clearList();
			highlightList.clear();
			this.repaint();
			this.revalidate();
		}
		//or saving graph log
		else if (source == logButton) {
			saveLog();
		}
		//or saving adjacency matrix
		else if (e.getSource() == adjacencyButton) {
			saveMatrix();
		}
	}


	/*toggle labels*/
	private void toggleLabels() {
		labelOn = !labelOn;
		if (labelOn) {
			labelButton.setText(LABEL_ON);
			sg.showLabels();
		}
		else {
			labelButton.setText(LABEL_OFF);
			sg.hideLabels();
		}
	}

	/*toggle lock*/
	private void toggleLock() {
		lockOn = !lockOn;
		sg.getGraphLayout().lock(lockOn);
		if (lockOn) {
			lockButton.setText(LOCK_ON);
		}
		else {
			lockButton.setText(LOCK_OFF);
		}
		sg.repaint();
	}

	/*reapply layout*/
	private void reapplyLayout() {
		gf.setVisibleGlass(true); //glass pane visibility

		sg.getGraphLayout().lock(false);
		if (lockOn) {
			lockOn = false;
			lockButton.setText(LOCK_OFF);
		}
		sg.getGraphLayout().reset();
		for (int i = 0; i < ITERATIONS; i++) {
			sg.getGraphLayout().step();
		}
		sg.repaint();

		gf.setVisibleGlass(false);
	}

	/*toggle colors*/
	private void toggleColors() {
		//change graph colors
		sg.toggleColors();
			//repaint key
		keyPanel.removeAll();
		keyPanel.add(sg.keyPanel(), BorderLayout.PAGE_START);
		keyPanel.revalidate();
			//change button text
		if (((DoubleGraph)sg).isToggleOn()) {
			toggleButton.setText(TOGGLE_ON);
		}
		else {
			toggleButton.setText(TOGGLE_OFF);
		}
	}

	/*toggle highlight*/
	private void addToHighlightList() {
		String add = (String) highlightField.getSelectedItem();
		if (add.equals("") || highlightList.contains(add)) {
			return;
		}
		highlightList.addElement(add);
		highlightField.setSelectedItem("");
		this.repaint();
	}


	/*
	* test client
	*
	public static void main(String[] args) {
		File a = new File(args[0]);
		In adjIn = new In(a);
		double temp[] = adjIn.readAllDoubles();
		float adjVec[] = new float[temp.length];
		for (int i = 0; i < temp.length; i++) {
			adjVec[i] = (float) temp[i];
		}
		adjIn.close();

		/*for (int i = 0; i < adjVec.length; i++) {
			if (adjVec[i] > .9) {
				adjVec[i] += 1 + (int) (3 * Math.random());
			}
		}

		File b = new File(args[1]);
		In namesIn = new In(b);
		String[] names = namesIn.readAllStrings();
		namesIn.close();

		String title = "Hello";
		SingleGraph ig = new SingleGraph(adjVec, names, title);

		//GraphFramer f = new GraphFramer(ig, 0.05, 0.9, gd);
	}*/
}
