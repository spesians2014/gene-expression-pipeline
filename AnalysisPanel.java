/**
* GenEx GUI AnalysisPanel
* A progress bar; starts R analysis 
*
* Depends: Java Swing, utilities; AnalysisThread, FileCheck
* Parent: GenEx
**/

/*import libraries*/
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;


public class AnalysisPanel extends JPanel implements ActionListener {
	/**
	* instance variables/constants
	**/
	//constants
	//enum for which process to invoke
	public enum Process {ERROR, NORMAL, SPLIT, SOME, GRAPH};
	//dimensions
	private int WIDTH = (int) StartPanel.PADSIZE.getWidth();
	//instance variables
	//analysis arguments
	private File[] files;
	private int split;
	private Process doProcess;
	private AnalysisThread analysis;
	//GUI
	private JProgressBar progressBar;
	private JLabel bigtext;
	private JLabel status;
	private JButton backButton;
	private JButton cancelButton;
	private GenEx g;


	/**
	* constructor
	**/
	public AnalysisPanel(GenEx g) {
		//initialize some variables
		this.g = g;
		this.split = -1;
		this.files = g.getFiles();

		//set attributes
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setMaximumSize(GenEx.DEFAULT_SIZE);
		this.setOpaque(true);

		//add components
		this.addPadding();
		this.addPadding();
		bigtext = new JLabel("Please wait: analyzing...");
		bigtext.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 30));
		bigtext.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(bigtext);
		this.addProgressPanel();
		this.addButtons();
	}


	/**
	* build panel
	**/
	/*add padding*/
	private void addPadding() {
		JPanel padding = new JPanel();
		padding.setMaximumSize(StartPanel.PADSIZE);
		padding.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(padding);
	}

	/*add progress panel*/
	private void addProgressPanel() {
		//build parent panel
		JPanel progressPanel = new JPanel(new BorderLayout());
		progressPanel.setMaximumSize(StartPanel.PADSIZE);
		progressPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		//built components
		progressBar = new JProgressBar(0, 100);
		progressBar.setAlignmentX(Component.CENTER_ALIGNMENT);
		status = new JLabel("Pre-processing request...");
		status.setAlignmentX(Component.CENTER_ALIGNMENT);
		//add components
		progressPanel.add(progressBar, BorderLayout.CENTER);
		progressPanel.add(status, BorderLayout.PAGE_END);
		this.add(progressPanel);
	}

	/*add back & cancel buttons*/
	private void addButtons() {
		//build parent panel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.setMaximumSize(StartPanel.PADSIZE);
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		//build buttons
		backButton = new JButton("<< Back");
		backButton.addActionListener(this);
		backButton.setEnabled(true);
		backButton.setFocusPainted(false);
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(this);
		cancelButton.setEnabled(false);
		cancelButton.setFocusPainted(false);
		//add buttons
		buttonPanel.add(backButton);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(cancelButton);
		this.add(buttonPanel);
	}


	/**
	* start analysis!!
	**/
	/*start analysis or skip to graphing*/
	public void nextStep() {
		g.setVisibleGlass(true);
		//check files
		doProcess = FileCheck.checkAPFiles(files);
		if (doProcess == Process.GRAPH) {
			//skip to graphing
			toGraphPanel();
		}
		else {
			this.startAnalysis();
		}
	}
	/*start analysis*/
	private void startAnalysis() {
		this.setVisible(true);
		g.addAnalysisPanel();

		backButton.setEnabled(false);
		switch (doProcess) {
			case SPLIT: {
				//get how user wants to split data
				split = FileCheck.splitDataFrame(files[0]);
				g.setVisibleGlass(false);
				//start analysis
				analysis = new AnalysisThread(g);
				cancelButton.setEnabled(true); //enable cancel button
				analysis.execute(); //start analysis thread
				break;
			}
			case NORMAL: case SOME: {
				//start analysis thread
				g.setVisibleGlass(false);
				analysis = new AnalysisThread(g);
				cancelButton.setEnabled(true); //enable cancel button
				analysis.execute(); //start analysis thread
				break;
			}
			case ERROR: {
				g.setVisibleGlass(false);
				status.setText("ERROR: Could not process request.");
				backButton.setEnabled(true);
				break;
			}
		}
	}


	
	/**
	* access methods
	**/
	/*get progress bar*/
	public JProgressBar getProgressBar() {
		return progressBar;
	}
	/*get label*/
	public JLabel getLabel() {
		return status;
	}
	/*get process*/
	public AnalysisPanel.Process getProcess() {
		return doProcess;
	}
	/*get split method*/
	public int getSplit() {
		return split;
	}
	/*get cancel button*/
	public JButton getCancelButton() {
		return cancelButton;
	}
	/*get back button*/
	public JButton getBackButton() {
		return backButton;
	}
	/*get big text*/
	public void cancelText() {
		bigtext.setText("Analysis cancelled.");
	}
	/*get files*/
	public File[] getFiles() {
		return files;
	}


	/**
	* go to graph panel
	**/
	public void toGraphPanel() {
		g.toGraphPanel();
	}


	/**
	* action listener
	**/
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == cancelButton) {
			analysis.cancelThread();
			progressBar.setValue(0);
			progressBar.paintImmediately(0, 0, progressBar.getSize().width, progressBar.getSize().height);
			backButton.setEnabled(true);
			cancelButton.setEnabled(false);
		}
		else if (e.getSource() == backButton) {
			g.toFirst();
		}
	}
}
