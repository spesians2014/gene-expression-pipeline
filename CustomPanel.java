/**
* GenEx GUI custom settings panel
* Gives options for aggregation, correlation, file download
* returns options as argument array for DataAnalysis
*
* Depends: Java Swing, utilities
* Parent: StartPanel
**/

/*import libraries*/
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;


public class CustomPanel extends JPanel implements ActionListener {
	/**
	* instance variables/constants
	**/
	//dimensions
	public static final int WIDTH = 400;
	public static final int HEIGHT = 350;
	public static final Dimension DEFAULT_SIZE = new Dimension(WIDTH, HEIGHT);
	public static final int ARGS = 6; //number of options/arguments
	//parent frame
	private JFrame f = new JFrame("Custom settings");
	//aggregation & correlation methods
	private JComboBox<String> aggMethods;
	private JComboBox<String> corrMethods;
	//intermediate file download
	private JCheckBox aggData;
	private JCheckBox geneProbeMap;
	private JCheckBox corrMatrix;
	private JCheckBox pMatrix;
	//confirm/cancel buttons
	private JButton confirm;
	private JButton cancel;
	//parent frame
	private StartPanel sp;
	//file panel
	private FileUploadPanel filePanel;


	/**
	* constructor
	**/
	public CustomPanel(StartPanel sp) {
		this.sp = sp;

		//set panel attributes
		this.setPreferredSize(DEFAULT_SIZE);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		//add components
		JLabel title = new JLabel("Custom settings");
		title.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(title);
		this.addPadding();
		this.addFileOptions();
		this.addPadding();
		this.addCalcOptions();
		this.addConfirmButtons();

		//show panel
		this.displayPanel();
	}


	/**
	* add components
	**/
	
	/*intermediate file download*/
	private void addFileOptions() {
		//create parent panel
		JPanel fileOptionPanel = new JPanel();
		TitledBorder title = BorderFactory.createTitledBorder("Intermediate file download options");
		fileOptionPanel.setBorder(title);
		fileOptionPanel.setLayout(new GridLayout(2, 2));
		fileOptionPanel.setMaximumSize(new Dimension(WIDTH, 100));
		fileOptionPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

		//create checkboxes
		//aggData
		aggData = new JCheckBox("Aggregated dataset");
		aggData.setSelected(false);
		aggData.setFocusPainted(false);
		//geneProbeMap
		geneProbeMap = new JCheckBox("Gene-Probe map");
		geneProbeMap.setSelected(false);
		geneProbeMap.setFocusPainted(false);
		//corMatrix
		corrMatrix = new JCheckBox("Correlation matrix");
		corrMatrix.setSelected(false);
		corrMatrix.setFocusPainted(false);
		//pMatrix
		pMatrix = new JCheckBox("P-value matrix");
		pMatrix.setSelected(false);
		pMatrix.setFocusPainted(false);

		//add checkboxes
		fileOptionPanel.add(aggData);
		fileOptionPanel.add(geneProbeMap);
		fileOptionPanel.add(corrMatrix);
		fileOptionPanel.add(pMatrix);

		this.add(fileOptionPanel);
	}

	/*aggregation & correlation options*/
	private void addCalcOptions() {
		//parent panel
		JPanel calcOptionPanel = new JPanel();
		TitledBorder title = BorderFactory.createTitledBorder("Calculation options");
		calcOptionPanel.setBorder(title);
		calcOptionPanel.setLayout(new GridLayout(4, 1));
		calcOptionPanel.setMaximumSize(new Dimension(WIDTH, 140));
		calcOptionPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

		//create JComboBoxes
		//aggregation method options should match key in DataAnalysis!
		String[] aggMethodOptions = {"Select aggregation method", "Mean (default)", "Maximum Variance", "Maximum", "Minimum", "Maximum Mean", "Minimum Mean"};
		aggMethods = new JComboBox<String>(aggMethodOptions);
		aggMethods.setSelectedIndex(1);
		String[] corrMethodOptions = {"Select correlation method", "Pearson correlation (default)", "Spearman correlation"};
		corrMethods = new JComboBox<String>(corrMethodOptions);
		corrMethods.setSelectedIndex(1);

		//add comboboxes
		calcOptionPanel.add(new JLabel("  Aggregation method:"));
		calcOptionPanel.add(aggMethods);
		calcOptionPanel.add(new JLabel("  Correlation method:"));
		calcOptionPanel.add(corrMethods);

		this.add(calcOptionPanel);
	}

	/*confirm/cancel buttons*/
	private void addConfirmButtons() {
		//parent panel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setMaximumSize(new Dimension(WIDTH, 65));
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonPanel.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		//create buttons
		confirm = new JButton("Use custom settings");
		confirm.setFocusPainted(false);
		confirm.addActionListener(this);
		cancel = new JButton("Cancel");
		cancel.setFocusPainted(false);
		cancel.addActionListener(this);

		//add buttons
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(cancel);
		buttonPanel.add(confirm);

		this.add(buttonPanel);
	}

	/*add padding*/
	private void addPadding() {
		Dimension d = new Dimension(WIDTH, 15);
		JPanel padding = new JPanel();
		padding.setMaximumSize(d);
		padding.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(padding);
	}


	/**
	* Show/close panel
	**/
	/*create and show panel*/
	private void displayPanel() {
		f.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		f.setLocationRelativeTo(sp);
		f.add(this);
		f.pack();
		f.setVisible(true);
	}

	/*show panel*/
	public void showPanel() {
		f.setVisible(true);
	}

	/*hide panel*/
	public void hidePanel() {
		f.setVisible(false);
	}

	/*close panel -- permanently*/
	public void closePanel() {
		f.dispose();
	}



	/**
	* Get arguments (checkbox options, etc.)
	* Order: agg method, corr method, agg data, gpmap, corr matrix, p matrix
	* 1 is true, 0 is false
	**/
	public int[] getArguments() {
		int a = aggMethods.getSelectedIndex();
		int c = corrMethods.getSelectedIndex();
		int[] arguments = {
			((a == 0) ? 1 : a), //convert 0 index to default
			((c == 0) ? 1 : c),
			(aggData.isSelected() ? 1 : 0),
			(geneProbeMap.isSelected() ? 1 : 0),
			(corrMatrix.isSelected() ? 1 : 0),
			(pMatrix.isSelected() ? 1 : 0)
		};
		return arguments; 
	}


	/**
	* ActionListener
	**/
	/*action listener method, handles buttons*/
	public void actionPerformed (ActionEvent e) {
		//set arguments (option selections)
		if (e.getSource() == confirm) {
			//check corr/pMatrix download
			if (corrMatrix.isSelected() || pMatrix.isSelected()) {
				matrixDownloadWarning();
			}
			else {
				//send arguments to start panel
				sp.changeArguments(getArguments());
				hidePanel(); //hide panel
			}
		}
		else if (e.getSource() == cancel) {
			//send default arguments back to StartPanel
			sp.changeArguments(StartPanel.DEFAULT_ARGS);
			hidePanel(); //hide panel
		}
	}

	/*matrix download warning*/
	private void matrixDownloadWarning() {
		//ask if they would really like to download the matrices
		int choice = JOptionPane.showConfirmDialog(null, "Downloading the correlation and/or p-value matrices\nmay be extremely time and memory intensive.\nWould you like to use these settings anyway?", "Warning", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
		//if yes, let them download
		if (choice == JOptionPane.YES_OPTION) {
			//send new options back to StartPanel
			sp.changeArguments(getArguments());
			closePanel(); //close panel
		}
		//if no, uncheck buttons for them
		else {
			corrMatrix.setSelected(false);
			pMatrix.setSelected(false);
		}
	}
}

