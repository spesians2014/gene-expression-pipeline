/**
* GenEx GUI file upload panel
* Displays browse button and ListViewerPanel of uploaded files
*
* Depends: Java Swing, utilities; ListViewerPanel; ListViewerListener
* Parent: StartPanel
**/

/*import libraries*/
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;


public class FileUploadPanel extends JPanel implements ActionListener {
	/**
	* instance variables
	**/
	public static final Dimension DEFAULT_SIZE = new Dimension(400, 125);
	public static final Dimension PADDING = new Dimension(400, 8);
	public final int MAX_FILES;
	private JLabel uploadLabel;
	private JButton browseButton;
	private JFileChooser fileChooser;
	private DefaultListModel<String> list;
	private DefaultListModel<File> fileList;


	/**
	* constructor
	**/
	public FileUploadPanel(int max) {
		MAX_FILES = max; 
		fileChooser = new JFileChooser();
		list = new DefaultListModel<String>();
		fileList = new DefaultListModel<File>();

		//panel attributes
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setMaximumSize(DEFAULT_SIZE);

		//add components
		this.addTextAndButton();
		this.addPadding();
		this.addListViewer();
	}


	/**
	* add components
	**/
	/*add text and button*/
	private void addTextAndButton() {
		//text label
		String label = String.format("<html><p>Upload up to %d dataset files (.csv or .soft) or<br>correlation data from a previous session (.rds):</p></html>", MAX_FILES);
		uploadLabel = new JLabel(label);
		//button
		browseButton = new JButton("Browse...");
		browseButton.setFocusPainted(false);
		browseButton.addActionListener(this);
		//container panel
		JPanel browsePanel = new JPanel();
		browsePanel.setLayout(new BoxLayout(browsePanel, BoxLayout.X_AXIS));
		browsePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		browsePanel.add(uploadLabel);
		browsePanel.add(Box.createHorizontalGlue());
		browsePanel.add(browseButton);
		//add
		this.add(browsePanel);
	}

	/*add list viewer*/
	private void addListViewer() {
		//list viewer
		ListViewerPanel listViewer = new ListViewerPanel(list, fileList);
		listViewer.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(listViewer);
	}

	/*add padding (vertical)*/
	private void addPadding() {
		JPanel padding = new JPanel();
		padding.setMaximumSize(PADDING);
		padding.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(padding);
	}



	/**
	* access and modify panel
	**/
	/*add to end of list*/
	private void addListElement(File f) {
		list.addElement(f.getName());
		fileList.addElement(f);
	}

	/*modify uploadLabel*/
	public void setUploadLabel(String s) {
		uploadLabel.setText(s);
	}

	/*get file list*/
	public DefaultListModel<File> getFileList() {
		return fileList;
	}
	/*get file list as array*/
	public File[] getFileArray() {
		File[] files = new File[fileList.size()];
		Object[] fileObjects = fileList.toArray();
		for (int i = 0; i < fileList.size(); i++) {
			files[i] = (File) fileObjects[i];
		}
		return files;
	}

	/*get file list size*/
	public int numberOfFiles() {
		return fileList.size();
	}


	/**
	* action listener
	**/
	/*handle browse button & delete buttons*/
	public void actionPerformed(ActionEvent e) {
		//if browse button clicked
		if (e.getSource() == browseButton) {
			//open JFileChooser
			int returnVal = fileChooser.showOpenDialog(this);
			//add file name to list
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				//warning if too many files being loaded
				if (list.size() >= MAX_FILES) {
					this.tooManyFiles();
				}
				else {
					File f = fileChooser.getSelectedFile();
					addListElement(f);

				}
			}
		}
	}

	/**
	* too many files warning
	**/
	private void tooManyFiles() {
		String warning = String.format("Too many files! \nMaximum number of files is %d.", MAX_FILES);
		JOptionPane.showMessageDialog(new JFrame(), warning, "Warning", JOptionPane.WARNING_MESSAGE);
	}
}

