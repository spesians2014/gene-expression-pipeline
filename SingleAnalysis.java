/**
* GenEx SingleAnalysis
* Functions for aggregating and correlating 1 dataset
*
* Depends: JRI, Rengine, Analysis
*/

/*import libraries*/
import java.io.*;
import org.rosuda.JRI.Rengine;
import org.rosuda.JRI.REXP;
import org.rosuda.JRI.RBool;
import javax.swing.*;
import javax.swing.JOptionPane;


public class SingleAnalysis implements Analysis {
	/**
	* instance variables/constants
	**/
	/*constants*/
	//String SAFETY = "A" inherited from Analysis

	/*instance variables*/
	//Rengine
	private Rengine re;
	//file names
	private String filePath;
	private String fileName;
	private String rootPath;
	//R variable names
	private String gds;
	private String gdsDT;
	private String aggDT;
	private String corrEnv;
	//progress flags
	private boolean isFormatted;
	private boolean isAggregated;
	private boolean isCorrelated;


	/**
	* constructors
	**/
	/*normal constructor
	Assumes file has already been checked for type (.csv or .soft)*/
	public SingleAnalysis(File file, Rengine r) {
		this(file.getAbsolutePath(), FileCheck.getNameNoExt(file), false, r);
	}
	/*constructor from SplitAnalysis*/
	public SingleAnalysis(String filePath, String fileName, boolean isFormatted, Rengine re) {

		//instance variables
		this.re = re;
		this.filePath = filePath;
		this.rootPath = FileCheck.getPathNoExt(filePath);
		this.fileName = fileName;
		this.isFormatted = isFormatted;
		isFormatted = false;
		isAggregated = false;
		isCorrelated = false;
		setRVariableNames();
	}


	/**
	* constructor helper
	**/
	/*set R variable names*/
	private void setRVariableNames() {
		//R likes variable names to start with letters
		//guards against number-only filenames
		fileName = SAFETY + fileName;

		//set names
		gds = this.fileName + "gds";
		gdsDT = this.fileName + "gdsDT";
		aggDT = this.fileName + "aggDT";
		corrEnv = this.fileName + "corrEnv";
	}


	/**
	* methods
	**/
	/*parse data*/
	public boolean formatData() {
		//if already formatted, return
		if (isFormatted) {
			return isFormatted;
		}

		//if not already parsed, read soft file
		if (!Analysis.notNull(gds, re)) {
			//get extension
			String ext = FileCheck.getExtension(filePath);

			//if .soft file, use .soft parser
			if (ext.equals(FileCheck.SOFT)) {
				String parse = String.format("%s <- parseSOFT(\'%s\')", gds, filePath);
				re.eval(parse);
			}
			//if .csv file, use .csv parser
			else if (ext.equals(FileCheck.CSV)) {
				String parse = String.format("%s <- parseDELIM(\'%s\', \',\')", gds, filePath);
				re.eval(parse);
			}
			//if any text file, figure out whether delimiter is tab or character
			//and parse
			else {
				char delim = FileCheck.getDelimiter(filePath);
				String parse = String.format("%s <- parseDELIM(\'%s\', \'%s\')", gds, filePath, delim);
				re.eval(parse);
			}
		}
		//convert gds to gdsDT (data.table)
		String toDT = String.format("%s <- formatGDS(%s)", gdsDT, gds);
		re.eval(toDT);

		//check if parse and conversion successful
		isFormatted = Analysis.notNull(gdsDT, re);
		return isFormatted;
	}

	/*aggregate data
	does not check method*/
	public boolean aggData(int m) {
		//convert method to string
		String method = Analysis.getMethod(m);

		//if already formatted
		if (isFormatted) {
			String aggregate = String.format("%s <- aggData(%s, method = \'%s\')", aggDT, gdsDT, method);
			re.eval(aggregate);
		}

		//check if aggregation successful
		isAggregated = Analysis.notNull(aggDT, re);
		return isAggregated;
	}

	/*output gene probe map*/
	public boolean outGeneProbe() {
		//output GP map
		String geneProbe = String.format("GP <- outGeneProbe(%s, \'%s\')", gdsDT, rootPath);
		re.eval(geneProbe);
		//check if successful
		boolean success = Analysis.notNull("GP", re);
		return success;
	}

	/*output aggregated data .csv*/
	public boolean outAggData() {
		boolean success = false;

		//output aggregated data
		if (isAggregated) {
			String aggData = String.format("AD <- outAggData(%s, \'%s\')", aggDT, rootPath);
			re.eval(aggData);
			//check if successful
			success = Analysis.notNull("AD", re);
		}

		return success;
	}

	/*guess memory requirement for correlation in bytes
	use with a trycatch, in case it returns null?*/
	public long guessMem() {
		long need = 0;
		if (isAggregated) {
        	//get number of genes in dataset
			String guess = String.format("n <- nrow(%s)", aggDT);
			re.eval(guess);
			long n = (long) re.eval("n").asInt();
			need = n * n * 8; //because bytes
			return need;
		}
		return need;
	}

	/*correlate data
	does not check method*/
	public boolean corrData(int m) {
		//convert method to string
		String method = m == 1? "pearson": m == 2? "spearman": "error";

		if (isAggregated) {
			//corelate data if aggregated
			String correlate = String.format("%s <- corrData(%s, %s)", corrEnv, aggDT, method);
			re.eval(correlate);
			//check if successful
			isCorrelated = Analysis.notNull(corrEnv, re);
		}

		return isCorrelated;
	}

	/*save correlated data*/
	public boolean saveCorrData() {
		boolean success = false;

		if (isCorrelated) {
			//save corr data
			String save = String.format("SCD <- saveCorrData(%s, \'%s\')", corrEnv, rootPath);
			re.eval(save);
			//check if successful
			success = Analysis.notNull("SCD", re);
		}
		return success;
	}

	/*output correlation matrix*/
	public boolean outCorrMatrix() {
		boolean success = false;

		if (isCorrelated) {
			//output correlation matrix
			String write = String.format("OC <- outMatrix(%s$corrVec, %s$index, \'%s\')", corrEnv, corrEnv, rootPath+"_correlations.txt");
			re.eval(write);
			//check if successful
			success = Analysis.notNull("OC", re);
		}
		return success;

	}

	/*output p-value matrix*/
	public boolean outPvalMatrix() {
		boolean success = false;

		if (isCorrelated) {
			String write = String.format("OP <- outMatrix(%s$pVec, %s$index, \'%s\')", corrEnv, corrEnv, rootPath+"_pvals.txt");
			re.eval(write);
			//check if successful
			success = Analysis.notNull("OP", re);
		}
		return success;

	}

	/*clean up R workspace: remove all but corrEnv*/
	public void cleanUp() {
		String remove = String.format("rm(list = ls()[-match(\"%s\", ls())])", corrEnv);
		re.eval(remove);
	}
}
