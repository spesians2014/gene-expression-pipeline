/**
* GenEx Analysis interface
* Implemented by SingleAnalysis and SplitAnalysis
*
* Depends: JRI
*/

/*import libraries*/
import java.io.*;
import org.rosuda.JRI.Rengine;
import org.rosuda.JRI.REXP;
import org.rosuda.JRI.RBool;
import javax.swing.*;
import javax.swing.JOptionPane;


public interface Analysis {
	/**
	* instance variables and constants
	**/
	/*constants*/
	public static final String SAFETY = "A";
	//R likes variable names to start with letters
	public static final String VARENV = "varEnv";
	//enum constants
	public static String[] enums = {null, "mean", "maxvar", "max", "min", "maxmean", "minmean"};

	public static String getMethod(int m) {
		if (m > 0 && m < 7)
			return enums[m];
		else
			return "error";
	}	

	/**
	* static methods
	**/
	/*start Rengine*/
	public static Rengine startR() {
		Rengine re = Rengine.getMainEngine();
        //check if R Engine exists
		if (re == null) {
            // Create R Engine
			String[] Rargs = {"--vanilla"};
			re = new Rengine(Rargs, false, null);

			// import R function library
			re.eval("library(GenExR)");
			re.eval("gc()");
		}

        // Check that R is ready
		if(!re.waitForR()) {
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog(frame, "Cannot load R.", 
				"Warning", 
				JOptionPane.WARNING_MESSAGE);
		}

		return re;
	}

	/*ends R session -- only include if you're done with entire Java session*/
	public static void endR(Rengine re) {
		if (re != null) {
			re.end();
		}
	}

	/*check if R object is NOT null*/
	public static boolean notNull(String object, Rengine re) {
		if (re == null)
			return false;
		//if not null, re.eval(check).asBool() == FALSE; .isFALSE() == TRUE
		return re.eval(String.format("tryCatch({identical(%s, -1)}, error = function(e){return(TRUE)})", object)).asBool().isFALSE();
	}


	/**
	* abstract methods
	**/
	/*methods that should be implemented by subclasses*/
	public abstract boolean formatData();
	public abstract boolean aggData(int method);
	public abstract boolean outGeneProbe();
	public abstract boolean outAggData();
	public abstract long guessMem();
	public abstract boolean corrData(int method);
	public abstract boolean saveCorrData();
	public abstract boolean outCorrMatrix();
	public abstract boolean outPvalMatrix();
	public abstract void cleanUp();

}