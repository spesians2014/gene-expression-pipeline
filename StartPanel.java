/**
* GenEx GUI Opening panel
* File upload, GO button, advanced options and help button
* 
* Depends: Java Swing, utilities; StartPanelListener
* Parent: GenEx
**/

/*import libraries*/
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;


public class StartPanel extends JPanel {
	/**
	* instance variables/constants
	**/
	public static final Dimension PADSIZE = new Dimension(400, 55);
	public static final int[] DEFAULT_ARGS = {1, 1, 0, 0, 0, 0};
	private FileUploadPanel filePanel;
	private int[] arguments;
	private File[] files; //dataset files
	//package private for listener access
	GenEx g;
	CustomPanel customPanel;
	JButton analyzeButton;
	JButton customButton;
	JButton helpButton; 


	/**
	* constructor
	**/
	public StartPanel(GenEx g) {
		this.g = g;
		arguments = DEFAULT_ARGS;

		//set panel size & layout
		this.setPreferredSize(GenEx.DEFAULT_SIZE);
		this.setMinimumSize(GenEx.DEFAULT_SIZE);
		this.setOpaque(true);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(Box.createRigidArea(PADSIZE));

		//add components
		addWelcomeText();
		addUploadPanel();
		addAnalyzeButton();
		addOtherButtons();

		//add listener
		this.addListener();
	}


	/**
	* add components
	**/
	/*welcome text*/
	private void addWelcomeText() {
		//welcome title text
		JLabel welcome = new JLabel("GenEx");
		welcome.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 45));
		welcome.setAlignmentX(Component.CENTER_ALIGNMENT);

		//subtitle text
		JLabel subtitle = new JLabel("A tool for gene coexpression analysis");
		subtitle.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
		subtitle.setAlignmentX(Component.CENTER_ALIGNMENT);
		//padding
		JLabel padding = new JLabel(" ");

		//add 
		this.add(welcome);
		this.add(subtitle);
		this.add(padding);
	}

	/*file upload section*/
	private void addUploadPanel() {
		//create panel and add
		filePanel = new FileUploadPanel(GenEx.MAX_FILES);
		filePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(filePanel);
	}

	/*add analyze button*/
	private void addAnalyzeButton() {
		//panel for button (allows us to set button size)
		JPanel buttonPanel = new JPanel();
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonPanel.setMaximumSize(PADSIZE);

		//build button
		analyzeButton = new JButton("Create coexpression graph");
		analyzeButton.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
		analyzeButton.setPreferredSize(new Dimension(400, 50));
		analyzeButton.setFocusPainted(false);

		//add button
		buttonPanel.add(analyzeButton);
		this.add(buttonPanel);
	}

	/*add buttons*/
	private void addOtherButtons() {
		//container panel
		Dimension panelSize = new Dimension(400, 25);
		JPanel buttonPanel = new JPanel();
		buttonPanel.setMaximumSize(panelSize);
		buttonPanel.setPreferredSize(panelSize);
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

		//custom settings button
		customButton = new JButton("<html><font color = #0000FF><u>Custom settings</u></font></html>");
		customButton.setBorder(null);
		customButton.setFocusPainted(false);
		customButton.setContentAreaFilled(false);
		buttonPanel.add(customButton);

		//padding
		buttonPanel.add(Box.createHorizontalGlue());

		//help button
		helpButton = new JButton("<html><font color = #0000FF><u>Help</u></font></html>");
		helpButton.setBorder(null);
		helpButton.setFocusPainted(false);
		helpButton.setContentAreaFilled(false);
		//buttonPanel.add(helpButton);

		//add to panel
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(buttonPanel);
	}

	/*add listener*/
	private void addListener() {
		StartPanelListener l = new StartPanelListener(this);
		this.addMouseListener(l);
	}

	/**
	* modify and access panel
	**/
	/*get arguments*/
	public int[] getArguments() {
		return arguments;
	}

	/*change arguments*/
	public void changeArguments(int[] newArgs) {
		if (arguments.length == newArgs.length) {
			arguments = newArgs;
		}
		else {
			throw new IllegalArgumentException();
		}
	}

	/*return files*/
	public File[] getFiles() {
		return files;
	}

	/*update files*/
	public void updateFiles() {
		File[] newFiles = filePanel.getFileArray();
		files = newFiles;
		g.updateFiles(newFiles);
	}
}



/**
* StartPanel listener
* handles 
**/
class StartPanelListener extends MouseInputAdapter implements ActionListener {
	/*instance variables*/
	private StartPanel sp;

	/*constructor*/
	public StartPanelListener(StartPanel sp) {
		this.sp = sp;

		sp.analyzeButton.addActionListener(this);
		sp.customButton.addActionListener(this);
		//sp.helpButton.addActionListener(this);
	}

	/*action listener*/
	public void actionPerformed(ActionEvent e) {
		//custom settings button
		if (e.getSource() == sp.customButton) {
			if (sp.customPanel == null) {
				sp.customPanel = new CustomPanel(sp);
			}
			else {
				sp.customPanel.showPanel();
			}
			
		}
		//else if (e.getSource() == sp.helpButton) {;}
		//analyze button
		else if (e.getSource() == sp.analyzeButton) {
			//update files
			sp.updateFiles();
			//send arguments to GenEx
			sp.g.updateArgs(sp.getArguments());
			//check files
			File[] files = sp.getFiles();
			boolean checked = FileCheck.checkSPFiles(files);

			//go to next GenEx panel if files are okay
			//start analysis
			if (checked) {
				sp.g.createAnalysisPanel();
			}
		}
	}
}
