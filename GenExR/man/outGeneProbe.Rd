\name{outGeneProbe}
\alias{outGeneProbe}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Output gene-probe map file
}
\description{
Output gene-probe map .csv file.
}
\usage{
outGeneProbe(gdsDT, filepath)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{gdsDT}{
\code{data.table} of expression dataset.
}
  \item{filepath}{
Root filename to use.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
.csv file.
}
\references{
R base package.
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{write.csv}}, \code{\link{outCorrData}}
}
\examples{

}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ misc }
