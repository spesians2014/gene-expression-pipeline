\name{corrData}
\alias{corrData}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Correlation & p-values of aggregated data.
}
\description{
Calculates a correlation matrix and a p-value matrix for a  \code{\link[data.table]{data.table}} of aggregated data. The correlation and p-value data are saved as vectors containing the strict upper triangle of the matrices to reduce memory usage.
}
\usage{
corrData(aggDT, fxn, size = 1800)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{aggDT}{
\code{\link[data.table]{data.table}} containing aggregated data. Contains a column labelled \code{'Gene'} and a column labelled \code{'Probe'}. Each row is data for a unique gene.
}
\item{fxn}{
Function to use for correlation. Must take two matrices as input and produce a correlation matrix of values.
}
  \item{size}{
  Size of blocks.
}
}
\details{
Maximum size is theoretical \code{N} by \code{N} matrix where \code{(N) (N - 1) / 2 = 2 ^ 31 - 1}.
Correlation and p-value results are returned as vectors with the strict upper triangle of the matrices.
Correlation is calculated blockwise to avoid performance impact of cache swapping. \code{size} specifies size of blocks.
}
\value{
An environment with components: 
  \item{corrVec}{
  Numeric vector containing strict upper triangle of correlation matrix
}
  \item{pVec}{
  Numeric vector containing strict upper triangle of p-value matrix
  }
  \item{index}{
  Character vector of gene names (\code{aggDT} row names)
  }
}
\references{
modified from bigcor by A.N. Speiss, 2013
http://rmazing.wordpress.com/2013/02/22/bigcor-large-correlation-matrices-in-r
}
\author{
Rachel Xu
}
\note{
\code{\link{spearman}} or \code{\link{pearson}} can be taken as function arguments.
Calls \code{\link{tableAsMatrix}}, \code{\link{retvec}}, \code{\link{getp}}.
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{spearman}}, \code{\link{pearson}}, \code{\link{saveCorrData}}, \code{\link{outCorrData}}, \code{\link{cor}}, \code{\link[WGCNA]{corFast}}
}
\examples{

}

% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ misc }
