\name{loadCorrDataRDS}
\alias{loadCorrDataRDS}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Load correlation data from .rds files
}
\description{
Loads correlation data, p-value data, and gene index vectors saved as .rds files. Assumes the corrlation and p-value data files are vectors of the strict upper triangle of correlation/p-value matrices.
}
\usage{
loadCorrDataRDS(filepath)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{filepath}{
Filepath of .rds file with correlation data
}
}
\details{
It is essentially a wrapper for three calls to \code{\link{readRDS}}.
This function is MUCH preferred to \code{\link{loadCorrDataCSV}}. It is much faster for R to read in from .rds, a binary flat file, than .csv text file.
}
\value{
An environment with components: 
\item{corrVec}{
Numeric vector containing strict upper triangle of correlation matrix
}
\item{pVec}{
Numeric vector containing strict upper triangle of p-value matrix
}
\item{index}{
Character vector of gene names (\code{aggDT} row names)
}
}
\references{
R base package
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{loadCorrDataCSV}}, \code{\link{saveCorrData}}
}
\examples{

}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ misc }
