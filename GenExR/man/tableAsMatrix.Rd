\name{tableAsMatrix}
\alias{tableAsMatrix}
\title{
Convert a data table of aggregated data to a purely numeric matrix.
}
\description{
Convert a \code{\link[data.table]{data.table}} of aggregated data to a purely numeric matrix. This function assists in GenExR's correlation functions and should not be directly called by the end user. 
}
\usage{
tableAsMatrix(aggDT)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{aggDT}{
 \code{\link[data.table]{data.table}} containing aggregated data from a data set, with a column labelled \code{'Gene'} and a column labelled \code{'Probe'}, and each row corresponds to a unique gene.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
Matrix of data values, where each column corresponds to a unique gene.
}
\author{
Rachel Xu
}
\note{
Called in \code{\link{corrData}}
}

\seealso{
\code{\link{corrData}}
}

% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ misc }
