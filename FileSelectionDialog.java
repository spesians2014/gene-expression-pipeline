/**
* GenEx GUI file selection panel
* Allows user to choose which files/datasets to graph
* returns options as File array for GraphPanel
*
* Reference:
* Code modified from Oracle CustomDialog.java example
* http://docs.oracle.com/javase/tutorial/uiswing/examples/components/DialogDemoProject/src/components/CustomDialog.java
*
* Depends: Java Swing, utilities; FileCheck
* Parent: StartPanel
**/

/*import libraries*/
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.util.*;
import java.util.Vector;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.beans.*; //property change stuff


public class FileSelectionDialog extends JDialog implements PropertyChangeListener {
	/**
	* instance variables/constants
	**/
	private JOptionPane optionPane;
	private File[] files;
	private JCheckBox[] boxes;
	private int selected;
	private int n;
	private GenEx g;


	/**
	* Constructor
	**/
	public FileSelectionDialog(File[] files, GenEx g) {
		super(new JFrame(), "Select Files");

		//initialize instance variables
		this.g = g;
		this.files = files;
		n = files.length;
		selected = 0;

		//make option pane
		makeOptionPane();
		this.setContentPane(optionPane);

		//settings
		this.setLocationRelativeTo(g);
		this.pack();
		this.setVisible(true);
	}


	/**
	* make option pane
	**/
	private void makeOptionPane() {
		//create checkboxes
		boxes = new JCheckBox[n];
		for (int i = 0; i < n; i++) {
			boxes[i] = new JCheckBox(FileCheck.getDataName(files[i]));
			boxes[i].setSelected(false);
		}

		//create array with message and checkboxes
		Object[] array = {"Please select up to two files to graph:", boxes};

		//create JOptionPane
		optionPane = new JOptionPane(array, JOptionPane.INFORMATION_MESSAGE, JOptionPane.OK_OPTION);
		//register property change listener
		optionPane.addPropertyChangeListener(this);

		//handle window closting
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent w) {
				optionPane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
			}
		});
	}


	/**
	* check and update selected files
	**/
	private boolean checkAndUpdateFiles() {
		boolean filesOK = true;

		//get files that have been selected
		Vector<File> selectedFiles = new Vector<File>(GenEx.MAX_FILES);
		selected = 0;
		for (int i = 0; i < n; i++) {
			if (boxes[i].isSelected()) {
				selected++;
				selectedFiles.add(files[i]);
			}
		}

		//check number of selectd files: at least 1, no more than 2
		if (selected < 1) {
			JOptionPane.showMessageDialog(null, "Please select at least 1 file/dataset.", "No File", JOptionPane.WARNING_MESSAGE);
			filesOK = false;
		}
		else if (selected > GenEx.MAX_FILES) {
			String message = String.format("Please select no more than %d files/datasets.", GenEx.MAX_FILES);
			JOptionPane.showMessageDialog(null, message, "No File", JOptionPane.WARNING_MESSAGE);
			filesOK = false;
		}
		//change GenEx files
		else {
			files = selectedFiles.toArray(new File[selectedFiles.size()]);
			g.updateFiles(files);
			filesOK = true;
		}

		return filesOK;
	}


	/**
	* PropertyChangeListener
	**/
	public void propertyChange(PropertyChangeEvent e) {
		String prop = e.getPropertyName();

		if (this.isVisible() && (e.getSource() == optionPane) && (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
			Object value = optionPane.getValue();

			if (value.equals(JOptionPane.UNINITIALIZED_VALUE)) {
	            //ignore reset
				return;
			}

	        //Reset the JOptionPane's value -- otherwise, no property change event will be fired if button pressed again
			optionPane.setValue(
				JOptionPane.UNINITIALIZED_VALUE);

			//if OK button
			if (value.equals(JOptionPane.OK_OPTION)) {
				//check and update files
				if (checkAndUpdateFiles()) {
					//start to make graph panel
					g.createGraphPanel();
					//get rid of dialog
					this.dispose();
				}
			}
			//if user closed dialog
			else {
				//start to make graph panel with first two files
				g.createGraphPanel();
					//get rid of dialog
				this.dispose();
			}
		}
	}
}