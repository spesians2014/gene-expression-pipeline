/**
* GenEx GUI ListViewerPanel
* Displays each list element as a panel w/ delete button
* Works for lists of strings and lists of files + list of filename strings
*
* Depends: Java Swing, utilities; ListViewerModel, ListViewerListener
**/

/*import libraries*/
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;


public class ListViewerPanel extends JPanel {
	/**
	* instance variables and constants
	**/
	private final Dimension DEFAULT_SIZE = new Dimension(400, 75);
	private ListViewerModel model;
	private JScrollPane scrollPane; 

	/**
	* constructor
	**/
	public ListViewerPanel(DefaultListModel<String> list) {
		this(list, null, false);
	}
	public ListViewerPanel(DefaultListModel<String> list, DefaultListModel<File> fileList) {
		this(list, fileList, true);
	} 
	public ListViewerPanel(DefaultListModel<String> list, DefaultListModel<File> fileList, boolean withFiles) {
		//set listModel instance variables
		model = new ListViewerModel(list, fileList, withFiles);

		//set attributes
		this.setPreferredSize(DEFAULT_SIZE);
		this.setOpaque(true);
		this.setBackground(Color.WHITE);
		this.setLayout(new BorderLayout());

		//add elements
		scrollPane = new JScrollPane(model);
		scrollPane.setPreferredSize(DEFAULT_SIZE);
		this.add(scrollPane, BorderLayout.CENTER);

		this.setVisible(true);
	}

	public void clearList() {
		model.removeAll();
		model.clearLists();
	}
}




/**
* ListViewerModel is a panel that adds/deletes elements of list
* Depends: Java Swing, utilities; ListViewerElement (nested class)
**/
class ListViewerModel extends JPanel {
	/**
	* Helper class:
	* ListViewerElement is a panel with a delete button for each list element
	**/
	private class ListViewerElement extends JPanel implements ActionListener, MouseListener {
		//constants
		final String DELETE = "X";
		//instance variables
		JLabel text;
		JButton deleteButton;
		boolean selected;

		//constructor
		ListViewerElement(String s) {
			selected = false;

			//build text label
			text = new JLabel(s);
			text.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));

			//build delete button
			deleteButton = new JButton(DELETE);
			deleteButton.setBorder(null);
			deleteButton.setFocusPainted(false);
			deleteButton.setContentAreaFilled(false);
			deleteButton.setVisible(false);
			deleteButton.addActionListener(this);

			//build component
			this.setOpaque(true);
			this.setBackground(Color.WHITE);
			this.setBorder(BorderFactory.createEmptyBorder(0, 8, 0, 8));
			this.setLayout(new BorderLayout(0, 0));
			this.add(text, BorderLayout.LINE_START);
			this.add(deleteButton, BorderLayout.LINE_END);
			this.setVisible(true);

			this.addMouseListener(this);
		}

		//change color
		private void changeColor() {
			if (selected) {
				this.setBackground(Color.LIGHT_GRAY);
			}
			else {
				this.setBackground(Color.WHITE);
			}
		}

		//action listener
		public void actionPerformed(ActionEvent e) {
			//delete all selected elements
			if (e.getSource() == deleteButton) {
				selected = true;
				removeSelectedElements();
				this.revalidate();
			}
		}

		//mouse listener methods
		public void mousePressed(MouseEvent e) {
			//switch selected-ness
			selected = !selected;
			//show/hide delete button, change background accordingly
			deleteButton.setVisible(selected);
			changeColor();
		}
		public void mouseClicked(MouseEvent e) {;}
		public void mouseEntered(MouseEvent e) {;}
		public void mouseExited(MouseEvent e) {;}
		public void mouseReleased(MouseEvent e) {;}
	}


	//instance variables
	private DefaultListModel<ListViewerElement> elements;
	private DefaultListModel<String> list;
	private DefaultListModel<File> fileList;
	private boolean withFiles;

	/**
	* constructor
	**/
	public ListViewerModel(DefaultListModel<String> list, DefaultListModel<File> fileList, boolean withFiles) {
		//instance variables
		this.list = list;
		this.fileList = fileList;
		this.withFiles = withFiles;
		this.elements = new DefaultListModel<ListViewerElement>();

		//panel attributes
		this.setOpaque(false);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setVisible(true);

		//add listener
		this.addListViewerListener();
	}

	/**
	* draw elements onto the list viewer model
	**/
	private void drawElements() {
		int s = list.size();
		for (int i = 0; i < s; i++) {
			addElement(i);
		}
	}

	/**
	* add listener
	**/
	private void addListViewerListener() {
		list.addListDataListener(new ListViewerListener(this));
	}

	/**
	* adding and removing
	**/ 
	/*remove selected items from list*/
	void removeSelectedElements() {
		int i = 0;
		if (i < list.size()) {
			do {
				if (elements.get(i).selected) {
					list.removeElementAt(i);
					if (withFiles) {
						fileList.removeElementAt(i);
					}
					i--;
				}
				i++;
			} while (i < list.size());
		}
	}

	/* add item to panel at index i*/
	void addElement(int i) {
		elements.addElement(new ListViewerElement(list.get(i))); //add to elements list
		this.add(elements.get(i)); //add to panel
		this.revalidate();
	}

	/*remove element from panel at index i*/
	void removeElement(int i) {
		this.remove(elements.get(i)); //remove from panel
		elements.removeElementAt(i); //remove from elements list
		this.repaint();
		this.revalidate();
	}

	/*return list*/
	void clearLists() {
		list.clear();
		elements.clear();
		if (withFiles) {
			fileList.clear();
		}
	}
}




/**
* List viewer listener
**/
class ListViewerListener implements ListDataListener {
	//instance variable
	private ListViewerModel model;

	//constructor
	public ListViewerListener(ListViewerModel model) {
		this.model = model;
	}

	//interval added
	public void intervalAdded(ListDataEvent e) {
		//add element to panel
		model.addElement(e.getIndex0());
	}

	//interval removed
	public void intervalRemoved(ListDataEvent e) {
		//remove element from panel
		model.removeElement(e.getIndex0());
	}

	//contents changed
	public void contentsChanged(ListDataEvent e) {
		;
	}

} 