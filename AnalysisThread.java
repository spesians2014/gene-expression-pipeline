/**
* GenEx GUI AnalysisThread
* Runs R analysis and updates AnalysisPanel
*
* Depends: Java Swing, utilities; SwingWorker; Rengine; Analysis / SingleAnalysis / SplitAnalysis
* Depends/parent: AnalysisPanel
**/

/*import libraries*/
import org.rosuda.JRI.Rengine;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.util.Vector;
import java.io.File;
import java.lang.InterruptedException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.CancellationException;


public class AnalysisThread extends SwingWorker<Boolean, String> {
	/**
	* instance variables/constants
	**/
	//constant
	private final String MEMWARNING = "Memory warning!"; //trigger memory warning

	//instance variables
	//Rengine
	private Rengine re;
	//parent
	private AnalysisPanel ap;
	//analysis arguments
	private Analysis[] analyses;
	private String guessMem;
	private File[] files;
	private int[] arguments;
		//RECALL: arguments are: agg method, corr method; write agg data, write gene-probe map, write correlation matrix, write p-value matrix
	private AnalysisPanel.Process process;
	private int split;
	//GUI components
	private JProgressBar progressBar;
	private int progressCount;
	private int TOTALTASKS = 5; //base number of INCREMENTBYs
	private int INCREMENTBY;
	private JLabel status;
	private JButton cancelButton;
	private JButton backButton;
	//flags
	private boolean isCancelled;
	//GenEx
	private GenEx g;

	/**
	* constructor
	**/
	public AnalysisThread(GenEx g) {
		//initialize flags
		isCancelled = false;

		//initialize analysis arguments
		this.g = g;
		this.re = g.getRE();
		this.ap = g.getAnalysisPanel();
		this.files = g.getFiles();
		this.analyses = new Analysis[files.length];
		this.arguments = g.getArguments();
		this.process = ap.getProcess();
		this.split = ap.getSplit();

		//initialize GUI components
		this.progressBar = ap.getProgressBar();
		this.progressCount = 0;
		this.status = ap.getLabel();
		this.cancelButton = ap.getCancelButton();
		this.backButton = ap.getBackButton();

		//set total tasks
		for (int i = 2; i < arguments.length; i++) {
			TOTALTASKS += arguments[i];
		}
		//set incrementby
		INCREMENTBY = 100 / TOTALTASKS;
	}

	/**
	* working
	**/
	@Override
	protected Boolean doInBackground() throws Exception {
		boolean success = false;
		//switch which process we do
		if (!isCancelled) {
			switch (process) {
				case NORMAL: {
					success = normalAnalysis();
					break;
				}
				case SOME: {
					whichFiles();
					success = normalAnalysis();
					break;
				}
				case SPLIT: {
					splitData();
					success = normalAnalysis();
					break;
				}
			}
		}

		return success;
	}


	/*identify which files need to be analyzed*/
	private void whichFiles() {
		if (!isCancelled) {
			Vector<File> newfiles = new Vector<File>(files.length);
			for (int i = 0; i < files.length; i++) {
				//add file to list if NOT correlated
				if (!FileCheck.isCorrelated(files[i])) {
					newfiles.add(files[i]);
				}
			}
			//if not empty, update files list
			if (!newfiles.isEmpty()) {
				files = newfiles.toArray(files);
			}
		}
	}

	/*split analysis
	assumes there is only one file and it is .soft*/
	private void splitData() {
		//check to make sure 'split' is a valid subgroup
		//and only one file is present
		if ((split >= 1) && (files.length == 1) && !isCancelled) {
			//create SplitAnalysis
			analyses[0] = new SplitAnalysis(g, split);
		}
	}

	/*normal analysis*/
	private Boolean normalAnalysis() {
		boolean success = false;

		//Initiate SingleAnalysis if not already a SplitAnalysis
		if ((analyses[0] == null) && !isCancelled) {
			for (int i = 0; i < files.length; i++) {
				analyses[i] = new SingleAnalysis(files[i], re);
			}
		}
		//publish status
		progressCount += INCREMENTBY;
		publish("Analysis initialized...");


		//format data
		if (!isCancelled) {
			for (int i = 0; i < analyses.length; i++) {
				success = analyses[i].formatData();
			}
			if (success) {
				progressCount += INCREMENTBY;
				publish("Data parsed...");
			}
		}
		else { //return
			progressCount = 0;
			publish("Error: cannot parse data - please check your file format!");
			return success;
		}

		//aggregate
		if (!isCancelled) {
			for (int i = 0; i < analyses.length; i++) {
				success = analyses[i].aggData(arguments[0]);
			}
			if (success) {
				progressCount += INCREMENTBY;
				publish("Data aggregated...");
			}
		}
		else {
			progressCount = 0;
			publish("Error: cannot aggregate data");
			return success;
		}

		//publish intermediate files?
		//aggregated data
		if (!isCancelled) {
			if (arguments[2] != 0) {
				for (int i = 0; i < analyses.length; i++) {
					success = analyses[i].outAggData();
				}
				if (success) {
					progressCount += INCREMENTBY;
					publish("Aggregated data file written...");
				}
				else {
					publish("Error: cannot write out aggregated data file");
				}
			}
			//gene-probe map
			if (arguments[3] != 0) {
				for (int i = 0; i < analyses.length; i++) {
					success = analyses[i].outGeneProbe();
				}
				if (success) {
					progressCount += INCREMENTBY;
					publish("Gene-probe map file written...");
				}
				else {
					publish("Error: cannot write out gene-probe map file");
				}
			}
		}


		//send memory warning
		if (!isCancelled) {
			//guess correlation memory usage
			guessMem();

			//schedule timer for 15sec before closing frame
			javax.swing.Timer t = new javax.swing.Timer(15000, new ActionListener(){
				public void actionPerformed(ActionEvent e){
					JOptionPane.getRootFrame().dispose();
				}
			});
			t.setRepeats(false);
			t.start();

			//send memory warning
			int reply = JOptionPane.showConfirmDialog(null, "Calculating correlation and p-value matrices requires\nat least " + guessMem + " of memory and may take several minutes.\nWould you like to continue?\n\n(Correlation automatically proceeds in 15 seconds.)", "Memory warning", JOptionPane.YES_NO_OPTION);
			//if no, cancel analysis thread
			if (reply == JOptionPane.NO_OPTION) {
				isCancelled = true;
				this.cancelThread();
				cancelledGUI();
			}
		}


		//chance that thread might get cancelled here, so make sure thread isn't cancelled
		if (!isCancelled() && !isCancelled) {
			//correlate & save correlated data
			progressCount += INCREMENTBY;
			publish("Correlating data...");

			for (int i = 0; i < analyses.length; i++) {
				success = analyses[i].corrData(arguments[1]);
				success = analyses[i].saveCorrData();
			}
			if (success) {
				progressCount += INCREMENTBY;
				publish("Data correlated...");
			}
			else {
				publish("Error: cannot correlate data");
				return success;
			}

			//publish intermediate files?
			//correlation matrix
			if (arguments[4] != 0) {
				publish("Writing correlation matrix file...");
				for (int i = 0; i < analyses.length; i++) {
					success = analyses[i].outCorrMatrix();
				}
				if (success) {
					progressCount += INCREMENTBY;
					publish("Correlation matrix file written...");
				}
				else {
					publish("Error: cannot write out correlation matrix file");
				}
			}
			//p-value matrix
			if (arguments[5] != 0) {
				publish("Writing p-value matrix file...");
				for (int i = 0; i < analyses.length; i++) {
					success = analyses[i].outPvalMatrix();
				}
				if (success) {
					progressCount += INCREMENTBY;
					publish("p-value matrix file written...");
				}
				else {
					publish("Error: cannot write out correlation matrix file");
				}
			}

			//load graph panel
			publish("Preparing to graph...");

			progressCount = 100;
			publish("Analysis complete! Continue to graph creation.");
		}

		return success;
	}

	/*get estimated memory usage as string*/
	private void guessMem() {
		//get usage in bytes
		long need = 0;
		for (int i = 0; i < analyses.length; i++) {
			need += analyses[i].guessMem();
		}

		//convert to String
		double mem;
		long bytetoGB = 1;
		//A gigabyte is 2^30 times as large as a byte.
		bytetoGB <<= 30;
		if (need > bytetoGB) {
			mem = (double) need / bytetoGB;
			guessMem = String.format("%1$,.1f GB", mem);
		}
		else {
			//A megabyte is 2^20 times as large as a byte, or 2^-10 times as large as a gigabyte.
			bytetoGB >>= 10;
			mem = (double) need / bytetoGB;
			guessMem = String.format("%1$,.1f MB", mem);
		}
		
	}

	/*cancel thread*/
	public void cancelThread() {
		if (!isCancelled()) {
			try {
				Thread.sleep(300); //halt .3 sec before cancelling
			} catch (InterruptedException ignore) {;}
			this.cancel(true);
			isCancelled = true;
		}
	}

	/*set cancelled GUI*/
	public void cancelledGUI() {
		ap.cancelText();
		status.setText("");
		progressCount = 0;
		progressBar.setValue(progressCount);
		progressBar.paintImmediately(0, 0, progressBar.getSize().width, progressBar.getSize().height);
		backButton.setEnabled(true);
		cancelButton.setEnabled(false);
	}

	/**
	* update GUI: intermediate values
	* runs in EDT when doInBackground() publishes
	**/
	@Override
	protected void process(List<String> chunks) {
		//we're only interested in the most recent update
		String mostRecent = chunks.get(chunks.size() - 1);

		//update status and progress bar
		status.setText(mostRecent);
		progressBar.setValue(progressCount);
		progressBar.paintImmediately(0, 0, progressBar.getSize().width, progressBar.getSize().height);
	}


	/**
	* update GUI: final value
	* run in EDT when doInBackground() completes
	**/
	@Override
	protected void done() {
		Boolean success;
		try {
			success = get();
			if (success) {
				backButton.setEnabled(true);
				cancelButton.setEnabled(false);
				ap.toGraphPanel();
			}
			else {
				cancelledGUI();
			}
		}
		catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		catch (ExecutionException e) {
			throw new RuntimeException(e);
		}
		catch (CancellationException e) {
			cancelledGUI();
		}
	}
}