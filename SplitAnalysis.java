/**
* GenEx GroupAnalysis
* Aggregates and correlates dataset split into subgroups
*
* Depends: JRI, Analysis
* Assumes Analysis parseSOFT and getVariables have been called successfully!
*/

/*import libraries*/
import java.io.*;
import org.rosuda.JRI.Rengine;
import org.rosuda.JRI.REXP;
import org.rosuda.JRI.RBool;
import javax.swing.*;
import javax.swing.JOptionPane;


public class SplitAnalysis implements Analysis {
	/**
	* instance variables
	**/
	//Rengine
	private static Rengine re;
	//file names
	private String fileName;
	private String rootPath; //root pathname for subsets
	//index of chosen way to split dataset
	private int v;
	//R variables
	private String gds;
	//SingleAnalysis sets
	private String[] names; //names of subsets
	private String[] paths; //paths of subsets
	private SingleAnalysis[] data; //SingleAnalysis for each suubset
	private int n; //number of subsets
	//progress flags
	private boolean isFormatted;
	//GenEx
	private GenEx g;


	/**
	* constructors
	**/
	/*normal constructor
	Assumes file has already been checked for type (.csv or .soft)*/
	public SplitAnalysis(GenEx g, int v) {
		//get GenEx
		this.g = g;

		//get Rengine
		this.re = g.getRE();

		//instance variables
		this.v = v;
		File file = (g.getFiles())[0]; //first file
		fileName = FileCheck.getNameNoExt(file);
		rootPath = FileCheck.getPathNoExt(file);
		gds = SAFETY + fileName + "gds";
		isFormatted = false;
		n = -1;

		//check if parsed
		boolean isParsed = Analysis.notNull(gds, re);
		isParsed = Analysis.notNull(VARENV, re);
		if (!isParsed)
			throw new NullPointerException();
	}


	/**
	* static methods
	**/
	/*parse GEO SOFT file
	Assumes has already been checked for .soft extension */
	public static boolean parseSOFT(File f) {
		boolean success = false;

		//R
		Rengine r = Analysis.startR();

		//read GEO SOFT file
		String filePath = f.getAbsolutePath();
		String gds = SAFETY + FileCheck.getNameNoExt(f) + "gds";
		String read = String.format("%s <- parseSOFT(\'%s\')", gds, filePath);
		r.eval(read);

		//check if parse successful
		success = Analysis.notNull(gds, r);
		return success;
	}

	/*get sample information from gds
	Use a try-catch with this to catch a null pointer
	in case there is no varEnv or gds*/
	public static String[] getSubsets(File f) {
		//R
		Rengine r = Analysis.startR();

		//collect variable information from R
		String gds = SAFETY + FileCheck.getNameNoExt(f) + "gds";
		String getVars = String.format("%s <- getVariables(%s)", VARENV, gds);
		r.eval(getVars);

		//return different variable information as String array
		String[] vars = r.eval(VARENV + "$names").asStringArray();
		return vars;
	}


	/**
	* methods
	**/
	/*format data*/
	public boolean formatData() {
		//extract datatables and create SingleAnalyses
		if (!isFormatted) {
			if (v < 1) {
				isFormatted = false;
				return isFormatted;
			}

			//create R environment containing subsets
			String make = String.format("DTEnv <- makeDTs(%d, %s, %s)", v, VARENV, gds);
			re.eval(make);
			isFormatted = Analysis.notNull("DTEnv", re);

			//fill in names
			getNames();
			//check number of subgroups
			if (n < 1) {
				isFormatted = false;
				return isFormatted;
			}

			//extract gdsDT and create new SingleAnalysis for each subgroup
			data = new SingleAnalysis[n];
			for (int i = 0; i < n; i++) {
				isFormatted = extractDT(i);
				data[i] = new SingleAnalysis(paths[i], names[i], true, re);
			}

		}
		return isFormatted;
	}

	/*fill in names and paths of sub-groups*/
	private void getNames() {
		try {
			//extract DTs from DTEnvs
			//get names to append
			String getNames = String.format("names <- getNames(%d, %s)", v, VARENV);
			re.eval(getNames);
			String[] appendNames = re.eval("names").asStringArray();
			n = appendNames.length;

			//create names for DataAnalysis
			names = new String[n];
			paths = new String[n];
			for (int i = 0; i < n; i++) {
				names[i] = fileName + "_" + appendNames[i];
				paths[i] = rootPath + "_" + appendNames[i];
			}
		}
		catch (NullPointerException e) {
			isFormatted = false;
		}
	}

	/*update GenEx files with future correlation file names*/
	private void updateGenExFiles() {
		File[] newFiles = new File[n];
		for (int i = 0; i < n; i++) {
			newFiles[i] = new File(paths[i] + "_corrData.rds");
		}
		g.updateFiles(newFiles);
	}

	/*extract DT from DTEnv*/
	private boolean extractDT(int i) {
		String dt = "A" + names[i] + "gdsDT";
		String dtEnv = String.format("DTEnv$DT%d", i+1);
		re.eval(dt + " <- " + dtEnv);
		//check if successful
		boolean success = Analysis.notNull(dt, re);
		return success;
	}

	/*aggregate data
	does not check method*/
	public boolean aggData(int method) {
		boolean success = false;
		for (int i = 0; i < n; i++) {
			success = data[i].aggData(method);
		}
		return success;
	}

	/*output gene probe map*/
	public boolean outGeneProbe() {
		boolean success = false;
		success = data[0].outGeneProbe(); //only need one!
		return success;
	}


	/*output aggregated data .csv*/
	public boolean outAggData() {
		boolean success = false;

		//output aggregated data
		for (int i = 0; i < n; i++) {
			success = data[i].outAggData();
		}
		return success;
	}

	/*guess memory requirement for correlation
	use with a trycatch, in case it returns null?*/
	public long guessMem() {
		/*estimate memory for correlation*/
		long guess = data[0].guessMem();
		guess = n * guess;
		return guess;
	}

	/*correlate data
	does not check method*/
	public boolean corrData(int method) {
		boolean success = false;
		//correlate
		for (int i = 0; i < n; i++) {
			success = data[i].corrData(method);
		}
		return success;
	}

	/*save correlated data*/
	public boolean saveCorrData() {
		boolean success = false;
		//save
		for (int i = 0; i < n; i++) {
			success = data[i].saveCorrData();
		}
		//update GenEx files
		updateGenExFiles();
		return success;
	}

	/*output correlation matrix*/
	public boolean outCorrMatrix() {
		boolean success = false;
		//write
		for (int i = 0; i < n; i++) {
			success = data[i].outCorrMatrix();
		}
		return success;
	}

	/*output p-value matrix*/
	public boolean outPvalMatrix() {
		boolean success = false;
		//write
		for (int i = 0; i < n; i++) {
			success = data[i].outPvalMatrix();
		}
		return success;
	}

	/*clean up R workspace: remove all*/
	public void cleanUp() {
		String remove = String.format("rm(list = ls())");
		re.eval(remove);
	}
}
