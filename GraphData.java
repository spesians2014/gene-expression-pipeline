/**
* GenEx GraphData
* Functions for interfacing R graph-related functions with Java
* Generates adjacency matrix
*
* Depends: JRI, Rengine, Analysis 
*
* TODO: ADD SUPPORT FOR .CSV MATRIX FILES
**/

/*import libraries*/
import java.io.*;
import org.rosuda.JRI.Rengine;
import org.rosuda.JRI.REXP;
import javax.swing.*;
import java.util.*;
import java.util.Vector;


public class GraphData {
	/**
	* Instance variables/constants
	**/
	/*constants*/
	//for two-graph comparison
	public static final int BOTH = 3;
	public static final int G1 = 1;
	public static final int G2 = 2;
	public static final int NONE = 0;

	/*instance variables*/
	private Rengine re;
	private File file;
	//file information
	private String fileName;
	private String filePath;
	//R variables names
	private String corrEnv;
	private String adjEnv;
	private String edgeList;
	//parameters
	private double pval;
	private double tau;
	//flags
	private boolean hasAdjacency;
	private boolean isLoaded;
	private boolean isList;
	//store full list of genes searched for
	private String[] originalGeneList;


	/**
	* constructor
	**/
	//from File
	public GraphData(File file, Rengine re) {
		this(file.getAbsolutePath(), FileCheck.getNameNoExt(file), re);
	}
	//from given filenames & filepaths
	public GraphData(String filePath, String fileName, Rengine re) {
		//set instance variables
		this.file = file;
		this.re = re;
		this.filePath = filePath;
		this.fileName = fileName;
		pval = -1;
		tau = -1;
		hasAdjacency = false;
		isLoaded = false;
		isList = false;
		//set R variable names
		corrEnv = Analysis.SAFETY + fileName + "corrEnv";
		adjEnv = Analysis.SAFETY + fileName + "adjEnv";
		edgeList = adjEnv + "$edgeList";
	}

	
	/**
	* R interface methods
	**/
	/*check if correlation data needs to be loaded; load if so*/
	public boolean checkAndLoad() {
		//check if corrEnv exists. If not, may need to load...
		if (!Analysis.notNull(corrEnv, re)) {
			String ext = FileCheck.getExtension(filePath);
			//if name ends in .RDS, need to load the file
			if (ext.equals(FileCheck.RDS)) {
				//LOAD this filepath
				isLoaded = loadCorrDataRDS(filePath);
			}
			//if not .rds file, check if file_corrData.rds exists
			else {
				//load filepath w/o extension + _corrData.rds, if exists
				String loadPath = FileCheck.getPathNoExt(filePath) + FileCheck.CORRDATA;
				boolean check = new File(loadPath).isFile();
				if (check) {
					isLoaded = loadCorrDataRDS(loadPath);
				}

				//if not .soft or .rds, check number of columns in file
				//if three columns, assume is adjacency list
				//and load 
				else if (!ext.equals(FileCheck.RDS) && !ext.equals(FileCheck.SOFT) && (FileCheck.getColumnNumber(filePath) == FileCheck.IS_LIST)) {
					isLoaded = loadListDataDELIM(filePath);
				}

				//TODO: check if it's a text file?
				//add support for text file-ness
				else {
					throw new NullPointerException();
				}
			}
		}

		isLoaded = Analysis.notNull(corrEnv, re);

		return isLoaded;
	}

	//load correlation data from RDS file
	public boolean loadCorrDataRDS(String path) {
		String load = String.format("%s <- loadCorrDataRDS(\'%s\')", corrEnv, path);
		re.eval(load);
		boolean success = Analysis.notNull(corrEnv, re);
		return success;
	}
	//load correlation data from delimited text file LIST
	public boolean loadListDataDELIM(String path) {
		//get delimiter
		char delim = FileCheck.getDelimiter(filePath);
		//load
		String load = String.format("%s <- loadListDataDELIM(\'%s\', \'%c\')", corrEnv, path, delim);
		re.eval(load);
		//check whether successful or not
		boolean success = Analysis.notNull(corrEnv, re);
		isList = success; //set flag
		return success;

	}
	//load correlation data from delimited text file matrix
	public boolean loadCorrDataDELIM(String corrFilePath, String pFilePath) {
		//get delimiter
		char delim = FileCheck.getDelimiter(corrFilePath);
		//load
		String load = String.format("%s <- loadCorrDataDELIM(\'%s\', \'%s\', \'%c\')", corrEnv, corrFilePath, pFilePath, delim);
		re.eval(load);
		boolean success = Analysis.notNull(corrEnv, re);
		return success;
	}

	/*is the data from a list?*/
	public boolean isList() {
		return isList;
	}


	/*get gene names in dataset
	probably want to try-catch this, in case it returns a null pointer*/
	public String[] getGenes() {
		String[] geneNames = null;
		if (isLoaded) {
			String getCorrEnv = String.format("%s$index", corrEnv);
			geneNames = re.eval(getCorrEnv).asStringArray();
		}
		return geneNames;
	}

	/*get gene names not found as Vector*/
	public Vector<String> genesNotFound() {
		Vector<String> list = new Vector<String>();
		try {
			Vector<String> foundGenes = new Vector(Arrays.asList(getGenes()));
			for (int i = 0; i < originalGeneList.length; i++) {
				//add gene to list if not found (case insensitive)
				boolean found = false;
				//iterate and check if gene list contains string
				for (String s : foundGenes) {
					found = s.equalsIgnoreCase(originalGeneList[i]);
					//break once found
					if (found) {
						break;
					}
				}
				//if not found, add to list of not found genes
				if (!found) {
					list.add(originalGeneList[i]);
				}
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		
		return list;
	}


	/*get N most correlated genes
	probably want to try-catch this, in case it returns a null pointer*/
	public String[] mostCorrGenes(int n) {
        //get most correlated genes
		String[] preload = null;
		if (isLoaded) {
			String get = String.format("preload <- getMostCorrGenes(%s$corrVec, %s$index, %d)", corrEnv, corrEnv, n);
			re.eval(get);
			preload = re.eval("preload").asStringArray();
		}
		return preload;
	}

	/*get 100 random genes
	probably want to try-catch this, in case it returns a null pointer*/
	public String[] randomGenes(int n) {
		String[] random = null;
		if (isLoaded) {
			String get = String.format("randgenes <- sample(%s$index, %d, replace = TRUE)", corrEnv, n);
			re.eval(get);
			random = re.eval("randgenes").asStringArray();
		}
		return random;
	}

	/*generate adjacency/edge list from gene list, p-value, and network threshold*/
	public boolean adjacency(String[] geneList, double pval, double tau) {
		boolean success = false;
		originalGeneList = geneList; //store

		if (isLoaded && (geneList != null)) {
			re.assign("geneList", geneList);
			String adj = String.format("%s <- adjacency(geneList, %f, %f, %s)", adjEnv, pval, tau, corrEnv);
			re.eval(adj);
			success = Analysis.notNull(adjEnv, re);
			hasAdjacency = success;
			this.pval = pval;
			this.tau = tau;
		}
		return success;
	}

	/*returns whether or not the GraphData has an adjacency matrix/edge list*/
	public boolean hasAdjacency() {
		return hasAdjacency;
	}

	/*get file name*/
	public String getFileName() {
		return fileName;
	}

	/*get updated gene list that does not have genes not found in the dataset
	try-catch b/c null pointer, etc...*/
	public String[] getGeneList() {
		String[] newList = null;
		if (hasAdjacency) {
			String list = adjEnv+ "$geneList";
			newList = re.eval(list).asStringArray();
		}
		return newList;
	}

	/*extract edge list info from R*/
	public EdgeList getEdgeList() {
		int[] first = null;
		int[] second = null;
		String[] names = null;
		float[] values = null;
		double[] temp = null;

		//check if edge list is empty
		if (hasAdjacency && Analysis.notNull(edgeList, re)) {
			//get first and second columns
			first = re.eval(edgeList + "[[1]]").asIntArray();
			second = re.eval(edgeList + "[[2]]").asIntArray();
			//get values (as doubles)
			temp = re.eval(edgeList + "[[3]]").asDoubleArray();
		}
		//convert values from double to float
		try {
			values = new float[temp.length];
			for (int i = 0; i < temp.length; i++) {
				values[i] = (float) temp[i];
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		//get gene list
		names = getGeneList();

		EdgeList edgeList = new EdgeList(first, second, names, values);

		return edgeList;
	}


	/*write adjacency data*/
	public boolean outAdjData(String filename) {
		boolean success = false;
		if (hasAdjacency) {
			//if from a list, write list out
			if (isList) {
				String write = String.format("OA <- outEdgeList(%s, \'%s\')", adjEnv, filename);
				re.eval(write);
			}
			else {
			//if not from a list, write matrix out
				String write = String.format("OA <- outMatrix(%s, %s$geneList, \'%s\')", adjEnv + "$adjVec", adjEnv, filename);
				re.eval(write);
			}
		}
		//check if successful
		success = Analysis.notNull("OA", re);
		return success;
	}


	/*generate single network graph*/
	public void graph() {
		if (Analysis.notNull(edgeList, re)) {
			try {
				//get edge list
				EdgeList el = getEdgeList();

				//graph
				SingleGraph sg = new SingleGraph(el, fileName);
				GraphFramer f = new GraphFramer(sg, pval, tau, this);

				//reset pvalue and tau
				pval = -1;
				tau = -1;
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
	}


	/*generate graphs of unions and unique portions of this graph and another*/
	public void doubleGraph(GraphData gd2) {
		if (this.hasAdjacency() || gd2.hasAdjacency()) {
			try {
				//get edge lists
				EdgeList el1 = this.getEdgeList();
				EdgeList el2 = gd2.getEdgeList();

           		//generate double graph
				DoubleGraph dg = new DoubleGraph(el1, el2, fileName, gd2.getFileName());
				GraphFramer f = new GraphFramer(dg, pval, tau, this);
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
		else {
			return;
		}
	}


	/**
	* Nested class: EdgeList
	* Contains edge list information
	**/
	public class EdgeList {
		/*instance variables*/
		private int[] first;
		private int[] second;
		private String[] names;
		private float[] values;
		private int E; //number of edges
		private boolean isValid = false;

		/*constructor*/
		public EdgeList(int[] first, int[] second, String[] names, float[] values) {
			this.first = first;
			this.second = second;
			this.names = names;
			this.values = values;

			E = values.length;

			//make sure parallel arrays (first, second, values) are same size
			if ((E == first.length) && (E == second.length)) {
				isValid = true;
			}
		}

		/*check if valid*/
		public boolean isValid() {
			return isValid;
		}

		/*access methods*/
		public int[] first() {
			return first;
		}
		public String getFirstName(int i) {
			return names[first[i] - 1];
			//this may throw array out of bounds error
			//subtract 1 because R is 1-indexed and Java is 0-indexed
		}

		public int[] second() {
			return second;
		}
		public String getSecondName(int i) {
			return names[second[i] - 1];
			//this may throw array out of bounds error
			//subtract 1 because R is 1-indexed and Java is 0-indexed
		}

		public String[] names() {
			return names;
		}
		public float[] values() {
			return values;
		}

		/*number of edges*/
		public int E() {
			return E;
		}
	}
}