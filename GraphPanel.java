/**
* GenEx GUI GraphPanel
* File upload/gene selection --> graph
* UNDER CONSTRUCTION YOOOOO
*
* Depends: Java Swing, utilities; GraphData; StdLib In
* Parent: GenEx
*
* TODO: MAKE IT PRETTY
* Integrate InteractGraph with this pane --> actively updating graph?
**/

/*import libraries*/
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.DefaultComboBoxModel;
import java.util.*;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import edu.princeton.cs.introcs.*;


public class GraphPanel extends JPanel implements ActionListener {
	/**
	* instance variables/contants
	**/
	//dimension-y stuff
	private final int CONTENT_WIDTH = 400;
	private final int GENE_PANEL_HEIGHT = 160;
	private final int MOD_PANEL_WIDTH = 195;
	//GUI
	private JTextField pField;
	private JTextField tauField;
	private JButton graphButton;
	private JButton browseButton;
	private JButton backButton;
	private JButton addButton;
	private JButton clearButton;
	private JFileChooser fileChooser;
	private File[] files;
	private File geneFile;
	private DefaultListModel<String> geneList;
	private Vector<String> geneVector; //contains genes from file
	private ListViewerPanel geneListViewer;
	private Java2sAutoComboBox autoDropBox; //autocomplete box
	private GraphData[] graphs;
	private GenEx g;


	/**
	* constructor
	**/
	public GraphPanel(GenEx g) {
		this.g = g;
		files = g.getFiles();

		graphs = new GraphData[files.length];
		fileChooser = new JFileChooser();
		geneList = new DefaultListModel<String>();
		geneVector = new Vector<String>();

		//set attributes
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setPreferredSize(GenEx.DEFAULT_SIZE);

		//load graph
		createGraphData();
		loadGeneNames();
		pValNotice();

		//add components
		this.addHeader();
		this.addParameters();
		this.addGeneListPanel();
		this.addGraphButton();
		this.addBackButton();

		this.setVisible(true);
	}


	/*update files*/
	public void updateFiles(File[] files) {
		this.files = files;
	}


	/**
	* add components
	**/
	/*add header/title text*/
	private void addHeader() {
		//padding
		JPanel padding = new JPanel();
		padding.setMaximumSize(new Dimension(CONTENT_WIDTH, 45));
		padding.setAlignmentX(Component.CENTER_ALIGNMENT);
		//text
		JLabel header = new JLabel("Graph Parameters");
		header.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 35));
		header.setAlignmentX(Component.CENTER_ALIGNMENT);
		//add
		this.add(padding);
		this.add(header);
		this.addPadding();
		this.addPadding();
		this.addPadding();
	}

	/*add pvalue and threshold*/
	private void addParameters() {
		//parent panel
		JPanel parameterPanel = new JPanel();
		parameterPanel.setLayout(new BoxLayout(parameterPanel, BoxLayout.X_AXIS));
		parameterPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		parameterPanel.setMaximumSize(new Dimension(CONTENT_WIDTH, 25));
		//parameter selection
		JLabel pLabel = new JLabel("p-value:");
		pField = new JTextField("0.05");
		pField.setMaximumSize(new Dimension(50, 25));
		pField.setPreferredSize(new Dimension(50, 25));
		pField.addActionListener(this);
		JLabel tauLabel = new JLabel("network threshold:");
		tauField = new JTextField("0.9");
		tauField.setMaximumSize(new Dimension(50, 25));
		tauField.setPreferredSize(new Dimension(50, 25));
		tauField.addActionListener(this);
		//add components
		parameterPanel.add(Box.createHorizontalGlue());
		parameterPanel.add(pLabel);
		parameterPanel.add(pField);
		parameterPanel.add(Box.createHorizontalGlue());
		parameterPanel.add(tauLabel);
		parameterPanel.add(tauField);
		parameterPanel.add(Box.createHorizontalGlue());
		this.add(parameterPanel);
		this.addPadding();
		this.addPadding();
	}

	/*add autocomplete dropdown box*/
	private JPanel createAutoDropBox() {
		//parent panel
		JPanel autoPanel = new JPanel();
		autoPanel.setMaximumSize(new Dimension(MOD_PANEL_WIDTH, 40));
		autoPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		autoPanel.setLayout(new BorderLayout());
		//components
		autoDropBox = new Java2sAutoComboBox(geneVector);
		autoDropBox.setPreferredSize(new Dimension(MOD_PANEL_WIDTH, 21));
		autoDropBox.setSelectedItem("");
		addButton = new JButton("Add gene");
		//add listener
		autoDropBox.addActionListener(this);
		autoDropBox.getTextField().setFocusable(true);
		autoDropBox.getTextField().addMouseListener
		(new MouseInputAdapter() {
			public void mouseClicked(MouseEvent e) {
				autoDropBox.getTextField().requestFocusInWindow();
			}

		});
		autoDropBox.getTextField().addKeyListener
		(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				if (key == KeyEvent.VK_ENTER) {
					addGene();
				}
			}
		});
		addButton.addActionListener(this);
		//add components
		autoPanel.add(autoDropBox, BorderLayout.PAGE_START);
		autoPanel.add(addButton, BorderLayout.PAGE_END);
		return autoPanel;
	}

	/*create upload file button*/
	private JPanel createUploadButton() {
		//parent panel
		JPanel uploadPanel = new JPanel();
		uploadPanel.setLayout(new BorderLayout());
		uploadPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		uploadPanel.setMaximumSize(new Dimension(MOD_PANEL_WIDTH, 60));
		//components
		JLabel browseLabel = new JLabel("<html><div WIDTH=195>Or upload file of gene names:</div>");
		browseButton = new JButton("Browse");
		browseButton.addActionListener(this);
		browseButton.setFocusPainted(false);
		//add
		uploadPanel.add(browseLabel, BorderLayout.PAGE_START);
		uploadPanel.add(browseButton, BorderLayout.CENTER);

		return uploadPanel;
	}

	/*create clear button*/
	private JPanel createClearButton() {
		//parent panel
		JPanel clearPanel = new JPanel();
		clearPanel.setLayout(new BorderLayout());
		clearPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		clearPanel.setMaximumSize(new Dimension(MOD_PANEL_WIDTH, 20));
		//component
		clearButton = new JButton("Clear list");
		clearButton.addActionListener(this);
		clearButton.setFocusPainted(false);
		//add
		clearPanel.add(clearButton, BorderLayout.PAGE_START);

		return clearPanel;
	}

	/*create genes list modification panel*/
	private JPanel createModPanel() {
		//parent panel
		JPanel modPanel = new JPanel();
		modPanel.setMaximumSize(new Dimension(MOD_PANEL_WIDTH, GENE_PANEL_HEIGHT));
		modPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		modPanel.setLayout(new BoxLayout(modPanel, BoxLayout.Y_AXIS));
		//add
		modPanel.add(createAutoDropBox());
		modPanel.add(createClearButton());
		modPanel.add(Box.createVerticalGlue());
		modPanel.add(createUploadButton());

		return modPanel;
	}

	/*create gene list viewer*/
	private void createListViewer() {
		geneListViewer = new ListViewerPanel(geneList);
		geneListViewer.setPreferredSize(new Dimension(MOD_PANEL_WIDTH, GENE_PANEL_HEIGHT));
		geneListViewer.setMaximumSize(new Dimension(MOD_PANEL_WIDTH, GENE_PANEL_HEIGHT));
		geneListViewer.setAlignmentX(Component.CENTER_ALIGNMENT);
	}

	/*add gene list components to panel*/
	private void addGeneListPanel() {
		JPanel geneListPanel = new JPanel();
		geneListPanel.setMaximumSize(new Dimension(CONTENT_WIDTH, GENE_PANEL_HEIGHT));
		geneListPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		geneListPanel.setLayout(new BoxLayout(geneListPanel, BoxLayout.X_AXIS));
		//add
		geneListPanel.add(createModPanel());
		geneListPanel.add(Box.createHorizontalGlue());
		createListViewer();
		geneListPanel.add(geneListViewer);
		this.add(geneListPanel);
		this.addPadding();
	}

	/*add graph button*/
	private void addGraphButton() {
		//parent panel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonPanel.setMaximumSize(new Dimension(CONTENT_WIDTH, 50));
		//graph
		graphButton = new JButton("Graph");
		graphButton.setFocusPainted(false);
		graphButton.addActionListener(this);
		graphButton.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
		graphButton.setPreferredSize(new Dimension(CONTENT_WIDTH, 50));
		//add
		buttonPanel.add(graphButton, BorderLayout.PAGE_START);
		this.addPadding();
		this.add(buttonPanel);
		this.addPadding();
	}

	/*add back button*/
	private void addBackButton() {
		//parent panel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonPanel.setMaximumSize(new Dimension(CONTENT_WIDTH, 30));
		//component
		backButton = new JButton("<< Back");
		backButton.setFocusPainted(false);
		backButton.addActionListener(this);
		//add
		buttonPanel.add(backButton, BorderLayout.LINE_START);
		this.add(buttonPanel);
	}

	/*add padding*/
	private void addPadding() {
		JPanel padding = new JPanel();
		padding.setMaximumSize(new Dimension(CONTENT_WIDTH, 5));
		padding.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(padding);
	}


	/**
	* GeneList operations
	**/
	/*add to geneList from file*/
	private void geneListFromFile(File f) {
		In in = new In(f);
		for(int i = 0; !in.isEmpty(); i++) {
			String add = in.readLine();
			addToGeneList(add);
		}
	}

	/*convert geneList to string array*/
	public String[] geneListToArray() {
		String[] list = new String[geneList.size()];
		Object[] copyList = geneList.toArray();
		for (int i = 0; i < geneList.size(); i++) {
			list[i] = (String) copyList[i];
		}
		return list;
	}

	/*add element to geneList*/
	public void addToGeneList(String s) {
		if (!geneList.contains(s) && !s.equals("")) {
			geneList.addElement(s);
		}
	}

	/*access gene list*/
	public DefaultListModel<String> geneList() {
		return geneList;
	}


	/**
	* GraphData methods
	**/
	/*create GraphData instances*/
	public void createGraphData() {
		int s = files.length;

		try {
			graphs = new GraphData[s];
			for (int i = 0; i < s; i++) {
				graphs[i] = new GraphData(files[i], g.getRE());
				graphs[i].checkAndLoad();
			}
		}
		catch (NullPointerException e) {
			e.printStackTrace();
			FileCheck.graphDataError();
		}
	}

	/*load autocomplete gene names from R*/
	public void loadGeneNames() {
		//load genes from R
		if (graphs[0] != null) {
			String[] geneNames = graphs[0].getGenes();
			geneVector = new Vector(Arrays.asList(geneNames));

			//if more than one file, add any additional genes to geneVector
			if (graphs.length > 1) {
				for (int i = 1; i < graphs.length; i++) {
					String[] moreNames = graphs[i].getGenes();

					//if the gene index isn't equal, 
					//add to the list
					if (!Arrays.equals(geneNames, moreNames)) {
						for (int j = 0; j < moreNames.length; j++) {
							geneVector.add(moreNames[j]);
						}
					}
				}
			}
		}
	}

	/*if one of the GraphData is from an edge list, warn user that p-value filgering will not work for that dataset*/
	public void pValNotice() {
		boolean notice = false;
		for (int i = 0; i < graphs.length; i++) {
			notice = graphs[i].isList();
			if (notice) {
				//show message
				JOptionPane.showMessageDialog(null, "<html>Data uploaded from a list of correlations will not be filtered by p-value. </html>", "P-value filtering", JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		}
	}

	/**
	* action listener
	**/
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();

		//if browse button clicked
		if (source == browseButton) {
			browseFiles();
		}
		//if add gene button clicked
		else if (source == addButton) {
			addGene();
		}
		//if clear button clicked
		else if (source == clearButton) {
			clearList();
		}
		//if graph button clicked
		else if (source == graphButton) {
			makeGraphs();
		}
		//back button
		else if (source == backButton) {
			g.toFirst();
		}
	}

	/*browse files*/
	private void browseFiles() {
		//open JFileChooser
		int returnVal = fileChooser.showOpenDialog(this);
			//set gene file & geneList
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			geneFile = fileChooser.getSelectedFile();
			geneListFromFile(geneFile);
		}
	}

	/*add gene*/
	private void addGene() {
		//get name
		String name = (String) autoDropBox.getSelectedItem();
			//add to gene list
		this.addToGeneList(name);
       		//Reset the text field.
		autoDropBox.setSelectedItem("");
	}

	/*clear list*/
	private void clearList() {
		geneListViewer.clearList();
		geneList.clear();
		this.repaint();
		this.revalidate();
	}

	/**
	* make graphs
	**/
	private void makeGraphs() {
		double pVal = Double.parseDouble(pField.getText());
		double tauVal = Double.parseDouble(tauField.getText());

		//if list > 2 and p-val, tau-val between 0 and 1, graph
		if (checkGP(geneList.size(), pVal, tauVal)) {
			String[] geneListArray = geneListToArray();

			//please wait glass panel visible
			g.setVisibleGlass(true);


			GraphMaker maker = new GraphMaker(graphs, geneListArray, files, pVal, tauVal, g);
			maker.execute();
		}
	}

	/*separate thread for getting adjacency matrix, constructing graph*/
	class GraphMaker extends SwingWorker<Integer, Void> {
		//instance variables
		GraphData[] graphs;
		String[] geneListArray;
		File[] files;
		double pVal;
		double tauVal;
		GenEx g;

		//constructor
		GraphMaker(GraphData[] graphs, String[] geneListArray, File[] files, double pVal, double tauVal, GenEx g) {
			super();
			this.graphs = graphs;
			this.geneListArray = geneListArray;
			this.files = files;
			this.pVal = pVal;
			this.tauVal = tauVal;
			this.g = g;
		}

		@Override
		public Integer doInBackground() {
			int s = graphs.length;

			//if 1 graph
			if (s == 1) {
				graphs[0].adjacency(geneListArray, pVal, tauVal);
				graphs[0].graph();
			}
			//if 2 graphs
			else if (s > 1) {
				for (int i = 0; i < graphs.length; i++) {
					graphs[i].adjacency(geneListArray, pVal, tauVal);
				}
				graphs[0].doubleGraph(graphs[1]);
			}
			return s;
		}

		@Override
		public void done() {
			try {
				get();
				g.setVisibleGlass(false);
			} catch (ExecutionException e) {
				e.getCause().printStackTrace();
				String msg = String.format("Unexpected problem: %s", 
					e.getCause().toString());
			} catch (InterruptedException e) {
				e.getCause().printStackTrace();
			}
		}
	}

	/**
	* GraphPanel parameters check
	**/
	private boolean checkGP(int size, double pVal, double tauVal) {
		//check size of list
		boolean sizeOK = true;
		if (size < 2) {
			sizeOK = false;
			String errorMessage = String.format("<html><p>Please make sure you have supplied<br />at least two genes to graph!</p></html>");
			JOptionPane.showMessageDialog(null, errorMessage, "Gene list too short", JOptionPane.WARNING_MESSAGE);
		}

		//check value bounds -- must be between 0 and 1
		boolean valuesOK = true;
		if ((pVal > 1) || (pVal < 0) || (tauVal > 1) || (tauVal < 0)) {
			valuesOK = false;
			String errorMessage = String.format("<html><p>Please make sure your p-value and<br />network threshold are between 0 and 1.</p></html>");
			JOptionPane.showMessageDialog(null, errorMessage, "Cutoff value error", JOptionPane.WARNING_MESSAGE);
		}

		boolean bothOK = (sizeOK && valuesOK);
		return bothOK;
	}


}
